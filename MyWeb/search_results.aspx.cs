﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MyWeb
{
    public partial class search_results : System.Web.UI.Page
    {
        public string key_result = "";
        private string lang = "";
        public string att_level_result = "";
        public string manId = "";
        public string catId = "";
        public string catLevel = "";
        int recordCount = 0;
        int pageSize = 10;
        public int currentPage = 1;
        public int intPageNumber = 0;
        public string viewBy_result = GlobalClass.viewProducts9;
        public string sortBy_result = GlobalClass.viewProducts10;
        public string host = ConfigurationManager.AppSettings["url-web-base"];
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = Global.GetLang();
            try { pageSize = int.Parse(GlobalClass.viewProducts1); }
            catch { }
            if (Request["page"] != null) { currentPage = Int32.Parse(Request["page"]); }
            if (Request["attid"] != null) { att_level_result = Request["attid"].ToString(); }
            if (Request["key"] != null) { key_result = Request["key"].ToString(); }

            if (Request["cat"] != null)
            {
                string catTag = Request["cat"].ToString();
                if (Request["cat"].ToString().Contains("/"))
                {
                    string[] arrRes = Request["cat"].ToString().Split('/');
                    catTag = arrRes[0];
                    string manTag = arrRes[1];
                    var pagMan = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GM) && s.pagTagName == manTag).FirstOrDefault();
                    if (pagMan != null)
                        manId = pagMan.pagId.ToString();
                }
                var objSelected = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagTagName == catTag).FirstOrDefault();
                if (objSelected != null)
                {
                    catId = objSelected.pagId.ToString();
                    catLevel = objSelected.paglevel;
                    string strLBTagName = "/" + objSelected.pagTagName + ".html";
                    string _str = "";
                    _str += "<h2><a href='" + strLBTagName + "' >" + objSelected.pagName + "</a></h2>";
                    ltrHead.Text = _str;
                }
            }
            else
            {
                ltrHead.Text = "Kết quả tìm kiếm";
            }
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            string _str = "";

            viewBy_result = !String.IsNullOrEmpty(Request["view"]) ? Request.QueryString["view"].ToString() : viewBy_result;
            sortBy_result = !String.IsNullOrEmpty(Request["sort"]) ? Request["sort"].ToString() : sortBy_result;

            List<string> _listAttribute = BindDataByATT();
            IEnumerable<tbAttPro> attpro = db.tbAttPros.OrderBy(a => a.attId);
            IEnumerable<tbProduct> product = db.tbProducts.Where(p => p.proLang == lang).OrderBy(p => p.proId);
            if (!string.IsNullOrEmpty(manId))
            {
                product = product.Where(x => x.manufacturerId == int.Parse(manId));
            }
            if (!string.IsNullOrEmpty(catId))
            {
                List<string> lstCat = db.tbPages.Where(x => x.paglevel.StartsWith(catLevel) && x.pagActive == 1 && x.paglevel.Length >= catLevel.Length && x.pagType == int.Parse(pageType.GP)).Select(x => x.pagId.ToString()).ToList();
                product = product.Where(x => lstCat.Contains(x.catId));
            }
            if (_listAttribute.Count() > 0)
            {
                for (int i = 0; i < _listAttribute.Count(); i++)
                {
                    List<string> list = new List<string>();
                    string[] ListAttId = _listAttribute[i].ToString().Split(Convert.ToChar(","));
                    attpro = db.tbAttPros.Where(p => ListAttId.Contains(p.attId.ToString()));
                    foreach (var item in attpro)
                    {
                        list.Add(item.proId.ToString());
                    }
                    if (list.Count() > 0)
                    {
                        product = product.Where(p => list.Contains(p.proId.ToString()));
                    }

                }
            }

            if (sortBy_result == "name")
                product = product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else if (sortBy_result == "namedesc")
                product = product.OrderByDescending(s => s.proName).ThenBy(s => s.proName).ThenByDescending(s => s.proName).ToList();
            else if (sortBy_result == "price")
                product = product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else if (sortBy_result == "pricedesc")
                product = product.OrderByDescending(s => double.Parse(s.proPrice)).ThenByDescending(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else
                product = product.OrderBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            recordCount = product.Count();
            product = product.Skip((currentPage - 1) * pageSize).Take(pageSize);

            _str += "<ul class=\"product-list\">";
            _str += common.LoadProductList(product, viewBy_result);
            _str += "</ul>";
            ltrProList.Text = _str;

            if (recordCount % pageSize == 0) intPageNumber = recordCount / pageSize;
            else intPageNumber = recordCount / pageSize + 1;
            if (intPageNumber > 1)
            {
                string strPage = "";
                strPage += "<ul class=\"pager-nav\">";
                if (currentPage != 1)
                    strPage += "<li class=\"first_result\"><a><<</a></li><li class=\"prev_result\"><a><</a></li>";

                for (int i = 1; i <= intPageNumber; i++)
                {
                    if (i == currentPage)
                    {
                        strPage += "<li class=\"active pa pag" + i + "\" page=\"" + i + "\"><a href=\"javascript:{}\" rel=\"" + i + "\">" + i + "</a></li>";
                    }
                    else
                    {
                        strPage += "<li class=\"pa pag" + i + "\" page=\"" + i + "\"><a class=\"next_page\" href=\"javascript:{}\" rel=\"" + i + "\">" + i + "</a></li>";
                    }

                }
                if (currentPage != intPageNumber)
                    strPage += "<li class=\"next_result\"><a>></a></li><li class=\"last_result\"><a>>></a></li>";
                strPage += "</ul>";


                ltrPaging.Text = strPage;
            }
        }

        private List<string> BindDataByATT()
        {
            List<string> strArr = att_level_result.Split(Convert.ToChar(",")).ToList();
            List<string> listAtt = new List<string>();
            for (int i = 0; i < strArr.Count(); i++)
            {
                tbAttribute obj = db.tbAttributes.FirstOrDefault(a => a.Level == strArr[i] && a.Type == 1);
                string _strList = "";
                if (obj != null)
                    _strList = obj.Id + ",";

                for (int j = i + 1; j < strArr.Count(); j++)
                {
                    string ai = strArr[i].Substring(0, 5);
                    string aj = strArr[j].Substring(0, 5);
                    if (ai == aj)
                    {
                        tbAttribute attj = db.tbAttributes.FirstOrDefault(a => a.Level == strArr[j] && a.Type == 1);
                        _strList += attj.Id + ",";
                        strArr.RemoveAt(j);
                        j--;
                    }
                }
                if (_strList.Length > 0)
                {
                    _strList = _strList.Substring(0, _strList.Length - 1); listAtt.Add(_strList);
                }
            }
            return listAtt;
        }

    }
}