﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="MyWeb._Default" %>

    <!DOCTYPE HTML>
    <html>

    <head runat="server">
        <title></title>
        <asp:Literal runat="server" ID="ltrMainMeta"></asp:Literal>
        <%--META--%>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <asp:Literal ID="ltrMeta" runat="server"></asp:Literal>

            <%--style import--%>
                <link rel="stylesheet" type="text/css" href="theme/default/dist/css/bootstrap.min.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/font-awesome.min.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/datepicker.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/bootstrap-datepicker.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/owl-carousel/owl.carousel.css">
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/owl-carousel/owl.theme.css">
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/owl-carousel/owl.transitions.css">
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/cloudzoom/css/cloudzoom.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/cloudzoom/css/thumbelina.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/styles.css" />
                <link type="text/css" rel="stylesheet" href="../../theme/default/plugins/lightbox/colorbox.css" />
                <asp:Literal ID="lrtStyleSheet" runat="server"></asp:Literal>
                <%--style mobile--%>
                    <link rel="stylesheet" type="text/css" href="theme/default/css/icons.css" />
                    <link rel="stylesheet" type="text/css" href="theme/default/plugins/responside/component.css" />

                    <%--javascript import--%>
                        <!--[if lt IE 9]><script src="theme/default/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
                        <script src="theme/default/assets/js/ie-emulation-modes-warning.js"></script>
                        <script src="theme/default/assets/js/ie10-viewport-bug-workaround.js"></script>
                        <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
                        <script type="text/javascript" src="theme/default/js/jquery.min.js"></script>
                        <script type="text/javascript" src="theme/default/dist/js/bootstrap.min.js"></script>
                        <script type="text/javascript" src="theme/default/js/bootstrap-typeahead.js"></script>
                        <script type="text/javascript" src="theme/default/plugins/owl-carousel/owl.carousel.js"></script>
                        <script type="text/javascript" src="theme/default/js/script.js"></script>
                        <script type="text/javascript" src="theme/default/plugins/cloudzoom/js/cloudzoom.js"></script>
                        <script type="text/javascript" src="theme/default/plugins/cloudzoom/js/thumbelina.js"></script>
                        <script type="text/javascript" src="../../theme/default/plugins/lightbox/jquery.colorbox.js"></script>
                        <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

                        <%--javascript mobile--%>
                            <script type="text/javascript" src="theme/default/plugins/responside/modernizr.custom.js"></script>
                            <script type="text/javascript" src="theme/default/plugins/responside/classie.js"></script>
                            <script type="text/javascript" src="theme/default/plugins/responside/mlpushmenu.js"></script>
                            <script type="text/javascript" src="theme/default/js/bootstrap-datepicker.js"></script>

    </head>

    <body>
        <!--onload="_googWcmGet('number', '<%= GlobalClass.conTel %>')"-->
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '1895087717410765',
                    cookie: true,
                    xfbml: true,
                    version: 'v2.8'
                });
                FB.AppEvents.logPageView();
            };
        </script>
        <form id="MyWebForm" runat="server">
            <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
            <asp:Literal ID="ltrFacebook" runat="server"></asp:Literal>
            <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
            <asp:Literal ID="ltrH" runat="server"></asp:Literal>
            <asp:PlaceHolder ID="MainPlaceHolder" runat="server"></asp:PlaceHolder>
            <asp:Literal ID="ltrFooterBody" runat="server"></asp:Literal>
            <asp:Literal ID="ltrAdv" runat="server"></asp:Literal>
            <asp:Literal ID="ltrPopup" runat="server"></asp:Literal>
            <asp:Literal ID="ltrBottomLayer" runat="server"></asp:Literal>
            <asp:Literal ID="ltrLiveChat" runat="server"></asp:Literal>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="loadViewPro"></div>
                        </div>
                    </div>
                </div>
            </div>



            <span id="top-link-block" class="hidden">
            <a href="#top" class="well-sm" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
                <img src="theme/default/img/icon_gototop.png" />
            </a>
        </span>
            <%--javascript initialize --%>
                <script type="text/javascript">
                    $(document).ready(function() {
                        var listChk = [];
                        $("input[name='checksub']").click(function() {
                            var le = $(this).val()
                            if (this.checked == true) {
                                listChk.push($(this).val());
                                $("input.box" + le).attr("checked", true);
                            } else {
                                $("input.box" + le).attr("checked", false);
                                var removeitem = $(this).val();
                                listChk = $.grep(listChk, function(value) {
                                    return value != removeitem;
                                });
                            }
                            if (listChk == "") {
                                window.location.reload();
                            } else {
                                var cat = window.location.href;
                                cat = cat.substr(cat.lastIndexOf('/') + 1);
                                cat = cat.substr(0, cat.indexOf(".html"));
                                $("#loadFillter").load("/search_results.aspx?attid=" + listChk + "&cat=" + cat);
                            }
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        var Accordion = function(el, multiple) {
                            this.el = el || {};
                            this.multiple = multiple || false;
                            var links = this.el.find('.link');
                            links.on('click', {
                                el: this.el,
                                multiple: this.multiple
                            }, this.dropdown)
                        }

                        Accordion.prototype.dropdown = function(e) {
                            var $el = e.data.el;
                            $this = $(this),
                                $next = $this.next();

                            $next.slideToggle();
                            $this.parent().toggleClass('open');
                            if (!e.data.multiple) {
                                $el.find('.sub-menu-left').not($next).slideUp().parent().removeClass('open');
                            };
                        }
                        var accordion = new Accordion($('#menu_left'), false);
                    });

                    if (($(window).height() + 100) < $(document).height()) {
                        $('#top-link-block').removeClass('hidden').affix({
                            offset: {
                                top: 100
                            }
                        });
                    }

                    function openViewPro(proID) {
                        $("#loadViewPro").load("/view_product.aspx?id=" + proID);
                        $('.bs-example-modal-lg').modal();
                    }

                    function addToCartView(proID) {
                        $("#loadViewPro").load("/add_cart_product.aspx?id=" + proID);
                        $('.bs-example-modal-lg').modal();
                    }
                    $(document).ready(function() {
                        $(".modal .close").click(function() {
                            $("#loadViewPro").html("");
                        });

                        listProductSearch();
                    })
                    var tagsource = []

                    function listProductSearch() {
                        $.ajax({
                            async: false,
                            type: "POST",
                            url: "webService.asmx/getProductList",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(response) {
                                var tagProSearch = response.d;

                                for (i = 0; i < tagProSearch.length; i++) {
                                    tagsource.push({
                                        "id": tagProSearch[i].id,
                                        "name": tagProSearch[i].strName,
                                        "txtimg": tagProSearch[i].strImg
                                    });
                                }
                            },
                            failure: function(response) {}

                        });
                    }

                    function displayResult(item, val, text) {
                        console.log(item);
                    }

                    $(function() {
                        $('[id*=txtSearch]').typeahead({
                            source: tagsource,
                            itemSelected: displayResult
                        });
                    });
                </script>
                <script type="text/javascript">
                    new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'), {
                        type: 'cover'
                    });
                </script>
                <script type="text/javascript">
                    document.write("<script type='text/javascript' language='javascript'>MainContentW = 1040;LeftBannerW = 129;RightBannerW = 0;LeftAdjust = 0;RightAdjust = 1;TopAdjust = 145;ShowAdDiv();window.onresize=ShowAdDiv;<\/script>");
                </script>

                <script type="text/javascript">
                    $(window).load(function() {
                        $('#myModalPopUp').modal('show');
                    });
                </script>

                <script>
                    $(".body-social .facebook span").html("<i class='fa fa-facebook' aria-hidden='true'></i>");
                    $(".body-social .tweet span").html("<i class='fa fa-twitter' aria-hidden='true'></i>");
                    $(".body-social .googleplus span").html("<i class='fa fa-google-plus' aria-hidden='true'></i>");

                    $(".box-pro-home .cate-header:eq(1)").appendTo(".du-an-giao-dich");
                    $(".box-pro-home .product-list:eq(1)").appendTo(".du-an-giao-dich");

                    $(".project-in-home #listProjectSale .listProject_label span").html("SỰ KIỆN MỞ BÁN");
                    $("<div class='text-su-kien-mo-ban'>Hàng tuần, chúng tôi tổ chức rất nhiều sự kiện mở bán các dự án BĐS ở nhiều phân khúc khác nhau. Đây được coi là cơ sở để giúp bạn kết nối và nhanh chóng gia tăng thu nhập tốt nhất ...</div>").insertAfter(".project-in-home .listProject_label span");
                    $("<div class='line'><img src='uploads/layout/default/css/images/line.jpg' /></div>").insertAfter(".project_dev .listProject_label,.text-tu-van-vien");
                    $("#listProjectHot .listProject_label span").html("CÁC DỰ ÁN ĐANG GIAO DỊCH VÀ PHÁT TRIỂN");

                    $(".title-custumer-say").html("Tư vấn viên");
                    $("<div  class='text-tu-van-vien'>Hàng trăm dự án trên toàn quốc được công bố hỗ trợ các thông tin chính thống nhất cho bạn…</div>").insertAfter(".tu-van .title-custumer-say");
                    $("<div class='line'><img src='uploads/layout/default/css/images/line.jpg' /></div>").insertAfter(".text-tu-van-vien");
                    $(".line2").insertAfter(".content2 .box-about-us .header");
                    $(".line3").insertAfter(".content2 .box-news-home .header");
                    $(".line4").insertAfter(".content2 .box-top-video .header");

                    $(".p_2 .prjRegisterBtn .col-md-8 input").attr("value", "ĐĂNG KÝ TƯ VẤN");
                    $(".dang-ky-tu-van .prjRegisterName .control-label").html("<i class='fa fa-user' aria-hidden='true'></i>");
                    $(".dang-ky-tu-van .prjRegisterPhone .control-label").html("<i class='fa fa-phone aria-hidden='true'></i>");
                    $(".dang-ky-tu-van .prjRegisterEmail .control-label").html("<i class='fa fa-envelope' aria-hidden='true'></i>");
                    $(".dang-ky-tu-van .prjRegisterContent>.control-label").html("<i class='fa fa-file-text' aria-hidden='true'></i>");
                    $("#ctl18_ucProjectRegister_txtname").attr("Placeholder", "Họ và tên");
                    $("#ctl18_ucProjectRegister_txtDienthoai").attr("Placeholder", "Số điện thoại");
                    $("#ctl18_ucProjectRegister_txtmail").attr("Placeholder", "Email");
                    $("#ctl18_ucProjectRegister_txtnoidung").attr("Placeholder", "Ghi chú...");
                    $(".dang-ky-tu-van .prjRegisterBtn .btn.btn-primary.btn-md").attr("value", "ĐĂNG KÝ TƯ VẤN");

                    $("<div class='header'>ĐĂNG KÝ TƯ VẤN MIỄN PHÍ</div>").appendTo(".dang-ky-tu-van").insertBefore(".box-sub-prj-register");
                    $("<div class='text-dk'>Hàng trăm dự án trên toàn quốc được công bố hỗ trợ các thông tin chính thống nhất cho bạn…</div>").insertAfter(".project_dev .listProject_label span");
                    $(".line5").insertAfter(".text-su-kien-mo-ban,.dang-ky-tu-van .header");

                    $("<div class='header'>ĐĂNG KÝ TƯ VẤN MIỄN PHÍ</div>").insertBefore(".col_left .box-sub-prj-register #ctl18_ucProjectRegister1_udpPrlRegister");

                    $(".box-news-event .header").html("BẢNG BÁO GIÁ");
                    // $("<div class='line_page'><img src='uploads/layout/default/css/images/line_page.png'/></div>").insertAfter("#listProjectByCat .listProjectHeader a,.box-news-sub .txt-name-sub,.box-sub-contact .cate-header .txt-name-sub,.box-page-sub .txt-name-sub,#projDetail .infoTop .infoTopHead");

                    $(".con_name").html('<i class="fa fa-user" aria-hidden="true"></i>');
                    $(".con_address").html('<i class="fa fa-globe" aria-hidden="true"></i>');
                    $(".con_phone").html('<i class="fa fa-mobile" aria-hidden="true"></i>');
                    $(".con_phone_1").html('<i class="fa fa-phone" aria-hidden="true"></i>');
                    $(".con_mail").html('<i class="fa fa-envelope-o" aria-hidden="true"></i>');
                    $(".con_content").html('<i class="fa fa-comments" aria-hidden="true"></i>');
                    $(".con_cap").html('<i class="fa fa-cube" aria-hidden="true"></i>');

                    $("#ctl18_ucLoadControl_ctl00_txtname").attr("Placeholder", 'Họ và tên');
                    $("#ctl18_ucLoadControl_ctl00_txtdiachi").attr("Placeholder", 'Địa chỉ');
                    $("#ctl18_ucLoadControl_ctl00_txtDienthoai").attr("Placeholder", 'Số điện thoại');
                    $("#ctl18_ucLoadControl_ctl00_txtDienthoaicodinh").attr("Placeholder", 'Số điện thoại cố định');
                    $("#ctl18_ucLoadControl_ctl00_txtmail").attr("Placeholder", 'Email');
                    $("#ctl18_ucLoadControl_ctl00_txtnoidung").attr("Placeholder", 'Nội dung');
                    $("#ctl18_ucLoadControl_ctl00_txtCaptcha").attr("Placeholder", 'Capcha');

                    // ==== menu ====
                    if ($(window).width() < 992) {
                        $(".col_left").insertAfter(".col_right");
                    }
                    $(".menu_mobile .fa.fa-bars").click(function() {
                        $(".btn-close").addClass("menu-bg");
                        $(".menu-chinh").toggleClass("menu-chinh-1");
                    });
                    $(".btn-close").click(function() {
                        $(this).removeClass("menu-bg");
                        $(".menu-chinh").removeClass("menu-chinh-1");
                    });

                    $(".project-in-home .projItems_ticket_sale").html("Mở bán");
                    $(".project_dev .projItems_ticket_sale").html("Hot");

                    // ==== dk ====
                    $(".click_dk_all").click(function() {
                        $(".dk_all").toggleClass("dk_all_1");
                        $(".close_dk").toggleClass("close_dk_1");
                    });
                    $(".dk_all .fa-times").click(function() {
                        $(".dk_all").removeClass("dk_all_1");
                        $(".close_dk").removeClass("close_dk_1");
                    });
                    $(".close_dk").click(function() {
                        $(".dk_all").removeClass("dk_all_1");
                        $(this).removeClass("close_dk_1");
                    });

                    $(".dk_all #ctl18_ucProjectRegister2_txtname").attr("Placeholder", 'Họ và tên');
                    $(".dk_all #ctl18_ucProjectRegister2_txtmail").attr("Placeholder", 'Email');
                    $(".dk_all #ctl18_ucProjectRegister2_txtDienthoai").attr("Placeholder", 'Số điện thoại');
                    $(".dk_all #ctl18_ucProjectRegister2_txtnoidung").attr("Placeholder", 'Nội dung');

                    // ==== scroll to top ====
                    $(".to-top").click(function() {
                        $("html,body").animate({
                            scrollTop: 0
                        }, 600);
                    });
                    $(window).scroll(function() {
                        if ($(this).scrollTop()) {
                            $('.to-top:hidden').stop(true, true).fadeIn();
                        } else {
                            $('.to-top').stop(true, true).fadeOut();
                        }
                    });
                </script>
        </form>
    </body>

    </html>