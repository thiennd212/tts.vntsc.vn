﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.project
{
    public partial class projectRegister : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            int tt = Convert.ToInt32(drlTrangthai.SelectedItem.Value);            
            var _listForder = db.tbContacts.Where(s => s.conType == 10).ToList();
            if (tt != -1)
                _listForder = _listForder.Where(x => x.conActive == tt).ToList();
            if (_listForder.Any())
            {                
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected void drlTrangthai_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected string FormatDate(object date)
        {
            if (date != null)
            {
                if (date.ToString().Trim().Length > 0)
                {
                    if (DateTime.Parse(date.ToString()).Year != 1900)
                    {
                        DateTime dNgay = Convert.ToDateTime(date.ToString());
                        return ((DateTime)dNgay).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        protected string Actives(string actives)
        {
            string Chuoi = "";
            if (actives == "0")
            {
                Chuoi = "Chưa xem";
            }
            else
            {
                Chuoi = "Đã xem";
            }
            return Chuoi;
        }

        protected string GetProjectName(string proId)
        {
            if (!string.IsNullOrEmpty(proId))
            {
                var prj = db.tbProjects.FirstOrDefault(x => x.proId.ToString() == proId);
                if (prj != null)
                    return prj.proName;
                else
                    return "";
            }
            else
                return "";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Edit":
                    var list = db.tbContacts.FirstOrDefault(s => s.conId == int.Parse(strID));
                    txtTennguoiLH.Text = list.conName;
                    txtDiachi.Text = list.conAddress;
                    txtDienthoai.Text = list.conTel;
                    txtMail.Text = list.conMail;
                    txtNoidung.Text = list.conDetail;
                    txtNgay.Text = FormatDate(list.conDate).ToString();
                    list.conActive = 1;
                    db.SubmitChanges();
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Del":
                    tbContactDB.tbContact_Delete(strID);
                    BindData();
                    break;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }
    }
}