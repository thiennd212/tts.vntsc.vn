﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.system
{
    public partial class configuration : System.Web.UI.UserControl
    {
        #region[Declare Varian]
        string lang = "vi";
        string changeFreq = "weekly";
        string pagePriority = "0.80";
        string productPriority = "0.80";
        string newsPriority = "0.70";
        dataAccessDataContext db = new dataAccessDataContext();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                common.OnlyInputNumber(txtviewNews1);
                common.OnlyInputNumber(txtviewNews2);
                common.OnlyInputNumber(txtviewNews3);
                common.OnlyInputNumber(txtviewNews6);
                common.OnlyInputNumber(txtviewProducts1);
                common.OnlyInputNumber(txtviewProducts2);
                common.OnlyInputNumber(txtviewProduct14);
                common.OnlyInputNumber(txtviewProducts4);
                BindConfig();
            }
        }

        void BindConfig()
        {
            txtproductInLine.Text = GlobalClass.ProductInline != null && GlobalClass.ProductInline != "" ? GlobalClass.ProductInline : "3";
            txtGoogleAnalytics.Text = GlobalClass.Google_Analytics_Id;
            var list = new List<tbConfig>();
            list = db.tbConfigs.Where(s => s.conLang == lang).ToList();
            if (list.Count > 0)
            {
                string[] viewNews = list[0].viewNews.Split(Convert.ToChar(","));
                string[] viewProducts = list[0].viewProducts.Split(Convert.ToChar(","));
                string[] viewBanner = list[0].viewBanner.Split(Convert.ToChar(","));
                string[] viewSocial = list[0].viewSocial.Split(Convert.ToChar(","));
                string[] viewPayment = list[0].viewPayment.Split(Convert.ToChar(","));
                txtId.Value = list[0].conid + "";
                txtTencoquan.Text = list[0].conCompany;
                txtDiachi.Text = list[0].conAddress;
                txtDienthoai.Text = list[0].conTel;
                txtFax.Text = list[0].conFax;
                txtWebsite.Text = list[0].conWebsite;
                txtPhuongthucguimail.Text = list[0].conMail_Method;
                txtMaychugiumail.Text = list[0].conMail_Smtp;
                txtCongguimail.Text = list[0].conMail_Port + "";
                txtEmaillienhe.Text = list[0].conMail_Info;
                txtEmailgui.Text = list[0].conMail_Noreply;
                txtPasswordmail.Attributes.Add("Value", list[0].conMail_Pass);
                fckFooter.Text = list[0].conCopyright;
                fckNguoilienhe.Text = list[0].conContact1;
                txtTitle.Text = list[0].conTitle;
                txtDesc.Text = list[0].conDescription;
                txtKey.Text = list[0].conKeywords;
                txtlivebox.Text = list[0].livechat;
                txtImage.Text = list[0].conEditorial;
                txtdongchay.Text = list[0].conContact;
                txtviewNews1.Text = viewNews[0];
                txtviewNews2.Text = viewNews[1];
                txtviewNews3.Text = viewNews[2];
                txtviewNews4.Text = viewNews[3];
                txtviewNews5.Text = viewNews[4];
                txtviewNews6.Text = viewNews[5];
                txtviewNews8.Text = viewNews[7];
                txtviewNews9.Text = viewNews[8];
                ddlNewsList.SelectedValue = viewNews[9];
                txtviewProducts1.Text = viewProducts[0];
                txtviewProducts2.Text = viewProducts[1];
                txtviewProduct14.Text = viewProducts[13];
                txtviewProducts3.Text = viewProducts[2];
                txtviewProducts4.Text = viewProducts[3];
                drlShowCatelogy.SelectedValue = viewProducts[5];
                drlShowProduct.SelectedValue = viewProducts[6];
                txtviewProducts8.Text = viewProducts[7];
                ddlSort.SelectedValue = viewProducts[9];

                try
                {
                    txtviewProductsHot.Text = viewProducts[10];
                }
                catch{}

                try
                {
                    txtviewProductsPromo.Text = viewProducts[11];
                }
                catch{}
                
                try
                {
                    if (viewProducts[12] == "1")
                    {
                        chkViewFast.Checked = true;
                    }
                    else
                    {
                        chkViewFast.Checked = false;
                    }
                }
                catch
                {
                    chkViewFast.Checked = false;
                }
                try
                {
                    if (list[0].catId + "" != "0")
                    {
                        ddlStyleNews.SelectedValue = list[0].catId + "";
                    }
                }
                catch{ }
                if (viewSocial.Count() > 0)
                {
                    txtfacebook.Text = viewSocial[0];
                }
                if (viewSocial.Count() > 1)
                {
                    txttweet.Text = viewSocial[1];
                }
                if (viewSocial.Count() > 2)
                {
                    txtyoutube.Text = viewSocial[2];
                }
                if (viewSocial.Count() > 3)
                {
                    txtzingme.Text = viewSocial[3];
                }
                if (viewSocial.Count() > 4)
                {
                    txtgoogle.Text = viewSocial[4];
                }
                if (viewSocial.Count() > 5)
                {
                    txtRSS.Text = viewSocial[5];
                }

                if (viewSocial.Count() > 6)
                {
                    txtStyleYahoo.Text = viewSocial[6];
                }
                else
                {
                    txtStyleYahoo.Text = "2";
                }

                if (viewSocial.Count() > 8)
                {
                    txtStyleSkype.Text = viewSocial[8];
                }
                else
                {
                    txtStyleSkype.Text = "16";
                }

                try
                {
                    if (viewSocial.Count() > 7)
                    {
                        chkCommnetFacebook.Checked = viewSocial[7].Contains("1") ? true : false;
                    }

                    if (viewSocial.Count() > 11)
                    {
                        txtWidthCB.Text = viewSocial[11];
                    }
                    else
                    {
                        txtWidthCB.Text = "770";
                    }
                    if (viewSocial.Count() > 12)
                    {
                        txtNumCB.Text = viewSocial[12];
                    }
                    else
                    {
                        txtNumCB.Text = "5";
                    }


                    if (viewSocial.Count() > 14)
                    {
                        chkCommentFNews.Checked = viewSocial[14].Contains("1") ? true : false;
                    }
                    if (viewSocial.Count() > 15)
                    {
                        chkCommentFPage.Checked = viewSocial[15].Contains("1") ? true : false;
                    }
                }
                catch{ }

                ddlNewsIndex.SelectedValue = viewNews[6];
                
                ckePageError.Text = list[0].pageError;
                txth1.Text = list[0].Field1;
                CKEField2.Text = list[0].Field2;
                CKEField4.Text = list[0].Field4;
                chkLogin.Checked = list[0].Field5.Contains("1") ? true : false;
                txtSDTKyThuat.Text = list[0].conTelKyThuat;
                chkPopup.Checked = list[0].conPopup == 1 ? true : false;
                chkPopup2.Checked = list[0].conPopup2 == 1 ? true : false;
            }
            else
            {
                txtId.Value = "";
                txtTencoquan.Text = "";
                txtDiachi.Text = "";
                txtDienthoai.Text = "";
                txtFax.Text = "";
                txtWebsite.Text = "";
                txtPhuongthucguimail.Text = "";
                txtMaychugiumail.Text = "";
                txtCongguimail.Text = "";
                txtEmaillienhe.Text = "";
                txtEmailgui.Text = "";
                txtPasswordmail.Text = "";
                fckFooter.Text = "";
                fckNguoilienhe.Text = "";
                txth1.Text = "";
                chkLogin.Checked = false;
                txtSDTKyThuat.Text = "";
            }
        }

        protected bool Validation()
        {
            if (txtTencoquan.Text == "") { ltrErr.Text = "Tên cơ quan không được để trống !!"; txtTencoquan.Focus(); pnlErr.Visible = true; return false; }
            if (txtCongguimail.Text == "") { ltrErr.Text = "Cổng gửi mail không được để trống !!"; txtCongguimail.Focus(); pnlErr.Visible = true; return false; }
            if (txtEmaillienhe.Text == "") { ltrErr.Text = "Email liên hệ không được để trống !!"; txtEmaillienhe.Focus(); pnlErr.Visible = true; return false; }
            if (txtEmailgui.Text == "") { ltrErr.Text = "Email gửi không được để trống !!"; txtEmailgui.Focus(); pnlErr.Visible = true; return false; }
            if (txtPasswordmail.Text == "" && txtId.Value == "") { ltrErr.Text = "Passwordmail không được để trống !!"; txtPasswordmail.Focus(); pnlErr.Visible = true; return false; }
            if (txtTitle.Text == "") { ltrErr.Text = "Chưa nhập tiêu đề website !!"; txtTitle.Focus(); pnlErr.Visible = true; return false; }
            if (txtKey.Text == "") { ltrErr.Text = "Chưa nhập từ khóa !!"; txtKey.Focus(); pnlErr.Visible = true; return false; }
            if (txtDesc.Text == "") { ltrErr.Text = "Chưa nhập mô tả !!"; txtDesc.Focus(); pnlErr.Visible = true; return false; }
            return true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string[] viewNews = new string[10];
                string[] viewProducts = new string[14];
                string[] viewBanner = new string[8];
                string[] viewSocial = new string[16];
                string[] viewPayment = new string[8];
                viewNews[0] = !String.IsNullOrEmpty(txtviewNews1.Text) ? txtviewNews1.Text : "10";
                viewNews[1] = !String.IsNullOrEmpty(txtviewNews2.Text) ? txtviewNews2.Text : "4";
                viewNews[2] = !String.IsNullOrEmpty(txtviewNews3.Text) ? txtviewNews3.Text : "6";
                viewNews[3] = !String.IsNullOrEmpty(txtviewNews4.Text) ? txtviewNews4.Text : "6";
                viewNews[4] = !String.IsNullOrEmpty(txtviewNews5.Text) ? txtviewNews5.Text : "5";
                viewNews[5] = !String.IsNullOrEmpty(txtviewNews6.Text) ? txtviewNews6.Text : "5";
                viewNews[6] = !String.IsNullOrEmpty(ddlNewsIndex.SelectedValue) ? ddlNewsIndex.SelectedValue : "1";
                viewNews[7] = !String.IsNullOrEmpty(txtviewNews8.Text) ? txtviewNews8.Text : "5";
                viewNews[8] = !String.IsNullOrEmpty(txtviewNews9.Text) ? txtviewNews9.Text : "4";
                viewNews[9] = !String.IsNullOrEmpty(ddlNewsList.SelectedValue) ? ddlNewsList.SelectedValue : "1";
                viewProducts[0] = !String.IsNullOrEmpty(txtviewProducts1.Text) ? txtviewProducts1.Text : "10";
                viewProducts[1] = !String.IsNullOrEmpty(txtviewProducts2.Text) ? txtviewProducts2.Text : "3";
                viewProducts[13] = !String.IsNullOrEmpty(txtviewProduct14.Text) ? txtviewProduct14.Text : "3";
                viewProducts[2] = !String.IsNullOrEmpty(txtviewProducts3.Text) ? txtviewProducts3.Text : "5";
                viewProducts[3] = !String.IsNullOrEmpty(txtviewProducts4.Text) ? txtviewProducts4.Text : "10";
                viewProducts[4] = drlShowCatelogy.SelectedValue;
                viewProducts[5] = drlShowCatelogy.SelectedValue;
                viewProducts[6] = drlShowProduct.SelectedValue;
                viewProducts[7] = !String.IsNullOrEmpty(txtviewProducts8.Text) ? txtviewProducts8.Text : "5";
                viewProducts[9] = ddlSort.SelectedValue;

                viewProducts[10] = !String.IsNullOrEmpty(txtviewProductsHot.Text) ? txtviewProductsHot.Text : "5";
                viewProducts[11] = !String.IsNullOrEmpty(txtviewProductsPromo.Text) ? txtviewProductsPromo.Text : "5";
                viewProducts[12] = chkViewFast.Checked ? "1" : "0";


                viewSocial[0] = txtfacebook.Text.Trim();
                viewSocial[1] = txttweet.Text.Trim();
                viewSocial[2] = txtyoutube.Text.Trim();
                viewSocial[3] = txtzingme.Text.Trim();
                viewSocial[4] = txtgoogle.Text.Trim();
                viewSocial[5] = txtRSS.Text.Trim();
                viewSocial[6] = txtStyleYahoo.Text.Trim() != null ? txtStyleYahoo.Text.Trim() : "2";
                viewSocial[7] = chkCommnetFacebook.Checked ? "1" : "0";
                viewSocial[8] = txtStyleSkype.Text.Trim() != null ? txtStyleSkype.Text.Trim() : "16";
                viewSocial[11] = txtWidthCB.Text.Trim() != null ? txtWidthCB.Text.Trim() : "770";
                viewSocial[12] = txtNumCB.Text.Trim() != null ? txtNumCB.Text.Trim() : "5";
                try
                {
                    viewSocial[14] = chkCommentFNews.Checked ? "1" : "0";
                    viewSocial[15] = chkCommentFPage.Checked ? "1" : "0";
                }
                catch { }

                if (txtId.Value == "")
                {
                    tbConfigDATA ObjtbConfigDATA = new tbConfigDATA();
                    ObjtbConfigDATA.conCompany = txtTencoquan.Text;
                    ObjtbConfigDATA.conAddress = txtDiachi.Text;
                    ObjtbConfigDATA.conTel = txtDienthoai.Text;
                    ObjtbConfigDATA.conFax = txtFax.Text;
                    ObjtbConfigDATA.conWebsite = txtWebsite.Text;
                    ObjtbConfigDATA.conMail_Method = txtPhuongthucguimail.Text;
                    ObjtbConfigDATA.conMail_Smtp = txtMaychugiumail.Text;
                    ObjtbConfigDATA.conMail_Port = txtCongguimail.Text;
                    ObjtbConfigDATA.conMail_Info = txtEmaillienhe.Text;
                    ObjtbConfigDATA.conMail_Noreply = txtEmailgui.Text;
                    if (txtPasswordmail.Text.Trim() != "") ObjtbConfigDATA.conMail_Pass = txtPasswordmail.Text;
                    ObjtbConfigDATA.conEditorial = txtImage.Text;
                    ObjtbConfigDATA.conContact1 = fckNguoilienhe.Text;
                    ObjtbConfigDATA.conCopyright = fckFooter.Text;
                    ObjtbConfigDATA.catId = ddlStyleNews.SelectedValue;
                    ObjtbConfigDATA.conContact = txtdongchay.Text;
                    ObjtbConfigDATA.conTitle = txtTitle.Text;
                    ObjtbConfigDATA.conKeywords = txtKey.Text;
                    ObjtbConfigDATA.conDescription = txtDesc.Text;
                    ObjtbConfigDATA.conLang = lang.Trim();
                    ObjtbConfigDATA.viewNews = string.Join(",", viewNews);
                    ObjtbConfigDATA.viewProducts = string.Join(",", viewProducts);
                    ObjtbConfigDATA.viewBanner = string.Join(",", viewBanner);
                    ObjtbConfigDATA.viewSocial = string.Join(",", viewSocial);
                    ObjtbConfigDATA.viewPayment = string.Join(",", viewPayment);
                    ObjtbConfigDATA.pageError = ckePageError.Text;
                    ObjtbConfigDATA.Field5 = chkLogin.Checked ? "1" : "0";
                    ObjtbConfigDATA.livechat = txtlivebox.Text;
                    GlobalClass.conCopyright = fckFooter.Text;
                    GlobalClass.Field1 = txth1.Text;
                    GlobalClass.Field2 = CKEField2.Text;
                    GlobalClass.Field4 = CKEField4.Text;
                    if (tbConfigDB.tbConfig_Add(ObjtbConfigDATA))
                    {
                        tbConfig con = db.tbConfigs.FirstOrDefault(x => x.conLang == lang);
                        con.conTelKyThuat = txtSDTKyThuat.Text;
                        con.conPopup = chkPopup.Checked ? 1 : 0;
                        con.conPopup2 = chkPopup2.Checked ? 1 : 0;
                        db.SubmitChanges();
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới cấu hình thành công !!";
                    }
                }
                else
                {
                    if (txtId.Value.Length > 0)
                    {
                        List<tbConfigDATA> list = tbConfigDB.tbConfig_GetID(txtId.Value);
                        if (list.Count > 0)
                        {
                            list[0].conid = txtId.Value;
                            list[0].conCompany = txtTencoquan.Text;
                            list[0].conAddress = txtDiachi.Text;
                            list[0].conTel = txtDienthoai.Text;
                            list[0].conFax = txtFax.Text;
                            list[0].conWebsite = txtWebsite.Text;
                            list[0].conMail_Method = txtPhuongthucguimail.Text;
                            list[0].conMail_Smtp = txtMaychugiumail.Text;
                            list[0].conMail_Port = txtCongguimail.Text;
                            list[0].conMail_Info = txtEmaillienhe.Text;
                            list[0].conMail_Noreply = txtEmailgui.Text;
                            if (txtPasswordmail.Text != "")
                            {
                                list[0].conMail_Pass = txtPasswordmail.Text;
                            }
                            else
                            {
                                list[0].conMail_Pass = list[0].conMail_Pass;
                            }
                            list[0].conEditorial = txtImage.Text;
                            list[0].conContact = txtdongchay.Text;
                            list[0].conContact1 = fckNguoilienhe.Text;
                            list[0].conCopyright = fckFooter.Text;
                            list[0].catId = ddlStyleNews.SelectedValue;
                            list[0].conTitle = txtTitle.Text;
                            list[0].conKeywords = txtKey.Text;
                            list[0].conDescription = txtDesc.Text;
                            list[0].conLang = lang.Trim();
                            list[0].viewNews = string.Join(",", viewNews);
                            list[0].viewProducts = string.Join(",", viewProducts);
                            list[0].viewBanner = string.Join(",", viewBanner);
                            list[0].viewSocial = string.Join(",", viewSocial);
                            list[0].viewPayment = string.Join(",", viewPayment);
                            list[0].pageError = ckePageError.Text;
                            list[0].Field5 = chkLogin.Checked ? "1" : "0";
                            list[0].livechat = txtlivebox.Text;
                            GlobalClass.conCopyright = fckFooter.Text;
                            GlobalClass.Field1 = txth1.Text;
                            GlobalClass.Field2 = CKEField2.Text;

                            GlobalClass.Field4 = CKEField4.Text;

                            if (tbConfigDB.tbConfig_Update(list[0]))
                            {
                                tbConfig con = db.tbConfigs.FirstOrDefault(x => x.conLang == lang && x.conid == int.Parse(txtId.Value));
                                con.conTelKyThuat = txtSDTKyThuat.Text;
                                con.conPopup = chkPopup.Checked ? 1 : 0;
                                con.conPopup2 = chkPopup2.Checked ? 1 : 0;
                                db.SubmitChanges();
                                pnlErr.Visible = true;
                                ltrErr.Text = "Cập nhật cấu hình thành công !!";
                            }
                        }
                    }
                }
                //Update = linq
                bool updateConfig = false;
                
                if (GlobalClass.ProductInline != txtproductInLine.Text.Trim())
                {
                    updateConfig = true;
                    MyWeb.Common.GlobalClass.SetAppSettingKey("product_in_line", txtproductInLine.Text.Trim());
                }
                if (GlobalClass.Google_Analytics_Id != txtGoogleAnalytics.Text.Trim())
                {
                    updateConfig = true;
                    MyWeb.Common.GlobalClass.SetAppSettingKey("google_analytics_id", txtGoogleAnalytics.Text.Trim());
                }
             
                if (updateConfig)
                {
                    BindConfig();
                }
                tbConfig conf = db.tbConfigs.FirstOrDefault(s => s.conLang.Trim().ToLower() == lang);
                if (conf != null)
                {
                    conf.Field1 = txth1.Text;
                    conf.Field2 = CKEField2.Text;
                    conf.Field4 = CKEField4.Text;
                    conf.Field5 = chkLogin.Checked ? "1" : "0";
                    db.SubmitChanges();
                }
                BindConfig();
            }
            MyWeb.Global.LoadConfig(lang);
            BindConfig();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            string _url = Request.Url.AbsoluteUri;
            Response.Redirect(_url);
        }

        protected void btnSiteMap_Click(object sender, EventArgs e)
        {
            Response.ContentType = "text/xml";
            string text = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + Environment.NewLine;
            text += "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">" + Environment.NewLine;

            //Home page
            text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>1.00</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;

            //Product
            IEnumerable<tbProduct> dbpro = db.tbProducts.Where(s => s.proLang == lang && s.proActive == 1).OrderByDescending(s => s.proId).OrderByDescending(s => s.proId);
            foreach (tbProduct p in dbpro)
            {
                string catId = p.catId.Split(Convert.ToChar(","))[0];
                if (MyWeb.Common.StringClass.Check(catId))
                    text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.proTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + productPriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            //News
            IEnumerable<tbNew> dbnew = db.tbNews.Where(s => s.newLang == lang && s.newActive == 0).OrderByDescending(s => s.newid);
            foreach (tbNew n in dbnew)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + n.newTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + newsPriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            //Page
            IEnumerable<tbPage> page = db.tbPages.Where(s => s.pagType == 1 && s.pagLang == lang && s.pagActive == 1).OrderByDescending(s => s.pagId);
            foreach (tbPage p in page)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.pagTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }
            //sản phẩm
            IEnumerable<tbPage> page1 = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagLang == lang && s.pagActive == 1).OrderByDescending(s => s.pagId);
            foreach (tbPage p in page1)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.pagTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }
            //News
            IEnumerable<tbPage> page2 = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GN) && s.pagLang == lang && s.pagActive == 1).OrderByDescending(s => s.pagId);
            foreach (tbPage p in page2)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + p.pagTagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            //Library
            IEnumerable<Library> library = db.Libraries.Where(s => s.Lang == lang && s.Active == 1).OrderByDescending(s => s.Id);
            foreach (Library l in library)
            {
                text += "<url>" + Environment.NewLine + "\t<loc>http://" + Request.Url.Host + "/" + l.TagName + ".html</loc>" + Environment.NewLine + "\t<changefreq>" + changeFreq + "</changefreq>" + Environment.NewLine + "\t<priority>" + pagePriority + "</priority>" + Environment.NewLine + "</url>" + Environment.NewLine;
            }

            text += "</urlset>";

            //Create sitemap.xml file
            string pathfile = Server.MapPath(Request.ApplicationPath.TrimEnd('/') + "/sitemap.xml");
            try
            {
                FileStream fs = null;
                if (!File.Exists(pathfile))
                {
                    using (fs = File.Create(pathfile)) { }
                }
                using (StreamWriter sw = new StreamWriter(pathfile))
                {
                    sw.Write(text);
                }

                string _url = Request.Url.AbsoluteUri;
                Response.Redirect("/admin-system/configure.aspx");
            }
            catch
            {
                //Download sitemap
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                FileInfo file = new FileInfo(pathfile);
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + file.Name + "\"");
                byte[] data = req.DownloadData(pathfile);
                response.BinaryWrite(data);
                response.End();
            }
        }
    }
}