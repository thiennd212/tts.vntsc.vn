﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="advertiseList.ascx.cs" Inherits="MyWeb.control.panel.advertise.advertiseList" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý quảng cáo</li>
    </ol>
    <asp:UpdatePanel ID="udpAdv" runat="server" RenderMode="Block" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:Panel ID="pnlListForder" runat="server" Visible="true">
                <h1 class="page-header">Quản lý quảng cáo</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse" data-sortable-id="table-basic-1">

                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                                <h4 class="panel-title">Danh sách quảng cáo</h4>
                            </div>

                            <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false" style="border-radius: 0px !important">
                                <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>

                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-5">
                                        <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                        <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Từ khóa tìm kiếm"></asp:TextBox>
                                    </div>

                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="drlForder" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlForder_SelectedIndexChanged">
                                            <asp:ListItem Value="0"> - Vị trí quảng cáo - </asp:ListItem>
                                            <asp:ListItem Value="1">Banner</asp:ListItem>
                                            <asp:ListItem Value="2">Slide giữa trang</asp:ListItem>
                                            <asp:ListItem Value="4">Bên trái</asp:ListItem>
                                            <asp:ListItem Value="3">Bên phải</asp:ListItem>
                                            <asp:ListItem Value="6">Trượt cạnh trái</asp:ListItem>
                                            <asp:ListItem Value="7">Trượt cạnh phải</asp:ListItem>
                                            <asp:ListItem Value="8">Popup trang chủ</asp:ListItem>
                                            <asp:ListItem Value="9">Góc phải</asp:ListItem>
                                            <asp:ListItem Value="10">Slide cuối trang</asp:ListItem>
                                            <asp:ListItem Value="11">Liên kết website</asp:ListItem>                                            
                                            <asp:ListItem Value="15">Slide bên trái menu</asp:ListItem>
                                            <asp:ListItem Value="16">Quảng cáo giữa</asp:ListItem>  
                                            <asp:ListItem Value="12">Theo nhóm sản phẩm</asp:ListItem>     
                                            <asp:ListItem Value="21">Theo nhóm tin tức</asp:ListItem>
                                            <asp:ListItem Value="22">Theo nhóm hãng sản xuất</asp:ListItem>
                                            <asp:ListItem Value="23">Theo nhóm thư viện</asp:ListItem>
                                            <asp:ListItem Value="24">Theo trang liên hệ</asp:ListItem>
                                            <asp:ListItem Value="20">Theo trang nội dung</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                                <thead>
                                                    <tr>
                                                        <th width="10">
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                        </th>
                                                        <th>Ảnh</th>
                                                        <th>Tên quảng cáo</th>
                                                        <th width="100">Liên kết</th>
                                                        <th width="70">Độ rộng</th>
                                                        <th width="70">Chiều cao</th>
                                                        <th width="70">Thứ tự</th>
                                                        <th width="20"></th>
                                                        <th width="100">Công cụ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="rptListAdv" runat="server" OnItemCommand="rptListAdv_ItemCommand">
                                                        <ItemTemplate>
                                                            <tr class="even gradeC">
                                                                <td>
                                                                    <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                                    <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "advId")%>' runat="server" />
                                                                    <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"advId")%>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <%#BindImages(DataBinder.Eval(Container.DataItem, "advImage").ToString())%>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtName" runat="server" Text='<%#Eval("advName").ToString()%>' OnTextChanged="txtName_TextChanged" AutoPostBack="true" class="form-control input-sm"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <%#DataBinder.Eval(Container.DataItem, "advLink")%>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNumberWidth" runat="server" Text='<%#Eval("advWidth").ToString()%>' OnTextChanged="txtNumberWidth_TextChanged" AutoPostBack="true" class="form-control input-sm" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNumberHight" runat="server" Text='<%#Eval("advHeight").ToString()%>' OnTextChanged="txtNumberHight_TextChanged" AutoPostBack="true" class="form-control input-sm" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNumberOrder" runat="server" Text='<%#Eval("advOrd").ToString()%>' OnTextChanged="txtNumberOrder_TextChanged" AutoPostBack="true" class="form-control input-sm" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"advId")%>' CommandName="Active" class='<%#ShowActiveClass(DataBinder.Eval(Container.DataItem, "advActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "advActive").ToString())%></asp:LinkButton>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"advId")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                                    <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"advId")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row dataTables_wrapper">
                                    <div class="col-sm-5">
                                        <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                            <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                        </div>
                                    </div>

                                    <div class="col-sm-7">
                                        <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                            <ul class="pagination">
                                                <li id="data-table_previous" class="paginate_button previous disabled">
                                                    <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                                </li>
                                                <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                        <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li id="data-table_next" class="paginate_button next">
                                                    <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
                <h1 class="page-header">Thêm/sửa quảng cáo</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                                <h4 class="panel-title">Thêm/sửa quảng cáo</h4>
                            </div>
                            <div class="panel-body panel-form">
                                <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                                    <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                        <button class="close" data-dismiss="alert" type="button">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                                    </div>
                                </asp:Panel>
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Vị trí quảng cáo:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlPosition" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged">
                                                <asp:ListItem Value="0"> - Vị trí quảng cáo - </asp:ListItem>
                                                <asp:ListItem Value="1">Banner</asp:ListItem>
                                                <asp:ListItem Value="2">Slide giữa trang</asp:ListItem>
                                                <asp:ListItem Value="4">Bên trái</asp:ListItem>
                                                <asp:ListItem Value="3">Bên phải</asp:ListItem>
                                                <asp:ListItem Value="6">Trượt cạnh trái</asp:ListItem>
                                                <asp:ListItem Value="7">Trượt cạnh phải</asp:ListItem>
                                                <asp:ListItem Value="8">Popup trang chủ</asp:ListItem>
                                                <asp:ListItem Value="9">Góc phải</asp:ListItem>
                                                <asp:ListItem Value="10">Slide cuối trang</asp:ListItem>
                                                <asp:ListItem Value="11">Liên kết website</asp:ListItem>                                                
                                                <asp:ListItem Value="15">Slide bên trái menu</asp:ListItem>
                                                <asp:ListItem Value="16">Quảng cáo giữa</asp:ListItem>
                                                <asp:ListItem Value="12">Theo nhóm sản phẩm</asp:ListItem>
                                                <asp:ListItem Value="21">Theo nhóm tin tức</asp:ListItem>
                                                <asp:ListItem Value="22">Theo nhóm hãng sản xuất</asp:ListItem>
                                                <asp:ListItem Value="23">Theo nhóm thư viện</asp:ListItem>  
                                                <asp:ListItem Value="24">Theo trang liên hệ</asp:ListItem>
                                                <asp:ListItem Value="20">Theo trang nội dung</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hidID" runat="server" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="pnModule" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Nhóm sản phẩm:</label>
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="chkCat" DataValueField="pagId" DataTextField="pagName" RepeatColumns="4" Width="100%" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="1"/>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnCatNew" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Nhóm tin tức:</label>
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="chkNew" DataValueField="pagId" DataTextField="pagName" RepeatColumns="4" Width="100%" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="1"/>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="pnMan" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Nhóm hãng sản xuất:</label>
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="chkMan" DataValueField="pagId" DataTextField="pagName" RepeatColumns="4" Width="100%" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="1"/>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnLib" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Nhóm thư viện:</label>
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="chkLib" DataValueField="pagId" DataTextField="pagName" RepeatColumns="4" Width="100%" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="1"/>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="pnPage" runat="server" Visible="false">
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Menu hiển thị:</label>
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="chkPage" DataValueField="pagId" DataTextField="pagName" RepeatColumns="4" Width="100%" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="1"/>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tên quảng cáo:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtTen" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh đại diện:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<% =txtImage.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Độ rộng ảnh:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtDorong" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Chiều cao ảnh:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtChieucao" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Trang liên kết:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtTranglienket" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Kiểu xuất hiện:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="drlKieuxuathien" runat="server" class="form-control">
                                                <asp:ListItem Value="_blank">Trang mới</asp:ListItem>
                                                <asp:ListItem Value="_self">Cùng trang</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Nội dung:</label>
                                        <div class="col-md-7">
                                            <%--<asp:TextBox ID="txtNoidung" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>--%>
                                            <CKEditor:CKEditorControl ID="txtNoidung" runat="server"></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Thứ tự:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtThuTu" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Hiển thị:</label>
                                        <div class="col-md-7">
                                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />Kích hoạt
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Kiểu hiển thị:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="drKieuHienThi" runat="server" class="form-control">
                                                <asp:ListItem Value="1">Chỉ xuất hiện trang chủ 1 lần</asp:ListItem>
                                                <asp:ListItem Value="_self">Cùng trang</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2"></label>
                                        <div class="col-md-7">
                                            <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                            <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>

