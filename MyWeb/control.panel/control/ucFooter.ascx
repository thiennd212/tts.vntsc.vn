﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFooter.ascx.cs" Inherits="MyWeb.control.panel.control.ucFooter" %>
<div id="footer" class="footer"> © <% = DateTime.Now.Year %> www.vntsc.vn All Rights Reserved </div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>