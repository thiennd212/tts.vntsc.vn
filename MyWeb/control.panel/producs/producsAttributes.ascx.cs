﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.producs
{
    public partial class producsAttributes : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); } if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }

            if (!IsPostBack)
            {
                BindForder();
                BindData();
            }

        }

        void BindData()
        {
            IEnumerable<tbAttribute> _list = db.tbAttributes.Where(a => a.Lang == lang && a.Type == 1).OrderBy(a => a.Level);

            if (txtSearch.Text != "")
            {
                _list = _list.Where(s => s.Name.Contains(txtSearch.Text)).ToList();
            }

            if (_list.Count() > 0)
            {
                recordCount = _list.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _list.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        protected string BindView(string name, string treecode, string img)
        {
            string str = "<i class=\"ion-ios-folder fa-2x text-inverse icon-forder\"></i>";
            if (treecode.ToString().Trim().Length > 50)
            {
                str = "<span class='icon-forder' style='margin-left:100px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 45)
            {
                str = "<span class='icon-forder' style='margin-left:90px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 40)
            {
                str = "<span class='icon-forder' style='margin-left:80px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 35)
            {
                str = "<span class='icon-forder' style='margin-left:70px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 30)
            {
                str = "<span class='icon-forder' style='margin-left:60px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 25)
            {
                str = "<span class='icon-forder' style='margin-left:50px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 20)
            {
                str = "<span class='icon-forder' style='margin-left:40px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i><span>";
            }
            else if (treecode.ToString().Trim().Length > 15)
            {
                str = "<span class='icon-forder' style='margin-left:30px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 10)
            {
                str = "<span class='icon-forder' style='margin-left:20px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 5)
            {
                str = "<span class='icon-forder' style='margin-left:10px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else
            {
                str = str;
            }
            return str = str + "&nbsp;" + name;
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            BindForder();
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        try
                        {
                            HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                            var data = db.tbAttributes.Where(u => u.Id == Convert.ToInt32(hidID.Value)).ToList();
                            if (data.Count > 0)
                            {
                                for (int m = 0; m < data.Count; m++)
                                {
                                    var levelcon = data[m].Level;
                                    var datacon = db.tbAttributes.Where(u => u.Level.Substring(0, levelcon.Length) == levelcon).ToList();
                                    for (int j = 0; j < datacon.Count; j++)
                                    {
                                        db.tbAttributes.DeleteOnSubmit(datacon[j]);
                                        db.SubmitChanges();

                                    }
                                }
                            }
                        }
                        catch { }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void txtNameForder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNameForder = (TextBox)sender;
            var blbID = (Label)txtNameForder.FindControl("lblID");
            tbAttribute list = db.tbAttributes.FirstOrDefault(s => s.Id == Int32.Parse(blbID.Text));
            if (list != null)
            {
                list.Name = blbID.Text;
                list.Name = txtNameForder.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên thuộc tính thành công !";
        }

        protected void txtOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtOrder = (TextBox)sender;
            var blbID = (Label)txtOrder.FindControl("lblID");
            tbAttribute list = db.tbAttributes.FirstOrDefault(s => s.Id == Int32.Parse(blbID.Text));
            if (list != null)
            {
                list.Ord = int.Parse(txtOrder.Text);
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        void BindForder()
        {
            string str = "";
            IEnumerable<tbAttribute> list = db.tbAttributes.Where(a => a.Lang == lang && a.Type == 1).OrderBy(a => a.Level);
            drlChuyenmuc.Items.Clear();
            drlChuyenmuc.Items.Add(new ListItem("- Nhóm cha -", ""));
            if (list.Count() > 0)
            {
                foreach (tbAttribute lst in list)
                {
                    str = "";
                    for (int j = 1; j < lst.Level.Length / 5; j++)
                    {
                        str = str + "---";
                    }
                    drlChuyenmuc.Items.Add(new ListItem(str.ToString() + lst.Name, lst.Level));
                }
            }

            try { drlChuyenmuc.SelectedValue = hidLevel.Value; }
            catch { }
            list = null;
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        #region[Resetcontrol]
        void Resetcontrol()
        {
            txtTenchuyenmuc.Text = "";
            txtImage.Text = "";
            chkKichhoat.Checked = false;
            txtImage.Text = "";
            hidID.Value = "";
            hidLevel.Value = "";
            txtCode.Text = "";
        }
        #endregion

        protected bool Validation()
        {
            if (txtTenchuyenmuc.Text == "") { ltrErr2.Text = "Chưa nhập tên tính năng !"; txtTenchuyenmuc.Focus(); pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidLevel.Value = "";
            hidID.Value = "";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            tbAttribute obj = db.tbAttributes.FirstOrDefault(s => s.Id == Int32.Parse(strID));
            switch (e.CommandName.ToString())
            {
                case "Active":
                    obj.Id = int.Parse(strID);
                    if (obj.Active == 0)
                    {
                        obj.Active = 1;
                    }
                    else { obj.Active = 0; }
                    db.SubmitChanges();

                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Del":
                    var data = db.tbAttributes.Where(u => u.Id == Convert.ToInt32(strID)).ToList();
                    if (data.Count > 0)
                    {
                        for (int i = 0; i < data.Count; i++)
                        {
                            var levelcon = data[i].Level;
                            var datacon = db.tbAttributes.Where(u => u.Level.Substring(0, levelcon.Length) == levelcon).ToList();
                            for (int j = 0; j < datacon.Count; j++)
                            {
                                db.tbAttributes.DeleteOnSubmit(datacon[j]);
                                db.SubmitChanges();

                            }
                        }
                    }

                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công nhóm chuyên mục !";
                    BindForder();
                    BindData();
                    break;
                case "Edit":
                    BindForder();
                    hidID.Value = strID;
                    hidLevel.Value = obj.Level.Substring(0, obj.Level.Length - 5);
                    drlChuyenmuc.SelectedValue = hidLevel.Value;
                    txtTenchuyenmuc.Text = obj.Name;
                    txtCode.Text = obj.Code;
                    txtThuTu.Text = obj.Ord.ToString();
                    txtImage.Text = obj.Image;
                    txtCode.Text = obj.Code;
                    txtTitle.Text = obj.MetaTitle;
                    txtDesscription.Text = obj.MetaDescription;
                    txtKeyword.Text = obj.MetaKey;
                    if (obj.Image.Length > 0)
                    {
                        txtImage.Text = obj.Image.ToString();
                    }
                    if (obj.Active == 1)
                    {
                        chkKichhoat.Checked = true;
                    }
                    else
                    {
                        chkKichhoat.Checked = false;
                    }

                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Add":
                    BindForder();
                    tbAttribute dbCats = db.tbAttributes.FirstOrDefault(s => s.Id == Int32.Parse(strID));
                    Resetcontrol();
                    hidLevel.Value = dbCats.Level;
                    drlChuyenmuc.SelectedValue = hidLevel.Value;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string scatActive = "";
                if (chkKichhoat.Checked == true) { scatActive = "1"; } else { scatActive = "0"; }
                string strID = hidID.Value;
                string sgrnlevel = hidLevel.Value;
                if (drlChuyenmuc.SelectedValue != sgrnlevel)
                    sgrnlevel = drlChuyenmuc.SelectedValue;

                if (strID.Length == 0)
                {
                    tbAttribute att = new tbAttribute();
                    att.Name = txtTenchuyenmuc.Text.Trim();
                    att.Code = txtCode.Text;
                    att.Image = txtImage.Text;
                    if (sgrnlevel.Length > 0)
                    {
                        att.Level = sgrnlevel + "00000";
                    }
                    else
                    {
                        att.Level = "00000";
                    }

                    att.Ord = Int32.Parse(txtThuTu.Text);
                    att.Active = short.Parse(scatActive);
                    att.Lang = lang;
                    att.Type = 1;
                    att.MetaKey = txtKeyword.Text;
                    att.MetaTitle = txtTitle.Text;
                    att.MetaDescription = txtDesscription.Text;
                    db.tbAttributes.InsertOnSubmit(att);
                    db.SubmitChanges();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Thêm mới thuộc tính thành công !";
                    BindData();
                    Resetcontrol();
                    hidLevel.Value = "";
                    pnlListForder.Visible = true;
                    pnlAddForder.Visible = false;
                }
                else
                {
                    tbAttribute att = db.tbAttributes.FirstOrDefault(a => a.Id == Int32.Parse(hidID.Value));
                    att.Name = txtTenchuyenmuc.Text.Trim();
                    att.Image = txtImage.Text;
                    att.Level = sgrnlevel + "00000";
                    att.Ord = Int32.Parse(txtThuTu.Text);
                    att.Code = txtCode.Text;
                    att.Active = short.Parse(scatActive);
                    att.Lang = lang;
                    att.Type = 1;
                    att.MetaKey = txtKeyword.Text;
                    att.MetaTitle = txtTitle.Text;
                    att.MetaDescription = txtDesscription.Text;
                    db.SubmitChanges();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật thuộc tính thành công !";
                    hidID.Value = "";
                    hidLevel.Value = "";
                    Resetcontrol();
                    BindData();
                    pnlListForder.Visible = true;
                    pnlAddForder.Visible = false;

                }
            }
        }
    }
}