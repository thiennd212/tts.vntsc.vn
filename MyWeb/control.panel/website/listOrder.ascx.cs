﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb;

namespace MyWeb.control.panel.website
{
    public partial class listOrder : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            string type = Request["type"] != null ? Request["type"].ToString() : "";
            string strWhere = "";

            strWhere = " AND shop.lang ='" + lang + "'";

            if (type == "listnew")
            {
                strWhere = " AND (status IS NULL OR status=1)";
            }
            else if (type == "listSuccess")
            {
                strWhere = " AND status=10";
            }

            if (txtSearch.Text != "")
            {
                strWhere += " AND (mail LIKE '%" + txtSearch.Text.Trim() + "%' OR  cus.phone LIKE '%" + txtSearch.Text.Trim() + "%' OR dbo.FCCH_ReTextUnicode(cus.name)  LIKE N'%" + txtSearch.Text.Trim() + "%' OR shop.proname LIKE N'%" + txtSearch.Text + "%')";
            }

            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtEndDate.Text))
            {
                string strNewSDate = txtStartDate.Text.Split(' ')[0] + " 12:00:00";
                string strNewEDate = txtEndDate.Text.Split(' ')[0] + " 12:00:00";
                strWhere += " AND cus.id in (select cus_id from  tbShopingcart s  where s.createdate between '" + DateTime.ParseExact(strNewSDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") +
              "' AND '" + DateTime.ParseExact(strNewEDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") + "' )";
            }

            if (drlSortBy.SelectedValue != "0")
            {
                if (drlSortBy.SelectedValue == "name")
                {
                    strWhere += "  ORDER BY name";
                }
                else if (drlSortBy.SelectedValue == "namedesc")
                {
                    strWhere += "  ORDER BY name DESC";
                }
                else if (drlSortBy.SelectedValue == "price")
                {
                    strWhere += "  ORDER BY totalmoney";
                }
                else if (drlSortBy.SelectedValue == "pricedesc")
                {
                    strWhere += "  ORDER BY totalmoney DESC";
                }
            }

            var _listForder = tbCustomersDB.tbCustomers_WhereByLinq(strWhere);

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected string FormatDate(object date)
        {
            if (date.ToString().Trim().Length > 0)
            {
                if (DateTime.Parse(date.ToString()).Year != 1900)
                {
                    DateTime dNgay = Convert.ToDateTime(date.ToString());
                    return ((DateTime)dNgay).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public string GetOrderCode(string ordId)
        {
            var ord = db.tbCustomers.FirstOrDefault(x => x.id == int.Parse(ordId));
            if (ord != null)
                return ord.OrderCode;
            else
                return "";
        }

        public static string NumberFormat(string so)
        {
            Double giatri = 0;
            if (so.ToString().Trim().Length > 0)
            {
                giatri = Double.Parse(so.ToString());
                so = giatri.ToString();
                string dp = display_so(so);
                string p1 = "", p2 = "";
                if (dp.IndexOf(".") > -1)
                {
                    string[] s = dp.Split(Convert.ToChar("."));
                    p1 = s[0];
                    p2 = s[1];
                }
                else
                {
                    p1 = dp;
                }
                if (p1.Length > 3)
                {
                    dp = "";
                    int count = 0;
                    for (int i = p1.Length - 1; i > -1; i--)
                    {
                        count++;
                        dp = p1[i].ToString() + dp;
                        if (i < p1.Length - 1 && count % 3 == 0 && i > 0)
                        {
                            dp = "." + dp;
                        }


                    }
                    if (p2.Length > 0)
                        dp = dp + "." + p2;
                }

                return dp;
            }
            else
            {
                return "";
            }
        }

        static string display_so(string so)
        {
            string pr = "";
            if (so.Length > 5)
            {
                string re = "";
                if (so.IndexOf(".") > -1)
                {
                    string[] s = so.Split(Convert.ToChar("."));
                    re = s[0];
                    if (s[1].Length > 2)
                    {
                        re += "." + s[1].Substring(0, 2);
                    }
                    else
                    {
                        re += "." + s[1];
                    }
                }
                else
                {
                    return so;
                }

                return re;
            }
            else
            {
                return so;
            }
            return so;
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    tbCustomersDB.tbCustomers_Delete(strID);
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công !";
                    break;
                case "Edit":
                    List<tbCustomersDATA> list = tbCustomersDB.tbCustomers_GetByID(strID);
                    hidID.Value = strID;
                    lblTenKH.Text = list[0].name;
                    lblDiachi.Text = list[0].Address;
                    lblDienthoai.Text = list[0].phone;
                    lblNoiduingYC.Text = list[0].detail;
                    lblSotien.Text = NumberFormat(list[0].totalmoney.ToString()) + " vnđ";
                    lblDienthoaicodinh.Text = list[0].Tell;
                    lblHinhthucthanhtoan.Text = list[0].Payment;
                    var ordCus = db.tbCustomers.FirstOrDefault(x => x.id == int.Parse(strID));
                    txtNoiDungKhac.Text = ordCus.Content;
                    try
                    {
                        if (list[0].status == null)
                        {
                            drlStutus.SelectedValue = "1";
                        }
                        else
                        {
                            drlStutus.SelectedValue = list[0].status;
                        }
                    }
                    catch{
                    }
                    List<tbShopingcartDATA> listcart = tbShopingcartDB.tbShopingcart_GetBycus_id(strID);
                    rptProList.DataSource = listcart;
                    rptProList.DataBind();
                    for (int i = 0; i < listcart.Count; i++)
                    {
                        Label lblorder = (Label)rptProList.Items[i].FindControl("lblorder");
                        int k = (i + 1);
                        lblorder.Text = k.ToString();
                    }
                    listcart.Clear();
                    listcart = null;

                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (drlStutus.SelectedValue != "0")
            {
                string strID = hidID.Value;
                List<tbCustomersDATA> list = tbCustomersDB.tbCustomers_GetByID(strID);
                list[0].status = drlStutus.SelectedValue;
                if (tbCustomersDB.tbCustomers_UpdateStatus(list[0]))
                {
                    tbCustomer up = db.tbCustomers.FirstOrDefault(x => x.id == int.Parse(strID));
                    up.Content = txtNoiDungKhac.Text;
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    { }

                    ltrErr.Text = "Cập nhật thành công !";
                    pnlErr.Visible = true;
                    BindData();
                    hidID.Value = "";
                    pnlListForder.Visible = true;
                    pnlAddForder.Visible = false;
                }

            }
            else
            {
                BindData();
                hidID.Value = "";
                pnlListForder.Visible = true;
                pnlAddForder.Visible = false;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            hidID.Value = "";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void drlSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected string Actives(string actives)
        {
            string str = "";
            if (actives == null || actives == "1" || actives == "")
            {
                str = "Mới gửi";
            }
            else if (int.Parse(actives) > 1 && int.Parse(actives) < 10)
            {
                str = "Đang xử lý";
            }
            else if (actives == "10")
            {
                str = "Thành công";
            }

            return str;
        }
    }
}