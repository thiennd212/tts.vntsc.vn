﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class homePage : System.Web.UI.UserControl
    {
        private string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            //Product
            List<tbProductDATA> _result = new List<tbProductDATA>();
            int intCount = 0;
            _result = tbProductDB.getProductListByAdmin(1, ref intCount, 5, 0, " AND LOWER(proLang) = '" + lang + "'", "proId DESC");

            rptProductList.DataSource = _result.ToList();
            rptProductList.DataBind();

            for (int i = 0; i < _result.Count; i++)
            {
                Label lblorder = (Label)rptProductList.Items[i].FindControl("lblorder");
                int k = (i + 1);
                lblorder.Text = k.ToString();
            }

            recordCount = (int)intCount;
            ltrNumberPro.Text = recordCount.ToString();

            //Khuyễn mại
            List<Mail_khuyenmai> _listForder = db.Mail_khuyenmais.OrderBy(s => s.id).ToList();
            ltrNumberEmail.Text = _listForder.Count().ToString();

            //Đơn hàng
            var _listOrder = tbCustomersDB.tbCustomers_WhereByLinq("AND shop.lang ='" + lang + "'");
            ltrNumberOrder.Text = _listOrder.Count().ToString();

            string strWhere = "";
            strWhere = " AND shop.lang ='" + lang + "'";
            strWhere = " AND (status IS NULL OR status=1)";
            var _listOrderNew = tbCustomersDB.tbCustomers_WhereByLinq(strWhere);
            var resultO = _listOrderNew.Take(5);
            rptFolderList.DataSource = resultO;
            rptFolderList.DataBind();


            //Tin tuc
            List<tbNewsDATA> _listNews = tbNewsDB.tbNews_GetByAll(lang);
            if (_listNews.Count() > 0)
            {
                ltrNews.Text = _listNews.Count().ToString();
            }
            var result = _listNews.Take(5);
            rptNewsList.DataSource = result;
            rptNewsList.DataBind();
            for (int i = 0; i < result.Count(); i++)
            {
                Label lblorder = (Label)rptNewsList.Items[i].FindControl("lblorder");
                int k = (i + 1);
                lblorder.Text = k.ToString();
            }


        }

        public string BindCateNameNews(string grnID)
        {
            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(grnID)).FirstOrDefault();
            return curNewType != null ? curNewType.pagName : "";
        }

        protected string FormatDate(object date)
        {
            if (date.ToString().Trim().Length > 0)
            {
                if (DateTime.Parse(date.ToString()).Year != 1900)
                {
                    DateTime dNgay = Convert.ToDateTime(date.ToString());
                    return ((DateTime)dNgay).ToString("MM/dd/yyyy hh:mm:ss");
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public string BindCateName(string strID)
        {
            string strReturn = "";
            var obj = db.tbPages.Where(u => u.pagId == Convert.ToInt32(strID)).ToList();
            if (obj.Count > 0)
            {
                strReturn = obj[0].pagName;
            }
            return strReturn;
        }

        public static string BindNumber(string strNumber)
        {
            Double giatri = 0;
            if (strNumber.ToString().Trim().Length > 0)
            {
                giatri = Double.Parse(strNumber.ToString());
                strNumber = giatri.ToString();
                string dp = display_so(strNumber);
                string p1 = "", p2 = "";
                if (dp.IndexOf(".") > -1)
                {
                    string[] s = dp.Split(Convert.ToChar("."));
                    p1 = s[0];
                    p2 = s[1];
                }
                else
                {
                    p1 = dp;
                }
                if (p1.Length > 3)
                {
                    dp = "";
                    int count = 0;
                    for (int i = p1.Length - 1; i > -1; i--)
                    {
                        count++;
                        dp = p1[i].ToString() + dp;
                        if (i < p1.Length - 1 && count % 3 == 0 && i > 0)
                        {
                            dp = "," + dp;
                        }


                    }
                    if (p2.Length > 0)
                        dp = dp + "." + p2;
                }

                return dp;
            }
            else
            {
                return "0";
            }
        }

        static string display_so(string so)
        {
            string pr = "";
            if (so.Length > 5)
            {
                string re = "";
                if (so.IndexOf(".") > -1)
                {
                    string[] s = so.Split(Convert.ToChar("."));
                    re = s[0];
                    if (s[1].Length > 2)
                    {
                        re += "." + s[1].Substring(0, 2);
                    }
                    else
                    {
                        re += "." + s[1];
                    }
                }
                else
                {
                    return so;
                }

                return re;
            }
            else
            {
                return so;
            }
            return so;
        }

        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        public static string ShowActiveClassNews(string proActive)
        {
            return proActive == "0" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        public static string ShowActiveNews(string proActive)
        {
            return proActive == "0" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void rptProductList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "DEL":
                    string tagName = db.tbProducts.Where(s => s.proId == int.Parse(strID)).FirstOrDefault().proTagName;
                    tbPage pg = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                    if (pg != null)
                    {
                        tbPageDB.tbPage_Delete(pg.pagId.ToString());
                    }
                    var comm = db.tbComments.Where(u => u.comProId == Convert.ToInt32(strID)).ToList();
                    if (comm.Count > 0)
                    {
                        for (int i = 0; i < comm.Count; i++)
                        {
                            db.tbComments.DeleteOnSubmit(comm[i]);
                            db.SubmitChanges();
                        }
                    }
                    tbProductDB.tbProduct_Delete(strID);
                    DeleteCenter(strID);
                    BindData();
                    break;
                case "Active":
                    tbProduct product = db.tbProducts.FirstOrDefault(s => s.proId == Int32.Parse(strID));
                    if (product.proActive == 0)
                    {
                        product.proActive = 1;
                    }
                    else { product.proActive = 0; }
                    db.SubmitChanges();
                    BindData();
                    break;
            }
        }
        private void DeleteCenter(string _strCatID)
        {
            IEnumerable<tbCatAtt> listcatatt = db.tbCatAtts.Where(c => c.catId == Int32.Parse(_strCatID));
            if (listcatatt.Count() > 0)
            {
                db.tbCatAtts.DeleteAllOnSubmit(listcatatt);
                db.SubmitChanges();
            }
        }

        protected void rptNewsList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    tbNew obj = db.tbNews.FirstOrDefault(s => s.newid == Int32.Parse(strID));
                    if (obj.newActive == 0) { obj.newActive = 1; } else { obj.newActive = 0; }
                    db.SubmitChanges();
                    BindData();
                    break;
                case "Del":
                    string tagName = db.tbNews.Where(s => s.newid == int.Parse(strID)).FirstOrDefault().newTagName;
                    string pagId = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault().pagId.ToString();
                    tbPageDB.tbPage_Delete(pagId);

                    var data = db.tbComments.Where(u => u.comNewId == Convert.ToInt32(strID)).ToList();
                    if (data.Count > 0)
                    {
                        for (int i = 0; i < data.Count; i++)
                        {
                            db.tbComments.DeleteOnSubmit(data[i]);
                            db.SubmitChanges();
                        }
                    }

                    tbNewsDB.tbNews_Delete(strID);
                    BindData();
                    break;
            }
        }

        public static string NumberFormat(string so)
        {
            Double giatri = 0;
            if (so.ToString().Trim().Length > 0)
            {
                giatri = Double.Parse(so.ToString());
                so = giatri.ToString();
                string dp = display_so(so);
                string p1 = "", p2 = "";
                if (dp.IndexOf(".") > -1)
                {
                    string[] s = dp.Split(Convert.ToChar("."));
                    p1 = s[0];
                    p2 = s[1];
                }
                else
                {
                    p1 = dp;
                }
                if (p1.Length > 3)
                {
                    dp = "";
                    int count = 0;
                    for (int i = p1.Length - 1; i > -1; i--)
                    {
                        count++;
                        dp = p1[i].ToString() + dp;
                        if (i < p1.Length - 1 && count % 3 == 0 && i > 0)
                        {
                            dp = "." + dp;
                        }


                    }
                    if (p2.Length > 0)
                        dp = dp + "." + p2;
                }

                return dp;
            }
            else
            {
                return "";
            }
        }

        protected string Actives(string actives)
        {
            string str = "";
            if (actives == null || actives == "1" || actives == "")
            {
                str = "Mới gửi";
            }
            else if (int.Parse(actives) > 1 && int.Parse(actives) < 10)
            {
                str = "Đang xử lý";
            }
            else if (actives == "10")
            {
                str = "Thành công";
            }

            return str;
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    tbCustomersDB.tbCustomers_Delete(strID);
                    BindData();
                    break;

            }
        }
    }
}