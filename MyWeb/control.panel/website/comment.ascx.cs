﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class comment : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindForder();
                BindData();
            }
        }

        void BindData()
        {
            List<MyWeb.tbComment> objComment = db.tbComments.OrderByDescending(s => s.comDate).ToList();

            if (drlTrangthai.SelectedValue.Trim() == "1")
            {
                objComment = objComment.Where(s => s.comNewId != null).ToList();
                if (drltp.SelectedValue.Trim() != "0" && drltp.SelectedValue.Trim() != "")
                {
                    objComment = objComment.Where(s => s.comNewId == Convert.ToInt32(drltp.SelectedValue)).ToList();
                }
            }
            else if (drlTrangthai.SelectedValue.Trim() == "2")
            {
                objComment = objComment.Where(s => s.comProId != null).ToList();
                if (drltp.SelectedValue.Trim() != "0" && drltp.SelectedValue.Trim() != "")
                {
                    objComment = objComment.Where(s => s.comProId == Convert.ToInt32(drltp.SelectedValue)).ToList();
                }
            }
            else if (drlTrangthai.SelectedValue.Trim() == "3")
            {
                objComment = objComment.Where(s => s.comNewId != null).ToList();
                if (drltp.SelectedValue.Trim() != "0" && drltp.SelectedValue.Trim() != "")
                {
                    objComment = objComment.Where(s => s.comNewId == Convert.ToInt32(drltp.SelectedValue)).ToList();
                }
            }

            if (objComment.Count() > 0)
            {
                recordCount = objComment.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = objComment.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }

        }

        void BindForder()
        {
            ddlNhom.Items.Clear();
            if (drlTrangthai.SelectedValue.Trim() == "1")
            {
                List<tbPage> list = db.tbPages.Where(s => s.pagType == 200).ToList();
                ddlNhom.Items.Add(new ListItem("-- Chọn nhóm tin --", "0"));
                foreach (var item in list)
                {
                    ddlNhom.Items.Add(new ListItem(item.pagName, item.pagId.ToString().Trim()));
                }
            }
            if (drlTrangthai.SelectedValue.Trim() == "2")
            {
                List<tbPage> list = db.tbPages.Where(s => s.pagType == 100).ToList();
                ddlNhom.Items.Add(new ListItem("-- Chọn nhóm sản phẩm --", "0"));
                foreach (var item in list)
                {
                    ddlNhom.Items.Add(new ListItem(item.pagName, item.pagId.ToString().Trim()));
                }
            }
            if (drlTrangthai.SelectedValue.Trim() == "3")
            {
                List<tbPage> list = db.tbPages.Where(s => s.pagType == 701).ToList();
                ddlNhom.Items.Add(new ListItem("-- Chọn nhóm tour --", "0"));
                foreach (var item in list)
                {
                    ddlNhom.Items.Add(new ListItem(item.pagName, item.pagId.ToString().Trim()));
                }
            }
        }

        protected void drlTrangthai_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlNhom.Visible = true;
            BindForder();
            BindData();
        }

        protected string FormatDate(object date)
        {
            if (date.ToString().Trim().Length > 0)
            {
                if (DateTime.Parse(date.ToString()).Year != 1900)
                {
                    DateTime dNgay = Convert.ToDateTime(date.ToString());
                    return ((DateTime)dNgay).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        protected string Actives(string actives)
        {
            string str = "";
            if (actives == "1")
            {
                str = "Hiển thị";
            }
            else
            {
                str = "Ẩn";
            }
            return str;
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Edit":
                    tbComment listE = db.tbComments.FirstOrDefault(s => s.comId == int.Parse(strID));
                    
                    txthoten.Text = listE.comName;
                    txtdiachi.Text = listE.comAddress;
                    txtemails.Text = listE.comEmail;
                    txtTomtat.Text = listE.comComment;
                    txtngay.Text = MyWeb.Common.DateTimeClass.ConvertDateTime(listE.comDate.ToString());
                    if (listE.comNewId != null)
                    {
                        var xx = db.tbNews.Where(u=>u.newid==listE.comNewId).FirstOrDefault();
                        txtten.Text = xx.newName;
                    }
                    else {
                        var xx = db.tbProducts.Where(u=>u.proId==listE.comProId).FirstOrDefault();
                        txtten.Text = xx.proName;

                    }
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Del":
                    tbComment coment1 = db.tbComments.FirstOrDefault(s => s.comId == int.Parse(strID));
                    db.tbComments.DeleteOnSubmit(coment1);
                    db.SubmitChanges();
                    BindData();
                    break;
                case "Active":
                    tbComment lst = db.tbComments.FirstOrDefault(s => s.comId == int.Parse(strID));;
                    if (lst != null)
                    { lst.comActive = lst.comActive == 0 ? 1 : 0; }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected void ddlNhom_SelectedIndexChanged(object sender, EventArgs e)
        {
            drltp.Visible = true;
            drltp.Items.Clear();

            if (drlTrangthai.SelectedValue.Trim() == "1")
            {
                List<tbNew> list = db.tbNews.Where(s => s.grnID == int.Parse(ddlNhom.SelectedValue)).ToList();
                drltp.Items.Add(new ListItem("--Chọn tin tức--", "0"));
                foreach (var item in list)
                {
                    drltp.Items.Add(new ListItem(item.newName, item.newid.ToString().Trim()));
                }

            }
            if (drlTrangthai.SelectedValue.Trim() == "2")
            {
                List<tbProduct> list = db.tbProducts.Where(s => s.catId == ddlNhom.SelectedValue).ToList();
                drltp.Items.Add(new ListItem("--Chọn sản phẩm--", "0"));
                foreach (var item in list)
                {
                    drltp.Items.Add(new ListItem(item.proName, item.proId.ToString().Trim()));
                }

            }
            if (drlTrangthai.SelectedValue.Trim() == "3")
            {
                int? catTourId = int.Parse(ddlNhom.SelectedValue.ToString());
                List<Tour> list = db.Tours.Where(s => s.IdTourCategory == catTourId).ToList();
                drltp.Items.Add(new ListItem("--Chọn tour--", "0"));
                foreach (var item in list)
                {
                    drltp.Items.Add(new ListItem(item.Name, item.Id.ToString().Trim()));
                }

            }

            //BindForder();
            BindData();
        }

        protected void drltp_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindForder();
            BindData();
        }

        protected string BindCate(string strID)
        {
            string strCate = "";
            tbComment obj = db.tbComments.FirstOrDefault(s => s.comId == int.Parse(strID));

            if (obj.comNewId != null)
            {
                List<tbNew> lsN = db.tbNews.Where(n => n.newid == obj.comNewId).ToList();
                if (lsN.Count() > 0)
                {
                    tbPage grn = db.tbPages.FirstOrDefault(s => s.pagId == lsN[0].grnID);
                    string linknew = "/" + lsN[0].newTagName + ".html";
                    strCate = lsN[0].newName;
                }
                else
                {
                    strCate = "Tin này không tồn tại hoặc đã bị xóa";
                }
            }
            else if (obj.comProId != null)
            {
                tbProduct pro = db.tbProducts.FirstOrDefault(s => s.proId == obj.comProId);
                if (pro != null)
                {
                    string[] proid = pro.catId.Split(',');
                    tbPage cat = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(proid[0]));
                    string linkpro = pro.proTagName + ".html";
                    strCate = pro.proName;
                }
            }

            return strCate;
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }
    }
}