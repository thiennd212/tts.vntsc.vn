﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_cart_product.aspx.cs" EnableEventValidation="false" Inherits="MyWeb.add_cart_product" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title></title>

</head>
<body>
    <form id="add_products" runat="server">        
        <asp:ScriptManager EnablePartialRendering="true" ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <asp:UpdatePanel ID="udpAdv" runat="server" RenderMode="Block" ChildrenAsTriggers="true">
            <ContentTemplate>
                <div class="box-cart-pop">
                    <div class="row-list-header">
                        Đơn hàng của bạn
                    </div>
                    <div class="row-list">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                    </div>
                    <div class="row-list">
                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th><%= MyWeb.Global.GetLangKey("cart_stt")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_product")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_amount")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_price")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_money")%></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="bodyCart">
                                <asp:Literal runat="server" ID="ltrCartBody"></asp:Literal>
                            </tbody>
                        </table>
                    </div>

                    <div class="row-list">
                        <asp:Literal ID="ltrThanhToan" runat="server"></asp:Literal>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        function UpdateProCart(id) {
            var sl = $("input.sl-" + id + "").val();
            $.ajax({
                async: false,
                type: "POST",
                url: "add_cart_product.aspx/Update",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ sl: sl, id: id }),
                success: function (response) {
                    var rs = response.d;
                    $("#bodyCart").html(rs);
                },
                failure: function (response) { }

            });
        }
        function DeleteProCart(id) {
            var cf = confirm("Bạn chắc chắn muốn xóa?");
            if (cf) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "add_cart_product.aspx/Delete",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ id: id }),
                    success: function (response) {
                        var rs = response.d;
                        $("#bodyCart").html(rs);
                    },
                    failure: function (response) { }

                });
            } else {
                alert("Error");
            }
        }
    </script>
</body>
</html>
