﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsDetail.ascx.cs" Inherits="MyWeb.views.news.ucNewsDetail" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<div class="box-news-detailt">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <asp:Literal ID="ltrNewName" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="body-detailt">  
        <asp:HiddenField runat="server" ID="hdNewId" />
        <asp:Literal runat="server" ID="ltrNewDetail"></asp:Literal>                  
    </div>
    <asp:Literal ID="ltrVideo" runat="server"></asp:Literal>
    <asp:Panel ID="pnDownload" runat="server">
        <div class="box-news-download">
            <asp:LinkButton ID="btnDownload" runat="server" OnClick="btnDownload_Click" Text="Tải về"></asp:LinkButton>
        </div>
    </asp:Panel>

    <div class="div-addthis-news">
        <div class="addthis_native_toolbox"></div>
    </div>
    <div class="box-tags-news">
        <asp:Literal ID="ltrTag" runat="server"></asp:Literal>
    </div>

    <%if (GlobalClass.commentFNews.Contains("1"))
      {%><div class="shares">
          <div class="fb-comments" data-href="<%=strUrlFace %>" data-colorscheme="<%=cf %>" data-width="<%=wf %>" data-numposts="<%=nf %>"></div>
      </div>
    <%} %>

    <div id="areaComment">        
        <asp:Literal runat="server" ID="ltrListComment"></asp:Literal>   
        <asp:Literal runat="server" ID="ltrPagingComment"></asp:Literal>
        <asp:Label runat="server" ID="lbAlter" CssClass="alterComment"></asp:Label>
        <div id="formComment">
            <div class="rows">
                <label>Tên:</label>
                <asp:TextBox runat="server" ID="txtCommentName" CssClass="inputComment"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvCommentName" ControlToValidate="txtCommentName" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="rows">
                <label>Email:</label>
                <asp:TextBox runat="server" ID="txtCommentEmail" CssClass="inputComment"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvCommentEmail" ControlToValidate="txtCommentEmail" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="rows">
                <label>Nội dung:</label>
                <asp:TextBox runat="server" ID="txtCommentContent" CssClass="inputComment" TextMode="MultiLine" Rows="5"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvCommentContent" ControlToValidate="txtCommentContent" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="rows">
                <asp:LinkButton runat="server" ID="lbtSendComment" OnClick="lbtSendComment_Click" ValidationGroup="sendComment">Gửi</asp:LinkButton>
            </div>
        </div>
    </div>

    <asp:Panel ID="pnlNewsOld" runat="server" Visible="false">
        <div class="clearfix">
            <div class="cate-header sub-top">
                <div class="txt-name-sub">
                    <%= MyWeb.Global.GetLangKey("news_related")%>
                </div>
            </div>
            <asp:Literal runat="server" ID="ltrTinlienquan"></asp:Literal>
            <ul class="ul-orther-news">
                <asp:Repeater ID="rptNewsOld" runat="server">
                    <ItemTemplate>
                        <li><a href='<%#strLink + (MyWeb.Common.StringClass.NameToTag(Eval("newTagname").ToString()))+".html" %>'><%#DataBinder.Eval(Container.DataItem, "newName")%> (<%# String.Format("{0:dd/MM/yyyy HH:mm:ss}", Eval("newDate"))%>)</a></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnNewsOrther" runat="server" Visible="false">
        <div class="clearfix">
            <div class="cate-header sub-top">
                <div class="txt-name-sub">
                    <%= MyWeb.Global.GetLangKey("news_other")%>
                </div>
            </div>
            <asp:Literal runat="server" ID="lrtTinLienQuanKhac"></asp:Literal>
            <ul class="ul-orther-news">
                <asp:Repeater ID="rptNewsOrther" runat="server">
                    <ItemTemplate>
                        <li><a href='<%#strLink + (MyWeb.Common.StringClass.NameToTag(Eval("newTagname").ToString()))+".html" %>'><%#DataBinder.Eval(Container.DataItem, "newName")%> (<%#String.Format("{0:dd/MM/yyyy HH:mm:ss}", Eval("newDate"))%>) </a></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </asp:Panel>
</div>
