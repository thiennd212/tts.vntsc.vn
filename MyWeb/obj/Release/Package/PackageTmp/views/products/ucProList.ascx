﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProList.ascx.cs" Inherits="MyWeb.views.products.ucProList" %>
<div class="box-pro-sub">
    <div class="cate-header sub-top">
        <div class="txt-name-sub" style="display: none">
            <asp:Literal ID="ltrName" runat="server"></asp:Literal>
        </div>
        <asp:Literal runat="server" ID="ltrGroupDes"></asp:Literal>
        <asp:Literal ID="ltrViewsContainer" runat="server"></asp:Literal>
        <div style="display: none">
            <asp:Panel ID="pnlDrop" runat="server">
                <div class="drop-fillter">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <%= MyWeb.Global.GetLangKey("orderby_pro") %> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li>
                                <asp:LinkButton ID="btnOderAZ" runat="server" OnClick="btnOderAZ_Click"><%=MyWeb.Global.GetLangKey("sort_by_name") %></asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="btnOderZA" runat="server" OnClick="btnOderZA_Click"><%=MyWeb.Global.GetLangKey("sort_by_namedesc") %></asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="btnOderPriceAug" runat="server" OnClick="btnOderPriceAug_Click"><%=MyWeb.Global.GetLangKey("sort_by_price") %></asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="btnOderPriceGar" runat="server" OnClick="btnOderPriceGar_Click"><%=MyWeb.Global.GetLangKey("sort_by_pricedesc") %></asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="list-cat-child col-md-12">
            <asp:Literal ID="ltrListCatChild" runat="server"></asp:Literal>
        </div>
    </div>
    <!--end sp-->
    <div id="loadFillter">
        <asp:Literal ID="ltrProList" runat="server"></asp:Literal>
        <div class="clearfix">
            <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
        </div>

        <asp:Literal ID="ltrCateView" runat="server"></asp:Literal>
    </div>
</div>
