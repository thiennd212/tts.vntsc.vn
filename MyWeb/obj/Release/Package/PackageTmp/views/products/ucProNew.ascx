﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProNew.ascx.cs" Inherits="MyWeb.views.products.ucProNew" %>

<%if (showAdv)
  {%>
<div class="box-product-new">
    <div class="header">
        <asp:Literal ID="ltrName" runat="server"></asp:Literal>
    </div>
    <ul class="product-list">
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
    </ul>
    <% if (showPage)
       { %>
    <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
    <% } %>
</div>
<%} %>

