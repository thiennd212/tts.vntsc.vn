﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTourSale.ascx.cs" Inherits="MyWeb.views.Tour.ucTourSale" %>
<div id="listTourSale" class="listTour_all">
    <div class="listTour_label">
        <span>Tour khuyến mại</span>
    </div>
    <div class="listTour_list">
        <asp:Literal runat="server" ID="ltrListItem"></asp:Literal>
    </div>
</div>
