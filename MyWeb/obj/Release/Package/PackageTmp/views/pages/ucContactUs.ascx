﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucContactUs.ascx.cs" Inherits="MyWeb.views.pages.ucContactUs" %>
<%@ Register assembly="MSCaptcha" namespace="MSCaptcha" tagprefix="cc1" %>
<div class="box-sub-contact">
    <asp:Literal runat="server" ID="ltrAdv"></asp:Literal>
    <div class="cate-header">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("contact") %></div>
    </div>
    <div class="box-body-sub">
        <div class="row">
            <div class="info"><asp:Literal ID="lblConfig" runat="server"></asp:Literal></div>
        </div>
        <div class="row">
            <div class="form-group">
                <span class="view-messenger"><asp:Label ID="lblthongbao" runat="server"></asp:Label></span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_name")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtname" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_address")%>:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtdiachi" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="rfvdiachi" runat="server" ControlToValidate="txtdiachi" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_mobilephone")%>:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="rfvDienthoai" runat="server" ControlToValidate="txtDienthoai" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_phone")%>:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDienthoaicodinh" runat="server" class="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_fax")%>:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtFax" runat="server" class="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_email")%>:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_content")%>:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtnoidung" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_capcha")%>:</label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtCaptcha" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3">
                    <cc1:CaptchaControl ID="imgCaptcha" CssClass="imgCaptcha" runat="server" CaptchaBackgroundNoise="High" CaptchaLineNoise="High" />
                </div>
                <div class="col-md-3">
                    <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtCaptcha" Display="Dynamic" ErrorMessage="Nhập mã an toàn!" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-7">
                    <asp:LinkButton ID="btnSend" runat="server" OnClick="btnSend_Click"><%= MyWeb.Global.GetLangKey("contact_send") %></asp:LinkButton>
                    <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click"><%= MyWeb.Global.GetLangKey("contact_cancel")%></asp:LinkButton>
                </div>
            </div>



        </div>
    </div>
</div>
