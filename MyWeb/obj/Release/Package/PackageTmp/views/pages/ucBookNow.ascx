﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBookNow.ascx.cs" Inherits="MyWeb.views.pages.ucBookNow" %>

<div class="box-sub-book-now bookNowForm">
    <%--<div class="header bookNowTitle">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("book-now") %></div>
    </div>--%>
    <asp:UpdatePanel ID="udpBook" runat="server">
        <ContentTemplate>
            <div class="box-body-sub bookNowContent">
                <div class="row">
                    <div class="form-group MessengerBox">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <span class="view-messenger">
                                <asp:Label ID="lblthongbao" runat="server"></asp:Label></span>
                        </div>
                    </div>
                    <div class="form-group bookNowName clearfix">
                        <label class="control-label col-md-4">
                            Thông tin khách hàng(*):
                            <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtname" Display="Dynamic" ErrorMessage="" SetFocusOnError="True" ValidationGroup="bookNow1"></asp:RequiredFieldValidator></label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNowEmail clearfix">
                        <label class="control-label col-md-4">
                            Email(*):
                            <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic" ValidationGroup="bookNow1"></asp:RegularExpressionValidator></label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNowPhone clearfix">
                        <label class="control-label col-md-4">
                            Số điện thoại(*):
                            <asp:RequiredFieldValidator ID="rfvDienthoai" runat="server" ControlToValidate="txtDienthoai" Display="Dynamic" ErrorMessage="" SetFocusOnError="True" ValidationGroup="bookNow1"></asp:RequiredFieldValidator></label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNowDate clearfix">
                        <label class="control-label col-md-4">Ngày khởi hành:</label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtThoiGian" runat="server" class="form-control" data-provide="datepicker"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNowNOP clearfix">
                        <label class="control-label col-md-4">Số người:</label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtSoNguoi" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNowContent clearfix">
                        <label class="control-label col-md-4">Yêu cầu thêm (nếu có):</label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtnoidung" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNowBtn">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <asp:LinkButton ID="btnSend" class="btn btn-primary btn-md" runat="server" ValidationGroup="bookNow1" OnClick="btnSend_Click">Gửi thông tin</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<script>
    function MyFunction() {
        $("#myModal").modal("hide");
    }
    var r = {
        'special': /[\W]/g,
        'quotes': /[^0-9^]/g,
        'notnumbers': /[^a-zA]/g
    }
    function valid(o, w) {
        o.value = o.value.replace(r[w], '');
    }

    $('#txtThoiGian').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        startDate: '-3d',
    });

    function SendDataContact() {
        $("#btSend").attr("disabled", "disabled");
        var hoten = $("#txtnamecal").val();
        var diachi = $("#txtAdd").val();
        var email = $("#txtmailcal").val();
        var phone = $("#txtDienthoaical").val();
        var content = $("#txtnoidungcal").val();
        var dientich = $(".s-calculator").html();
        var loaigo = $("#hdLoaiGo").val();
        var ketqua = $(".s-result").html();
        if (hoten == "" || diachi == "" || email == "" || phone == "") {
            alert("Vui lòng nhập đầy đủ thông tin bắt buộc(*)");
        } else {
            $.ajax({
                async: false,
                type: "POST",
                url: "webService.asmx/SendRegisterCalPrice",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ hoTen: hoten, diachi: diachi, email: email, phone: phone, content: content, dientich: dientich, loaigo: loaigo, ketqua: ketqua }),
                success: function (response) {
                    console.log(response.d);
                    if (response.d == true) {
                        $('#txtnamecal').val("");
                        $("#txtAdd").val("");
                        $('#txtmailcal').val("");
                        $('#txtDienthoaical').val("");
                        $('#txtnoidungcal').val("");
                        $("#btSendCal").removeAttr("disabled");
                        $("span#thongbao").html("Bạn đã gửi thông tin thành công !!");
                    } else {
                        $("#btSendCal").removeAttr("disabled");
                        $("span#thongbao").html("Lỗi không gửi được thông tin !!");
                    }
                },
                failure: function (response) {
                    $("#btSendCal").removeAttr("disabled");
                    $("span#thongbao").html("Lỗi không gửi được thông tin !!");
                }
            });
        }
    }
</script>
