﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSearchBoxOrder.ascx.cs" Inherits="MyWeb.views.control.ucSearchBoxOrder" %>
<asp:Panel runat="server" ID="pnBox">
    <div class="box-search-order col-md-12">
        <div class="title-search-order">TRA CỨU ĐƠN HÀNG</div>
        <div class="tile-in-md">Mã đơn hàng</div>
        <div class="input-madon">
            <asp:TextBox ID="txtMaDon" runat="server" class="input-text" placeholder="Mã đơn hàng"></asp:TextBox>
        </div>
        <div class="tile-in-sdt">Số điện thoại</div>
        <div class="input-sdt">
            <asp:TextBox ID="txtSoDienThoai" runat="server" class="input-text" placeholder="Số điện thoại"></asp:TextBox>
        </div>
        <div class="input-button">
            <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" class="btn-search color-backgroud">Tra cứu đơn hàng</asp:LinkButton>
            <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" class="btn-search color-backgroud">Bỏ qua</asp:LinkButton>
        </div>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnList">
    <div class="row">
        <h4 class="panel-title">Danh sách vận đơn</h4>
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                    <thead>
                        <tr>
                            <th width="10">STT
                            </th>
                            <th width="150">Ngày tháng</th>
                            <th width="120">Mã đơn hàng</th>
                            <th width="300">Diễn giải</th>
                            <th width="150">Nơi đi</th>
                            <th width="150">Nơi đến</th>
                            <th width="100">Tình trạng</th>
                            <th>Tình trạng thanh toán</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                            <ItemTemplate>
                                <tr class="even gradeC">
                                    <td style="text-align: center">
                                        <%# Container.ItemIndex + 1 %>
                                    </td>
                                    <td>
                                        <%#ConvertDateToString(DataBinder.Eval(Container.DataItem, "NgayThang"))%>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Code")%>' CommandName="Edit" ToolTip="Xem đơn"><%#DataBinder.Eval(Container.DataItem, "Code")%></asp:LinkButton>
                                    </td>
                                    <td>
                                        <%#DataBinder.Eval(Container.DataItem, "Description")%>
                                    </td>
                                    <td>
                                        <%#DataBinder.Eval(Container.DataItem, "AddressFrom")%>
                                    </td>
                                    <td>
                                        <%#DataBinder.Eval(Container.DataItem, "AddressTo")%>
                                    </td>
                                    <td>
                                        <%#GetStatus(Eval("Status").ToString())%>
                                    </td>
                                    <td>
                                        <%#GetStatusTT(Eval("StatusTT").ToString())%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtFocus" runat="server" CssClass="textForcus"></asp:TextBox>
    <div class="row dataTables_wrapper">
        <div class="col-md-5 col_right">
            <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination">
                    <li id="data-table_previous" class="paginate_button previous disabled">
                        <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                    </li>
                    <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                        <ItemTemplate>
                            <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                            <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <li id="data-table_next" class="paginate_button next">
                        <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnDetail">
    <h3>ĐƠN HÀNG CỦA BẠN</h3>
    <div class="title-detail">Thông tin chi tiết</div>
    <div class="col-md-12 col-tt">
        <div class="row">
            <label class="col-md-3">Đơn hàng</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrDonHang" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Ngày</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrNgay" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Trạng thái</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrTrangThai" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Mã đơn hàng</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrMaDonHang" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Thông tin khác </label>
            <label class="col-md-9">
                <asp:Literal ID="ltrThongTinKhac" runat="server"></asp:Literal>
            </label>
        </div>
    </div>

    <div class="title-detail">Tóm tắt đơn hàng</div>
    <div class="col-md-12 col-tt">
        <div class="row">
            <label class="col-md-3">Hình thức thanh toán</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrHinhThuc" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Thành tiền</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrThanhTien" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Phí vận chuyển</label>
            <label class="col-md-9">
                Miễn phí
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Tổng cộng</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrTong" runat="server"></asp:Literal>
            </label>
        </div>
    </div>

    <div class="title-detail">Thông tin khách hàng</div>
    <div class="col-md-12 col-tt">
        <div class="row">
            <label class="col-md-3">Tên người nhận</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrTenNguoiNhan" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Số điện thoại</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrSoDienThoai" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Email</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrEmail" runat="server"></asp:Literal>
            </label>
        </div>
        <div class="row">
            <label class="col-md-3">Địa chỉ</label>
            <label class="col-md-9">
                <asp:Literal ID="ltrDiaChi" runat="server"></asp:Literal>
            </label>
        </div>
    </div>

    <div class="col-md-12 list-product-order">
        <div class="panel-body">
            <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Sản phẩm</th>
                        <th width="100">Đơn giá</th>
                        <th width="180">Số lượng</th>
                        <th width="100">Tổng tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptProList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="lblorder" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <%#BindImages(DataBinder.Eval(Container.DataItem, "proid").ToString())%>
                                    <%#DataBinder.Eval(Container.DataItem, "proname")%>
                                </td>
                                <td><%#NumberFormat(DataBinder.Eval(Container.DataItem, "price").ToString())%> VNĐ</td>
                                <td><%#DataBinder.Eval(Container.DataItem, "number")%></td>
                                <td><%#NumberFormat(DataBinder.Eval(Container.DataItem, "money").ToString())%> VNĐ</td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>

    <asp:LinkButton ID="btnCancelx" runat="server" OnClick="btnCancelX_Click" class="btn-search color-backgroud">Đóng</asp:LinkButton>
</asp:Panel>
