﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAdvleft.ascx.cs" Inherits="MyWeb.views.control.ucAdvleft" %>

<%if(showAdv)
  {%>
    <div class="box-adv-left">
        <div class="header"><%= MyWeb.Global.GetLangKey("partner")%></div>
        <ul class="body-adv">
            <asp:Literal ID="ltradv" runat="server"></asp:Literal>
        </ul>
    </div>
<%} %>