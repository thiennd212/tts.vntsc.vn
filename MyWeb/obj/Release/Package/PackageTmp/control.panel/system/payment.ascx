﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="payment.ascx.cs" Inherits="MyWeb.control.panel.system.payment" %>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý thanh toán</li>
    </ol>
    <h1 class="page-header">Quản lý thanh toán</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Cấu hình thanh toán</h4>
                </div>
                <div class="panel-body panel-form">
                    <asp:Panel ID="pnlErr" runat="server" Visible="false">
                        <div class="alert alert-danger fade in" style="border-radius: 0px;">
                            <button class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                    <div class="form-horizontal form-bordered">
                        <div class="form-group">
                            <asp:HiddenField ID="txtId" runat="server" />
                            <label class="control-label col-md-12" style="text-align: left !important;"><b>CẤU HÌNH CỔNG THANH TOÁN BẢO KIM</b></label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Kích hoạt bảo kim:</label>
                            <div class="col-md-7">
                                <asp:CheckBox ID="chkbaokim" runat="server" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Email thanh toán:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtemaibaokim" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Mã Merchant Site:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtmerchantsitebk" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Mật khẩu:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtmkbaokim" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-12" style="text-align: left !important;"><b>CẤU HÌNH CỔNG THANH TOÁN NGÂN LƯỢNG</b></label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Kích hoạt ngân lượng:</label>
                            <div class="col-md-7">
                                <asp:CheckBox ID="chknganluong" runat="server" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Email thanh toán:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtemailnganluong" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Mã Merchant Site:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtmerchantsitenl" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Mật khẩu:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtmknl" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-7">
                                <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
