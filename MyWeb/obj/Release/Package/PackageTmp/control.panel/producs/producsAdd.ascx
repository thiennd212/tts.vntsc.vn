﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="producsAdd.ascx.cs" Inherits="MyWeb.control.panel.producs.producsAdd" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li><a href="/admin-product/list.aspx">Sản phẩm</a></li>
        <li class="active">Thêm mới sản phẩm</li>
    </ol>
    <!-- end breadcrumb -->
    <h1 class="page-header">Thêm mới sản phẩm</h1>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                <div class="panel-heading p-0">
                    <div class="panel-heading-btn m-r-10 m-t-10">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <div class="tab-overflow">
                        <ul class="nav nav-tabs nav-tabs-inverse">
                            <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                            <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin sản phẩm</a></li>
                            <li class=""><a href="#contain-tab" data-toggle="tab">Nội dung chi tiết</a></li>
                            <li class=""><a href="#img-tab" data-toggle="tab">Hình ảnh và video</a></li>
                            <li class=""><a href="#attr-tab" data-toggle="tab">Thuộc tính sản phẩm</a></li>
                            <li class=""><a href="#attr-fillter-tab" data-toggle="tab">Bộ lọc sản phẩm</a></li>
                            <li class=""><a href="#price-tab" data-toggle="tab">Thông tin giá, khuyến mại</a></li>
                            <li class=""><a href="#seo-tab" data-toggle="tab">Cấu hình SEO</a></li>
                            <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content panel-body panel-form">
                    <asp:Panel ID="pnlErr" runat="server" Visible="false">
                        <div class="alert alert-danger fade in" style="border-radius: 0px;">
                            <button class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>

                    <%--TABS INFO--%>
                    <div class="tab-pane fade active in" id="info-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Nhóm sản phẩm *:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlForder" runat="server" class="form-control" OnSelectedIndexChanged="ddlForder_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:HiddenField ID="hidID" runat="server" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Hãng sản xuất :</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlMan" runat="server" class="form-control"></asp:DropDownList>                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Model :</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlModel" runat="server" class="form-control"></asp:DropDownList>                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tên sản phẩm *:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTen" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mã sản phẩm:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtMa" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Bảo hành:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtBaoHanh" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mô tả ngắn:</label>
                                <div class="col-md-7">                                    
                                    <CKEditor:CKEditorControl ID="txtproWarranty" runat="server" Toolbar="Child"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Nội dung tóm tắt:</label>
                                <div class="col-md-7">
                                    <CKEditor:CKEditorControl ID="fckTomtat" runat="server" Toolbar="Child"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Số lượng:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtCount" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Thứ tự:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtThutu" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="control-label col-md-2">Tình trạng:</label>
                                <div class="col-md-7">
                                    <asp:RadioButtonList ID="rbtStatus" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="0">Còn Hàng</asp:ListItem>
                                        <asp:ListItem Value="1">Hết hàng</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Ngày đăng:</label>
                                <div class="col-md-7">
                                    <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtNewsDate" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Kích hoạt:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkKichhoat" runat="server" />
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <label class="control-label col-md-2">Kích hoạt trang chủ:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkHome" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--TABS NỘI DUNG--%>
                    <div class="tab-pane fade" id="contain-tab">
                        <div class="form-horizontal form-bordered">
                            <asp:Literal runat="server" ID="ltrTabBind"></asp:Literal>
                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <input id="btnAdd" type="button" value="Thêm tab" class="btn btn-primary btn-sm" />
                                    <%--<input id="btnUpdateTag" type="button" value="Lưu tab" class="btn btn-success btn-sm" />--%>
                                </div>
                            </div>
                            <div style="display: none;">
                                <asp:TextBox runat="server" ID="ltrHidValue"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <%--TABS ẢNH--%>
                    <div class="tab-pane fade" id="img-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Số lượng ảnh:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlImage" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div id="list_image">
                                <asp:Repeater ID="rptImages" runat="server">
                                    <ItemTemplate>
                                        <div class="form-group" id='row<%#Eval("Id") %>'>
                                            <label class="control-label col-md-2">Chọn ảnh:</label>
                                            <div class="col-md-7">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <asp:HiddenField ID="txtMId" runat="server" />
                                                        <asp:TextBox ID="txtMImage" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                        <button id="btnMImgImage" class="btn btn-success" type="button" style="margin-left: -12px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mã nhúng youtube:</label>
                                <div class="col-md-5">
                                    <asp:TextBox ID="txtVideo" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-5">
                                    <img src="../../theme/admin_cms/img/demoyou.jpg" width="270" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--TABS THUOC TINH--%>
                    <div class="tab-pane fade" id="attr-tab">
                        <div class="form-horizontal form-bordered">                            
                            <%if (attrpro)
                              { %>
                            <div class="form-group">
                                <label class="control-label col-md-2">Thuộc tính sản phẩm:</label>
                                <div class="col-md-7">
                                    <asp:CheckBoxList ID="chkThuocTinh" runat="server"></asp:CheckBoxList>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>

                    <%--TABS BO LOC--%>
                    <div class="tab-pane fade" id="attr-fillter-tab">
                        <div class="form-horizontal form-bordered">                            
                            <%if (attr)
                              { %>
                            <div class="form-group">
                                <label class="control-label col-md-2">Bộ lọc sản phẩm:</label>
                                <div class="col-md-7">
                                    <asp:CheckBoxList ID="chkTinhnang" runat="server"></asp:CheckBoxList>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>

                    <%--TABS GIÁ--%>
                    <div class="tab-pane fade" id="price-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Giá niêm yết:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtGiagoc" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Giá bán:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtGia" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Loại tiền:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtLoaitien" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tùy chọn:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox runat="server" ID="chkNoibat" Text="Nổi bật" />
                                    &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="chkSanphammoi" runat="server" Text="Mới" />
                                    &nbsp;&nbsp;
                                <asp:CheckBox ID="chkSpbanchay" runat="server" Text="Sản phẩm bán chạy" />
                                    &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="chkKM" runat="server" Text="Khuyến mại" />
                                </div>
                            </div>

                            <div class="form-group" id="trTextKM" runat="server">
                                <label class="control-label col-md-2">Nội dung khuyến mại:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtPromotions" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" id="trThoigianKM" runat="server">
                                <label class="control-label col-md-2">Thời gian khuyến mại:</label>
                                <div class="col-md-3">
                                    <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtTungay" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <label class="control-label col-md-1">Đến ngày:</label>
                                <div class="col-md-3">
                                    <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtDenngay" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <%--TABS SEO--%>
                    <div class="tab-pane fade" id="seo-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta title:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTieude" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Meta description:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDesscription" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Meta keyword:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtKeyword" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tags sản phẩm:</label>
                                <div class="col-md-7">
                                    <a onclick="javascript:ShowBlogNewOther('divRelate')" style="font-size: 20px; cursor: pointer;"><i class="fa ion-ios-pricetags"></i></a>
                                </div>
                            </div>
                            <div id="divRelate" style="display: none;">
                                <asp:UpdatePanel ID="udpTags" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="control-label col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:ListBox ID="lstNew" runat="server" Rows="10" class="form-control"></asp:ListBox>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Button class="btn btn-primary" ID="btnActive" runat="server" Text=">>" OnClick="btnActive_Click" />
                                                <asp:Button class="btn btn-primary" ID="btnUnactive" runat="server" Text="<<" OnClick="btnUnactive_Click" />
                                            </div>
                                            <div class="col-md-4">
                                                <asp:ListBox ID="lstB" runat="server" Visible="false" class="form-control"></asp:ListBox>

                                                <asp:ListBox ID="lstNewRelate" runat="server" Rows="10" class="form-control"></asp:ListBox>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal form-bordered">
                        <div class="form-group" style="border-top: 1px solid #eee;">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-7">
                                <asp:LinkButton ID="btnUpdateProduct" runat="server" OnClick="btnUpdate_Click" class="btn btn-primary">Cập nhật</asp:LinkButton>
                                <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click" class="btn btn-danger">Hủy</asp:LinkButton>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>

    </div>
</div>
