﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/views/control/ucLoadControl.ascx" TagPrefix="uc1" TagName="ucLoadControl" %>
<%@ Register Src="~/views/control/ucBanner.ascx" TagPrefix="uc2" TagName="ucBanner" %>
<%@ Register Src="~/views/control/ucSearchBox.ascx" TagPrefix="uc3" TagName="ucSearchBox" %>
<%@ Register Src="~/views/control/ucMenuTop.ascx" TagPrefix="uc4" TagName="ucMenuTop" %>
<%@ Register Src="~/views/control/ucLanguage.ascx" TagPrefix="uc5" TagName="ucLanguage" %>
<%@ Register Src="~/views/control/ucLinkAcount.ascx" TagPrefix="uc6" TagName="ucLinkAcount" %>
<%@ Register Src="~/views/control/ucShopCart.ascx" TagPrefix="uc7" TagName="ucShopCart" %>
<%@ Register Src="~/views/control/ucMenuMain.ascx" TagPrefix="uc8" TagName="ucMenuMain" %>
<%@ Register Src="~/views/control/ucSlides.ascx" TagPrefix="uc9" TagName="ucSlides" %>
<%@ Register Src="~/views/control/ucLogin.ascx" TagPrefix="uc10" TagName="ucLogin" %>
<%@ Register Src="~/views/control/ucOnline.ascx" TagPrefix="uc12" TagName="ucOnline" %>
<%@ Register Src="~/views/control/ucBoxface.ascx" TagPrefix="uc16" TagName="ucBoxface" %>
<%@ Register Src="~/views/control/ucAdvright.ascx" TagPrefix="uc18" TagName="ucAdvright" %>
<%@ Register Src="~/views/news/ucAboutUs.ascx" TagPrefix="uc20" TagName="ucAboutUs" %>
<%@ Register Src="~/views/products/ucProHome.ascx" TagPrefix="uc21" TagName="ucProHome" %>
<%@ Register Src="~/views/control/ucMenuFooter.ascx" TagPrefix="uc25" TagName="ucMenuFooter" %>
<%@ Register Src="~/views/control/ucFooter.ascx" TagPrefix="uc27" TagName="ucFooter" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc28" TagName="ucMenuLeft" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc55" TagName="ucMenuLeft2" %>

<%@ Register Src="~/views/products/ucFilterAttributes.ascx" TagPrefix="uc29" TagName="ucFilterAttributes" %>
<%@ Register Src="~/views/control/ucAdvCenter.ascx" TagPrefix="uc30" TagName="ucAdvCenter" %>
<%@ Register Src="~/views/products/ucProHot.ascx" TagPrefix="uc32" TagName="ucProHot" %>
<%@ Register Src="~/views/control/ucBreadcrumb.ascx" TagPrefix="uc34" TagName="ucBreadcrumb" %>
<%@ Register Src="~/views/control/ucNewsLetter.ascx" TagPrefix="uc36" TagName="ucNewsLetter" %>
<%@ Register Src="~/views/news/ucPriority.ascx" TagPrefix="uc38" TagName="ucPriority" %>
<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>
<%@ Register Src="~/views/products/ucProHomeV2.ascx" TagPrefix="uc1009" TagName="ucProHomeV2" %>
<%@ Register Src="~/views/control/ucSlidesFooter.ascx" TagPrefix="uc1010" TagName="ucSlidesFooter" %>
<%@ Register Src="~/views/products/ucFilterManufac.ascx" TagPrefix="uc1011" TagName="ucFilterManufac" %>
<%@ Register Src="~/views/control/ucSearchLinkOrder.ascx" TagPrefix="uc1012" TagName="ucSearchLinkOrder" %>


<div class="menu_click_close"></div>
<div class="menu_hover menu_hover_2"></div>
<!-- ==== header top ===== -->
<div class="container-fluid header_top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc1010:ucSlidesFooter runat="server" ID="ucSlidesFooter" />
            </div>
            <div class="img_not_run">
                <a href="">
                    <img src="/uploads/layout/default/css/images/img_not_run.jpg" alt="" /></a>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<!-- ==== header mid ==== -->
<div class="header_mid">
    <div class="container-fluid header_mid_1">
        <div class="container">
            <div class="row header_mid_top">
                <div class="col-md-12">
                    <div class="title">
                        Nhà bán lẻ trực tiếp phục vụ ngành công nghiệp
                    </div>
                    <div class="search_order">
                        <i class="fa fa-search search_image aria-hidden="true"></i><uc1012:ucSearchLinkOrder runat="server" ID="ucSearchLinkOrder"/>
                    </div>
                    <div class="custumer_care">
                        <div class="custumer_care_hover">CHĂM SÓC KHÁCH HÀNG</div>
                        <uc12:ucOnline runat="server" ID="ucOnline" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid header_mid_2">
        <div class="container">
            <div class="row header_mid_bottom">
                <div class="col-md-12">
                    <div class="banner_all">
                        <uc2:ucBanner runat="server" ID="ucBanner" />
                    </div>
                    <div class="search_all">
                        <uc3:ucSearchBox runat="server" ID="ucSearchBox" />
                    </div>
                    <div class="cart_all">
                        <uc7:ucShopCart runat="server" ID="ucShopCart" />
                    </div>
                    <div class="policy">
                        <p>Bảo hành chính hãng</p>
                        <p>Giao hàng tận nơi</p>
                        <p>Tư vấn 24/7</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<!-- ==== header bottom ==== -->
<div class="container-fluid header_bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title_menu">
                    <p>DANH MỤC SẢN PHẨM <i class="fa fa-bars" aria-hidden="true"></i></p>
                </div>
                <div class="menu_all">
                    <div class="menu_main_pc">
                        <uc8:ucMenuMain runat="server" ID="ucMenuMain" />
                    </div>
                    <div class="menu_mobile"><p>MENU <i class="fa fa-cog" aria-hidden="true"></i></p></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== main ==== -->

<!-- ==== slide ==== -->
<div class="container-fluid slider-adv">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="menu_left_all">
                    <%if (Session["home_page"] != null)
                    {%>
                        <uc28:ucMenuLeft runat="server" ID="ucMenuLeft" />
                    <%} %>
                    <div class="menu_left_fix"><uc55:ucMenuLeft2 runat="server" ID="ucMenuLeft2" /></div>
                </div>        
                <%if (Session["home_page"] != null)
                {%>
                <div class="slider_with_adv">
                    <div class="slider">
                        <uc9:ucSlides runat="server" ID="ucSlides" />
                    </div>
                    <div class="adv">
                        <uc18:ucAdvright runat="server" ID="ucAdvright" />
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<%if (Session["home_page"] != null)
{%>
<!-- ==== adv center ===== -->
<div class="container-fluid adv_center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc30:ucAdvCenter runat="server" ID="ucAdvCenter" />
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<!-- ==== pro hot ==== -->
<div class="container-fluid pro_hot">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc32:ucProHot runat="server" ID="ucProHot" />
            </div>
        </div>
    </div>
</div>

<!-- ==== pro home ==== -->
<div class="container-fluid pro_home">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc1009:ucProHomeV2 runat="server" ID="ucProHomeV2" />
            </div>
        </div>
    </div>
</div>
<%} %>

<%if (Session["home_page"] == null)
  {%>
<div class="container-fluid crum_all">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc34:ucBreadcrumb runat="server" ID="ucBreadcrumb" />
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid main_page_else">
    <div class="container">
        <div class="row">
            <%if (Session["page_product"] == null || Session["product_show_cat_child"] != null)
              {%>
            <div class="col-md-12 col_left col_left_fix">
                <%}
              else
              { %>
                <div class="col-md-9 col_left">
                    <%} %>
                    <uc1:ucLoadControl runat="server" ID="ucLoadControl" />
                </div>
                <%if (Session["page_product"] != null && Session["product_show_cat_child"] == null)
                  {%>
                <div class="col-md-3 col_right">
                    <div class="scroll_product_filter">
                        <%if (Session["page_product"] != null && Session["page_product"] == "true")
                          {%>
                        <uc1011:ucFilterManufac runat="server" ID="ucFilterManufac" />
                        <uc29:ucFilterAttributes runat="server" ID="ucFilterAttributes" />
                        <%} %>
                        <%if (Session["page_product"] != null && Session["page_product"] == "news"){%>
                        <uc38:ucPriority runat="server" ID="ucPriority" />
                        <%} %>
                    </div>
                </div>
                <%} %>
        </div>
    </div>
</div>
<%} %>
<div class="clearfix"></div>

<!-- === footer ==== -->
<div class="container-fluid policy_img">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <uc20:ucAboutUs runat="server" ID="ucAboutUs" />
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<footer class='container-fluid'>
    <div class="container">
        <div class="row">
            <div class="col-md-12 footer_top">
                <div class="footer_top_box">
                    <div class="footer_info_contact">
                        <uc27:ucFooter runat="server" ID="ucFooter" />
                    </div>
                    <div class="footer_menu">
                        <uc25:ucMenuFooter runat="server" ID="ucMenuFooter" />
                    </div>
                    <div class="footer_box_face">
                        <uc16:ucBoxface runat="server" ID="ucBoxface" />
                    </div>
                </div>
            </div>

            <div class="col-md-12 footer_mid">
                <div class="footer_mid_box">
                    <div class="bill_accep">
                        <p>Chấp nhận thanh toán: </p>
                        <ul>
                            <li><a href="javascript:void(0)">
                                <img src="/uploads/layout/default/css/images/bill_accep_1.png" alt="" /></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="/uploads/layout/default/css/images/bill_accep_2.png" alt="" /></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="/uploads/layout/default/css/images/bill_accep_3.png" alt="" /></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="/uploads/layout/default/css/images/bill_accep_4.png" alt="" /></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="/uploads/layout/default/css/images/bill_accep_5.png" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="send_mail">
                        <uc36:ucNewsLetter runat="server" ID="ucNewsLetter" />
                    </div>
                    <div class="tb_bct">
                        <a href="">
                            <img src="uploads/layout/default/css/images/tb_bct" alt="" /></a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 footer_bottom">
                <div class="footer_bottom_box">
                    <div class="footer_bottom_title_left">© 2007 Công ty Cổ phần TRSMART Việt Nam - ĐCĐK: 29F Hai Bà Trưng. GPĐKKD số: 0102516308 do Sở KHĐT Tp.Hà Nội cấp ngày 15/11/2007.</div>
                    <div class="footer_bottom_title_right">Design by <a href="http://vntsc.vn/" target="_blank">vntsc.vn</a></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="to_top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>



<script type="text/javascript">
    $(document).ready(function () {
        $("#content div").hide(); // Initially hide all content
        $("#tabs li:first").attr("id", "current"); // Activate first tab
        $("#content div:first").fadeIn(); // Show first tab content

        $('#tabs a').click(function (e) {
            e.preventDefault();
            if ($(this).closest("li").attr("id") == "current") { //detection for current tab
                return
            }
            else {
                $("#content div").hide(); //Hide all content
                $("#tabs li").attr("id", ""); //Reset id's
                $(this).parent().attr("id", "current"); // Activate this
                $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });
    });

    var owl_category_1 = $(".box-product-hot .body-pro");
    owl_category_1.owlCarousel({
        items: 5,
        loop: true,
        autoPlay: false,
        transitionStyle: "fade",
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [769, 2],
        itemsTablet: [641, 2],
        itemsTablet: [640, 2],
        itemsMobile: [320, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });



    var owl_category3 = $(".custumer-say");
    owl_category3.owlCarousel({
        items: 4,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [640, 2],
        itemsMobile: [320, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: false,
        paginationNumbers: false,
    });

    $(document).ready(function () {
        $(".slides .owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
        $(".slides .owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
    });
</script>
