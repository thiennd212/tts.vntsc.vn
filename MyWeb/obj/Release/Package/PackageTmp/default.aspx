﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="MyWeb._Default" %>

    <!DOCTYPE HTML>
    <html>

    <head runat="server">
        <title></title>
        <asp:Literal runat="server" ID="ltrMainMeta"></asp:Literal>
        <%--META--%>
            <html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" itemscope="itemscope" itemtype="http://schema.org/WebPage">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta http-equiv="content-language" itemprop="inLanguage" content="vi" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <asp:Literal ID="ltrMeta" runat="server"></asp:Literal>

            <%--style import--%>
                <link rel="stylesheet" type="text/css" href="/theme/default/dist/css/bootstrap.min.css" />
                <link rel="stylesheet" type="text/css" href="/theme/default/css/font-awesome.min.css" />
                <link rel="stylesheet" type="text/css" href="/theme/default/css/datepicker.css" />
                <link rel="stylesheet" type="text/css" href="/theme/default/css/bootstrap-datepicker.css" />
                <link rel="stylesheet" type="text/css" href="/theme/default/plugins/owl-carousel/owl.carousel.css">
                <link rel="stylesheet" type="text/css" href="/theme/default/plugins/owl-carousel/owl.theme.css">
                <link rel="stylesheet" type="text/css" href="/theme/default/plugins/owl-carousel/owl.transitions.css">
                <link rel="stylesheet" type="text/css" href="/theme/default/plugins/cloudzoom/css/cloudzoom.css" />
                <link rel="stylesheet" type="text/css" href="/theme/default/plugins/cloudzoom/css/thumbelina.css" />
                <link rel="stylesheet" type="text/css" href="/theme/default/css/styles.css" />
                <link type="text/css" rel="stylesheet" href="/theme/default/plugins/lightbox/colorbox.css" />
                <asp:Literal ID="lrtStyleSheet" runat="server"></asp:Literal>
                <%--style mobile--%>
                    <link rel="stylesheet" type="text/css" href="/theme/default/css/icons.css" />
                    <link rel="stylesheet" type="text/css" href="/theme/default/plugins/responside/component.css" />

                    <%--javascript import--%>
                        <!--[if lt IE 9]><script src="/theme/default/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
                        <script src="/theme/default/assets/js/ie-emulation-modes-warning.js"></script>
                        <script src="/theme/default/assets/js/ie10-viewport-bug-workaround.js"></script>
                        <!--[if lt IE 9]>
                        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                        <![endif]-->
                        <script type="text/javascript" src="/theme/default/js/jquery.min.js"></script>
                        <script type="text/javascript" src="/theme/default/dist/js/bootstrap.min.js"></script>
                        <script type="text/javascript" src="/theme/default/js/bootstrap-typeahead.js"></script>
                        <script type="text/javascript" src="/theme/default/plugins/owl-carousel/owl.carousel.js"></script>
                        <script type="text/javascript" src="/theme/default/js/script.js"></script>
                        <script type="text/javascript" src="/theme/default/plugins/cloudzoom/js/cloudzoom.js"></script>
                        <script type="text/javascript" src="/theme/default/plugins/cloudzoom/js/thumbelina.js"></script>
                        <script type="text/javascript" src="/theme/default/plugins/lightbox/jquery.colorbox.js"></script>
                        <script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
                        <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

                        <%--javascript mobile--%>
                            <script type="text/javascript" src="/theme/default/plugins/responside/modernizr.custom.js"></script>
                            <script type="text/javascript" src="/theme/default/plugins/responside/classie.js"></script>
                            <script type="text/javascript" src="/theme/default/plugins/responside/mlpushmenu.js"></script>
                            <script type="text/javascript" src="/theme/default/js/bootstrap-datepicker.js"></script>

                            <!--Start of Tawk.to Script-->
                            <script type="text/javascript">
                                var Tawk_API = Tawk_API || {},
                                    Tawk_LoadStart = new Date();
                                (function() {
                                    var s1 = document.createElement("script"),
                                        s0 = document.getElementsByTagName("script")[0];
                                    s1.async = true;
                                    s1.src = 'https://embed.tawk.to/5a123cdbbb0c3f433d4ca1cd/default';
                                    s1.charset = 'UTF-8';
                                    s1.setAttribute('crossorigin', '*');
                                    s0.parentNode.insertBefore(s1, s0);
                                })();
                            </script>
                            <!--End of Tawk.to Script-->

    </head>

    <body>
        <!--onload="_googWcmGet('number', '<%= GlobalClass.conTel %>')"-->
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '1895087717410765',
                    cookie: true,
                    xfbml: true,
                    version: 'v2.8'
                });
                FB.AppEvents.logPageView();
            };
        </script>
        <form id="MyWebForm" runat="server">
            <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
            <asp:Literal ID="ltrFacebook" runat="server"></asp:Literal>
            <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
            <asp:Literal ID="ltrH" runat="server"></asp:Literal>
            <asp:PlaceHolder ID="MainPlaceHolder" runat="server"></asp:PlaceHolder>
            <asp:Literal ID="ltrFooterBody" runat="server"></asp:Literal>
            <asp:Literal ID="ltrAdv" runat="server"></asp:Literal>
            <asp:Literal ID="ltrPopup" runat="server"></asp:Literal>
            <asp:Literal ID="ltrBottomLayer" runat="server"></asp:Literal>
            <asp:Literal ID="ltrLiveChat" runat="server"></asp:Literal>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="loadViewPro"></div>
                        </div>
                    </div>
                </div>
            </div>



            <span id="top-link-block" class="hidden">
            <a href="#top" class="well-sm" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
                <img src="/theme/default/img/icon_gototop.png" />
            </a>
        </span>
            <%--javascript initialize --%>
                <script type="text/javascript">
                    $(document).ready(function() {
                        var listChk = [];
                        $("input[name='checksub']").click(function() {
                            var le = $(this).val()
                            if (this.checked == true) {
                                listChk.push($(this).val());
                                $("input.box" + le).attr("checked", true);
                            } else {
                                $("input.box" + le).attr("checked", false);
                                var removeitem = $(this).val();
                                listChk = $.grep(listChk, function(value) {
                                    return value != removeitem;
                                });
                            }
                            if (listChk == "") {
                                window.location.reload();
                            } else {
                                var cat = window.location.href;
                                var tag = "";
                                var arrCat = cat.split('//');
                                var arrCat2 = arrCat[1].split("/");
                                var s1 = arrCat2[1];
                                if (arrCat2.length > 2) {
                                    tag = s1;
                                    tag += "/" + arrCat2[2].replace(".html", "");
                                } else {
                                    tag = s1.substr(0, cat.indexOf(".html"));
                                }
                                //cat = cat.substr(cat.lastIndexOf('/') + 1);
                                //cat = cat.substr(0, cat.indexOf(".html"));
                                $("#loadFillter").load("/search_results.aspx?attid=" + listChk + "&cat=" + tag);

                                $.ajax({
                                    async: false,
                                    type: "POST",
                                    url: "webService.asmx/GetMetaSEO",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({
                                        attid: listChk.join()
                                    }),
                                    success: function(response) {
                                        console.log(response.d);
                                        if (response.d != null) {
                                            if (response.d.Title != "") {
                                                $("title").html(response.d.Title);
                                            }

                                            if (response.d.Description != "") {
                                                $("meta[name='description']").attr("content", response.d.Description);
                                            }

                                            if (response.d.Keyword != "") {
                                                $("meta[name='keywords']").attr("content", response.d.Keyword);
                                            }
                                        }
                                    },
                                    failure: function(response) {}
                                });
                            }
                        });

                        // if ($(window).width() > 1199) {
                        //     $(window).scroll(function() {
                        //         var down_1 = $(".header_top").height();
                        //         var down_2 = $(".header_mid").height();
                        //         var down_3 = $(".header_bottom").height();
                        //         var down_4 = $(".crum_all").height();
                        //         var down_5 = $(".detail_box_1").height();
                        //         var down_6 = $(".detail_box_2 >.clearfix").height();
                        //         var down_7 = $(".box-pro-same").height();

                        //         var down_8 = $("#loadFillter").height();
                        //         if (($(this).scrollTop() > (down_1 + down_2 + down_3 + down_4)) && ($(this).scrollTop() < (down_1 + down_2 + down_3 + down_4 + down_5 + down_6))) {
                        //             $(".form_right_all").addClass("form_right_all_1");
                        //         } else {
                        //             $(".form_right_all").removeClass("form_right_all_1");
                        //         }

                        //         if (($(this).scrollTop() > (down_1 + down_2 + down_3 + down_4 + 60)) && ($(this).scrollTop() < (down_1 + down_2 + down_3 + down_4 + down_8 - 200))) {
                        //             $(".scroll_product_filter").addClass("scroll_product_filter_1");
                        //         } else {
                        //             $(".scroll_product_filter").removeClass("scroll_product_filter_1");
                        //         }
                        //     });
                        // }
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function() {
                        var listChk = [];
                        $("input[name='checksubman']").click(function() {
                            var parent = $(this).parent();
                            var link = parent.attr("name");
                            window.location.assign(link);
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        var Accordion = function(el, multiple) {
                            this.el = el || {};
                            this.multiple = multiple || false;
                            var links = this.el.find('.link');
                            links.on('click', {
                                el: this.el,
                                multiple: this.multiple
                            }, this.dropdown)
                        }

                        Accordion.prototype.dropdown = function(e) {
                            var $el = e.data.el;
                            $this = $(this),
                                $next = $this.next();

                            $next.slideToggle();
                            $this.parent().toggleClass('open');
                            if (!e.data.multiple) {
                                $el.find('.sub-menu-left').not($next).slideUp().parent().removeClass('open');
                            };
                        }
                        var accordion = new Accordion($('#menu_left'), false);
                    });

                    if (($(window).height() + 100) < $(document).height()) {
                        $('#top-link-block').removeClass('hidden').affix({
                            offset: {
                                top: 100
                            }
                        });
                    }

                    function openViewPro(proID) {
                        $("#loadViewPro").load("/view_product.aspx?id=" + proID);
                        $('.bs-example-modal-lg').modal();
                    }

                    function addToCartView(proID) {
                        $("#loadViewPro").load("/add_cart_product.aspx?id=" + proID);
                        $('.bs-example-modal-lg').modal();
                    }
                    $(document).ready(function() {
                        $(".modal .close").click(function() {
                            $("#loadViewPro").html("");
                        });

                        listProductSearch();
                    })
                    var tagsource = []

                    function listProductSearch() {
                        $.ajax({
                            async: false,
                            type: "POST",
                            url: "webService.asmx/getProductList",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(response) {
                                var tagProSearch = response.d;

                                for (i = 0; i < tagProSearch.length; i++) {
                                    tagsource.push({
                                        "id": tagProSearch[i].id,
                                        "name": tagProSearch[i].strName,
                                        "txtimg": tagProSearch[i].strImg
                                    });
                                }
                            },
                            failure: function(response) {}

                        });
                    }

                    function displayResult(item, val, text) {
                        console.log(item);
                    }

                    $(function() {
                        $('[id*=txtSearch]').typeahead({
                            source: tagsource,
                            itemSelected: displayResult
                        });
                    });
                </script>
                <script type="text/javascript">
                    new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'), {
                        type: 'cover'
                    });
                </script>
                <script type="text/javascript">
                    document.write("<script type='text/javascript' language='javascript'>MainContentW = 1040;LeftBannerW = 129;RightBannerW = 0;LeftAdjust = 0;RightAdjust = 1;TopAdjust = 145;ShowAdDiv();window.onresize=ShowAdDiv;<\/script>");
                </script>

                <script type="text/javascript">
                    $(window).load(function() {
                        $('#myModalPopUp').modal('show');
                    });
                </script>

                <script>
                    $(".col_right").insertAfter(".col_left");
                    // contact
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtname").attr("Placeholder", 'Họ và tên *');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtdiachi").attr("Placeholder", 'Địa chỉ *');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtDienthoai").attr("Placeholder", 'Số điện thoại *');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtDienthoaicodinh").attr("Placeholder", 'Số điện thoại cố định');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtmail").attr("Placeholder", 'Email');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtnoidung").attr("Placeholder", 'Nội dung');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtCaptcha").attr("Placeholder", 'Capcha');
                    $(".box-sub-contact #ctl20_ucLoadControl_ctl00_txtFax").attr("Placeholder", 'Fax');
                    //pay
                    $(".box-oder-pay #ctl20_ucLoadControl_ctl00_txtname").attr("Placeholder", 'Họ và tên *');
                    $(".box-oder-pay #ctl20_ucLoadControl_ctl00_txtAddress").attr("Placeholder", 'Địa chỉ *');
                    $(".box-oder-pay #ctl20_ucLoadControl_ctl00_txtPhone").attr("Placeholder", 'Số điện thoại *');
                    $(".box-oder-pay #ctl20_ucLoadControl_ctl00_txtPhoneCodinh").attr("Placeholder", 'Số điện thoại cố định');
                    $(".box-oder-pay #ctl20_ucLoadControl_ctl00_txtEmail").attr("Placeholder", 'Email');
                    $(".box-oder-pay #ctl20_ucLoadControl_ctl00_txtContent").attr("Placeholder", 'Nội dung');

                    // ==== scroll to top ====
                    $(".to_top").click(function() {
                        $("html,body").animate({
                            scrollTop: 0
                        }, 600);
                    });
                    $(window).scroll(function() {
                        if ($(this).scrollTop()) {
                            $('.to_top:hidden').stop(true, true).fadeIn();
                        } else {
                            $('.to_top').stop(true, true).fadeOut();
                        }
                    });

                    //newsletter
                    $(".news-letter .box-input .btn-letter").html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');

                    // change place off img on menu left
                    $(".wrap-sub-menu").each(function() {
                        $(this).children(".image-menu").insertAfter($(this).children(".sub-menu"));
                    });

                    // hover menu left chage img
                    $("#menu_left >li").each(function() {
                        $(this).mouseover(
                            function(e) {
                                $(this).children(".span-menu").children(".icon-menu").attr("src", "/uploads/files/back_sale_1.png");
                                return true;
                            }
                        );
                        $(this).mouseleave(
                            function(e) {
                                $(this).children(".span-menu").children(".icon-menu").attr("src", "/uploads/files/back_sale.png");
                                return true;
                            }
                        );
                    });

                    // change pagination
                    $("#loadFillter").each(function() {
                        $(this).children(".clearfix").insertAfter($(this).children(".product-list"));
                    });

                    // change place class list-cat-child
                    $(".col_left .box-pro-sub").each(function() {
                        $(this).children(".sub-top").children(".list-cat-child").insertAfter(".crum_all .box-breadcumb .breadcum");
                    });

                    $(".col_left .box-pro-detailt-sub").each(function() {
                        $(this).children(".list-cat-child").insertAfter(".crum_all .box-breadcumb .breadcum");
                    });
                    // img_card
                    $(".header_mid .header_mid_2 .cart_all .header-box-cart .icon-cart").html('<a href="javascript:void(0)"><img src="uploads/layout/default/css/images/img_card_top.png"></a>');

                    if ($(window).width() > 1199) {
                        if (!($(".slider-adv .container .row .col-md-12").children("div").hasClass("slider_with_adv"))) {
                            $(".slider-adv").css('paddingBottom', "0px");
                            $(".title_menu").mouseover(function() {
                                $(".menu_left_fix").addClass("block").css("width", "19.42%");
                                $(".menu_hover").addClass("menu_hover_1");
                            });

                            $(".menu_hover,.header_mid,.menu_all").mouseover(function() {
                                $(".menu_left_fix").removeClass("block");
                                $(".menu_hover").removeClass("menu_hover_1");
                            });
                        }
                    }

                    // contact
                    $(".box_contact").appendTo(".row_1");

                    // prodetail
                    $(".hotline_coppy_1").text($(".hotline_num1").text().replace("Tư vấn kinh doanh:", ''));
                    $(".form_right_0").each(function() {
                        $(this).children(".phone_number").appendTo($(this).next(".form_right_1").children(".vt_1").children(".phone_all"));
                    });
                    $(".form_right_0").each(function() {
                        $(this).children(".info-tag").insertAfter($(this).next(".form_right_1").children(".vt_1"));
                    });

                    // content pro change place
                    $(".pro_home .itemProContent").each(function() {
                        $(this).children(".pro-content").insertAfter($(this).children(".frame-content"));
                    });


                    // menu mobile
                    if ($(window).width() < 1025) {
                        $(".menu_main_pc").insertAfter(".menu_left_all");
                        $('<i class="fa fa-angle-down" aria-hidden="true"></i>').insertAfter("#nav-main-menu >.itop >a");
                        $("#nav-main-menu .itop > .sub-menu").hide();
                        $(".menu_main_pc .itop > a").after("<i class='fa fa-angle-down'></i>");
                        $(".nav-menu .fa-angle-down").click(function() {
                            $(this).next('.sub-menu').animate({
                                height: "toggle"
                            }, 500);
                        });
                    }
                    $(".menu_mobile").click(function() {
                        $(".menu_main_pc").addClass("menu_main_pc_1");
                        $(".menu_click_close").addClass("menu_click_close_all");
                    });

                    if ($(window).width() < 1025) {
                        $(".title_menu").click(function() {
                            $(".menu_left_all").addClass("menu_left_all_1");
                            $(".menu_click_close").addClass("menu_click_close_all");

                        });
                    }

                    $(".menu_click_close").click(function() {
                        $(".menu_main_pc").removeClass("menu_main_pc_1");
                        $(".menu_click_close").removeClass("menu_click_close_all");
                        $(".menu_left_all").removeClass("menu_left_all_1");
                    });

                    // menu scroll
                    // ==== scroll ====
                    if ($(window).width() < 480) {
                        $(window).scroll(function() {
                            if ($(this).scrollTop() > 200) {
                                $(".header_bottom").addClass('scroll');
                            } else {
                                $(".header_bottom").removeClass('scroll');
                            }
                        });
                    }

                    $('.title-search-order').insertBefore(".box-search-order");
                    $(".itemProContent").each(function() {
                        $(this).children(".pro-manu").insertBefore($(this).children(".pro-name"));
                    });
                    $("figure").each(function() {
                        $(this).children(".pro-manu").insertBefore($(this).children(".link-view"));
                    });
                    $(".col_phone_1 button").html("Mua nhanh");
                    $(".col_phone_2 #ctl18_ucLoadControl_ctl00_rptpro_ctl00_btnAddNow").html("Đặt hàng ngay");

                    $(".col_right .scroll_product_filter .box-news-priority .item-list").each(function() {
                        $(this).children(".news-more").insertAfter($(this).children(".div-news-container"));
                    });

                    // price
                    if (($(".color-text-second").html() == '0 đ') || $(".color-text-second").html() == ' đ') {
                        $(".color-text-second").html("Liên hệ");
                        $(".text-price-niem-yet").css('display', "none");
                    }

                    $(".itemProContent").each(function() {
                        $(this).children(".price-off").insertAfter($(this).children(".frame-content"));
                    });

                    // content prodetails
                    $(".detail_box_2").each(function() {
                        $(this).children(".clearfix").insertBefore($(this).children(".box-pro-same"));
                    });

                    // product_sale
                    if ($(".col_left").children("div").hasClass("box-product-new")) {
                        $(".main_page_else .container .row .col_left").css({
                            'width': "100% ",
                        });
                        $(".col_right").css({
                            'display': "none"
                        });
                    }
                    if ($(window).width() > 1200) {
                        if ($(".col_left").children("div").hasClass("box-product-new")) {
                            $(".main_page_else .container .row .col_left").css({
                                'padding-right': "15px",
                            });
                        }
                    }

                    //contact on box cart
                    $('<div class="body-cart-1"><div class="footer_on_menu"><div class="title">Công ty Cổ phần TRSMART Việt Nam</div><p>VĂN PHÒNG TỔNG CÔNG TY</p><p>Tòa Nhà 5 Tầng: Số 2A1, KĐT Đầm Trấu, P. Bạch Đằng, Q. Hai Bà</p><p>Trưng, TP. Hà Nội (Đối diện Bến Xe Lương Yên - Có bãi gửi Xe)</p><p>Tel: 04.8888.9999 - 0972.888.999</p><p>Khiếu nại: 0972.888.666</p><p>Website: | www.ttsmart.com.vn</p><p>Email: kinhdoanh@ttsmart.com.vn | ceo@ttsmart.vn</p></div><div class="maps_add_cart"><div class="hotline_1"><span class="hotline_coppy_2">Hotline: </span><span class="hotline_coppy_1">04.8888.9999 - 0972.888.999</span></div></div>').insertAfter(".col_left_fix .box-oder-pay .body-cart");

                    //change name news hot
                    $(".scroll_product_filter .box-news-priority .header").html("Giới thiệu công ty TTS");
                </script>
        </form>
    </body>

    </html>