﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class CenterDB
{
    #region[Center_Add]
    public static bool Center_Add(CenterDATA _CenterDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catName", _CenterDATA.CatId));
                dbCmd.Parameters.Add(new SqlParameter("@catImage", _CenterDATA.ProId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_Add]
    public static bool Center_Add2(CenterDATA _CenterDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catName", _CenterDATA.CatId));
                dbCmd.Parameters.Add(new SqlParameter("@catImage", _CenterDATA.ProId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_GetByID]
    public static List<CenterDATA> Center_GetByID(string Id)
    {
        List<CenterDATA> list = new List<CenterDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Id", Id));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterDATA objCenterDATA = new CenterDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[Center_Insert]
    public static bool Center_Insert(string CatId, string proId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Insert into Center(CatId, ProId) VALUES(" + CatId + ", " + proId + ")", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_Update]
    public static bool Center_Update(string CatId, string proId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Update Center set CatId = " + CatId + " where ProId = " + proId + "", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion

    #region[Center_GetByProIdAndCatId]
    public static List<CenterDATA> Center_GetByProIdAndCatId(string proId, string catId)
    {
        List<CenterDATA> list = new List<CenterDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetByProIDAndCatId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ProId", proId));
                dbCmd.Parameters.Add(new SqlParameter("@CatId", catId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterDATA objCenterDATA = new CenterDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_Center_GetProId]
    public static List<CenterDATA> Center_GetProId(string proid)
    {
        List<CenterDATA> list = new List<CenterDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetProId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ProId", proid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterDATA objCenterDATA = new CenterDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[Center_GetByAll]
    public static List<CenterDATA> Center_GetByAll()
    {
        List<CenterDATA> list = new List<CenterDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CenterDATA objCenterDATA = new CenterDATA(
                            reader["Id"].ToString(),
                            reader["CatId"].ToString(),
                            reader["ProId"].ToString());
                        list.Add(objCenterDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[Center_Update]
    public static bool Center_Update(CenterDATA _CenterDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("SP_Center_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", _CenterDATA.Id));
                dbCmd.Parameters.Add(new SqlParameter("@catName", _CenterDATA.CatId));
                dbCmd.Parameters.Add(new SqlParameter("@catImage", _CenterDATA.ProId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_Delete]
    public static bool Center_Delete(CenterDATA _CenterDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_Center_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Id", _CenterDATA.Id));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[Center_DeleteByProId]
    public static bool Center_DeleteByProId(string proId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("delete from center where ProId = '" + proId + "'", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[AttproDelete]
    public static bool Attpro_delete(string attid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Delete tbAttPro where Id = " + attid + "", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[AttCatDelete]
    public static bool AttCat_delete(string id)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Delete tbCatAtt where catId = " + id + "", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[AttproDelete]
    public static bool Insert_attpro(string attid, string proid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Insert into tbAttPro(attId, proId) VALUES(" + attid + ", " + proid + ")", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion

    #region[AttCatInsert]
    public static bool Insert_attcat(string attid, string catid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Insert into tbCatAtt(catId, attId) VALUES(" + catid + ", " + attid + ")", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
}