﻿using MyWeb;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbProductDB
{
    static SqlCommand cmd;


    #region[tbProduct_GetByAll]
    public static List<tbProductDATA> getProductListByAdmin(int intCurr, ref int intCount, int intPageSize,int intUserID, string strWhere, string strOrderBy)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_getListProductByAdmin", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;

                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@intCurrPage", intCurr));

                dbCmd.Parameters.Add(new SqlParameter("@intTotalRecords", intCount));
                dbCmd.Parameters["@intTotalRecords"].Direction = ParameterDirection.Output;

                dbCmd.Parameters.Add(new SqlParameter("@intPageSize", intPageSize));
                dbCmd.Parameters.Add(new SqlParameter("@intUserID", intUserID));
                dbCmd.Parameters.Add(new SqlParameter("@strWhere", strWhere));
                dbCmd.Parameters.Add(new SqlParameter("@strOrderBy", strOrderBy));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }


                intCount = int.Parse(dbCmd.Parameters["@intTotalRecords"].Value.ToString());
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion


    #region[tbProduct_Add]
    public static bool tbProduct_Add(tbProduct _tbProductDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proCode", _tbProductDATA.proCode));
                dbCmd.Parameters.Add(new SqlParameter("@proName", _tbProductDATA.proName));
                dbCmd.Parameters.Add(new SqlParameter("@proImage", _tbProductDATA.proImage));
                dbCmd.Parameters.Add(new SqlParameter("@proFiles", _tbProductDATA.proFiles));
                dbCmd.Parameters.Add(new SqlParameter("@proContent", _tbProductDATA.proContent));
                dbCmd.Parameters.Add(new SqlParameter("@proDetail", _tbProductDATA.proDetail));
                dbCmd.Parameters.Add(new SqlParameter("@proDate", _tbProductDATA.proDate));
                dbCmd.Parameters.Add(new SqlParameter("@proTitle", _tbProductDATA.proTitle));
                dbCmd.Parameters.Add(new SqlParameter("@proDescription", _tbProductDATA.proDescription));
                dbCmd.Parameters.Add(new SqlParameter("@proKeyword", _tbProductDATA.proKeyword));
                dbCmd.Parameters.Add(new SqlParameter("@proPriority", _tbProductDATA.proPriority));
                dbCmd.Parameters.Add(new SqlParameter("@proIndex", _tbProductDATA.proIndex));
                dbCmd.Parameters.Add(new SqlParameter("@catId", _tbProductDATA.catId));
                dbCmd.Parameters.Add(new SqlParameter("@proType", _tbProductDATA.proType));
                dbCmd.Parameters.Add(new SqlParameter("@proActive", _tbProductDATA.proActive));
                dbCmd.Parameters.Add(new SqlParameter("@proOriginalPrice", _tbProductDATA.proOriginalPrice));
                dbCmd.Parameters.Add(new SqlParameter("@proPrice", _tbProductDATA.proPrice));
                dbCmd.Parameters.Add(new SqlParameter("@proUnitprice", _tbProductDATA.proUnitprice));
                dbCmd.Parameters.Add(new SqlParameter("@proPromotions", _tbProductDATA.proPromotions));
                dbCmd.Parameters.Add(new SqlParameter("@proWeight", _tbProductDATA.proWeight));
                dbCmd.Parameters.Add(new SqlParameter("@proUnits", _tbProductDATA.proUnits));
                dbCmd.Parameters.Add(new SqlParameter("@proOrd", _tbProductDATA.proOrd));
                dbCmd.Parameters.Add(new SqlParameter("@proLang", _tbProductDATA.proLang));
                dbCmd.Parameters.Add(new SqlParameter("@proWarranty", _tbProductDATA.proWarranty));
                dbCmd.Parameters.Add(new SqlParameter("@proTagName", _tbProductDATA.proTagName));
                dbCmd.Parameters.Add(new SqlParameter("@proVideo", _tbProductDATA.proVideo != null ? _tbProductDATA.proVideo : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proCount", _tbProductDATA.proCount != null ? _tbProductDATA.proCount.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proCare", _tbProductDATA.proCare != null ? _tbProductDATA.proCare.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proSpLienQuan", _tbProductDATA.proSpLienQuan != null ? _tbProductDATA.proSpLienQuan.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proStatus", _tbProductDATA.proStatus != null ? _tbProductDATA.proStatus.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proUrl", _tbProductDATA.proUrl != null ? _tbProductDATA.proUrl.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proKieuspdaban", _tbProductDATA.proKieuspdaban != null ? _tbProductDATA.proKieuspdaban.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proNoibat", _tbProductDATA.proNoibat != null ? _tbProductDATA.proNoibat.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proBanchay", _tbProductDATA.proBanchay != null ? _tbProductDATA.proBanchay.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proKM", _tbProductDATA.proKM != null ? _tbProductDATA.proKM.ToString() : "0"));
                if (!string.IsNullOrEmpty(_tbProductDATA.proTungay + ""))
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proTungay", _tbProductDATA.proTungay));
                }
                else
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proTungay", DBNull.Value));
                }

                if (!string.IsNullOrEmpty(_tbProductDATA.proDenngay + ""))
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proDenngay", _tbProductDATA.proDenngay));
                }
                else
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proDenngay", DBNull.Value));
                }
                dbCmd.Parameters.Add(new SqlParameter("@proNoidungKM", _tbProductDATA.proNoidungKM != null ? _tbProductDATA.proNoidungKM.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@manufacturerId", _tbProductDATA.manufacturerId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbProduct_Update]
    public static bool tbProductTag(string proTag)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProductTag_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@strWhere", proTag));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    public static bool tbProduct_Update(tbProduct _tbProductDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proId", _tbProductDATA.proId));
                dbCmd.Parameters.Add(new SqlParameter("@proCode", _tbProductDATA.proCode));
                dbCmd.Parameters.Add(new SqlParameter("@proName", _tbProductDATA.proName));
                dbCmd.Parameters.Add(new SqlParameter("@proImage", _tbProductDATA.proImage));
                dbCmd.Parameters.Add(new SqlParameter("@proFiles", _tbProductDATA.proFiles));
                dbCmd.Parameters.Add(new SqlParameter("@proContent", _tbProductDATA.proContent));
                dbCmd.Parameters.Add(new SqlParameter("@proDetail", _tbProductDATA.proDetail));
                dbCmd.Parameters.Add(new SqlParameter("@proDate", _tbProductDATA.proDate));
                dbCmd.Parameters.Add(new SqlParameter("@proTitle", _tbProductDATA.proTitle));
                dbCmd.Parameters.Add(new SqlParameter("@proDescription", _tbProductDATA.proDescription));
                dbCmd.Parameters.Add(new SqlParameter("@proKeyword", _tbProductDATA.proKeyword));
                dbCmd.Parameters.Add(new SqlParameter("@proPriority", _tbProductDATA.proPriority));
                dbCmd.Parameters.Add(new SqlParameter("@proIndex", _tbProductDATA.proIndex));
                dbCmd.Parameters.Add(new SqlParameter("@catId", _tbProductDATA.catId));
                dbCmd.Parameters.Add(new SqlParameter("@proType", _tbProductDATA.proType));
                dbCmd.Parameters.Add(new SqlParameter("@proActive", _tbProductDATA.proActive));
                dbCmd.Parameters.Add(new SqlParameter("@proOriginalPrice", _tbProductDATA.proOriginalPrice));
                dbCmd.Parameters.Add(new SqlParameter("@proPrice", _tbProductDATA.proPrice));
                dbCmd.Parameters.Add(new SqlParameter("@proUnitprice", _tbProductDATA.proUnitprice));
                dbCmd.Parameters.Add(new SqlParameter("@proPromotions", _tbProductDATA.proPromotions));
                dbCmd.Parameters.Add(new SqlParameter("@proWeight", _tbProductDATA.proWeight));
                dbCmd.Parameters.Add(new SqlParameter("@proUnits", _tbProductDATA.proUnits));
                dbCmd.Parameters.Add(new SqlParameter("@proOrd", _tbProductDATA.proOrd));
                dbCmd.Parameters.Add(new SqlParameter("@proLang", _tbProductDATA.proLang));
                dbCmd.Parameters.Add(new SqlParameter("@proWarranty", _tbProductDATA.proWarranty));
                dbCmd.Parameters.Add(new SqlParameter("@proTagName", _tbProductDATA.proTagName));
                dbCmd.Parameters.Add(new SqlParameter("@proVideo", _tbProductDATA.proVideo != null ? _tbProductDATA.proVideo : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proCount", _tbProductDATA.proCount != null ? _tbProductDATA.proCount.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proCare", _tbProductDATA.proCare != null ? _tbProductDATA.proCare.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proSpLienQuan", _tbProductDATA.proSpLienQuan != null ? _tbProductDATA.proSpLienQuan.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proStatus", _tbProductDATA.proStatus != null ? _tbProductDATA.proStatus.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proUrl", _tbProductDATA.proUrl != null ? _tbProductDATA.proUrl.ToString() : ""));
                dbCmd.Parameters.Add(new SqlParameter("@proKieuspdaban", _tbProductDATA.proKieuspdaban != null ? _tbProductDATA.proKieuspdaban.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proNoibat", _tbProductDATA.proNoibat != null ? _tbProductDATA.proNoibat.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proBanchay", _tbProductDATA.proBanchay != null ? _tbProductDATA.proBanchay.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@proKM", _tbProductDATA.proKM != null ? _tbProductDATA.proKM.ToString() : "0"));
                dbCmd.Parameters.Add(new SqlParameter("@userId", _tbProductDATA.userId));
                if (!string.IsNullOrEmpty(_tbProductDATA.proTungay + ""))
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proTungay", _tbProductDATA.proTungay));
                }
                else
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proTungay", DBNull.Value));
                }

                if (!string.IsNullOrEmpty(_tbProductDATA.proDenngay + ""))
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proDenngay", _tbProductDATA.proDenngay));
                }
                else
                {
                    dbCmd.Parameters.Add(new SqlParameter("@proDenngay", DBNull.Value));
                }
                dbCmd.Parameters.Add(new SqlParameter("@proNoidungKM", _tbProductDATA.proNoidungKM != null ? _tbProductDATA.proNoidungKM : ""));
                dbCmd.Parameters.Add(new SqlParameter("@manufacturerId", _tbProductDATA.manufacturerId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[sp_tbProduct_UpdateproOriginalPrice]
    public static bool tbProduct_UpdateproOriginalPrice(tbProductDATA _tbProductDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_UpdateproOriginalPrice", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proId", _tbProductDATA.proId));
                dbCmd.Parameters.Add(new SqlParameter("@proOriginalPrice", _tbProductDATA.proOriginalPrice));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbProduct_UpdateproPrice]
    public static bool tbProduct_UpdateproPrice(tbProductDATA _tbProductDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_UpdateproPrice", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proId", _tbProductDATA.proId));
                dbCmd.Parameters.Add(new SqlParameter("@proPrice", _tbProductDATA.proPrice));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbProduct_UpdateproOrd]
    public static bool tbProduct_UpdateproOrd(tbProductDATA _tbProductDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_UpdateproOrd", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proId", _tbProductDATA.proId));
                dbCmd.Parameters.Add(new SqlParameter("@proOrd", _tbProductDATA.proOrd));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbProduct_Delete]
    public static bool tbProduct_Delete(string sproId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@proId", sproId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbProduct_GetByAll]
    public static List<tbProductDATA> tbProduct_GetByAll(string sproLang)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proLang", sproLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetByID]
    public static List<tbProductDATA> tbProduct_GetByID(string proId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proId", proId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbProductDATA> tbProduct_GetByTagName(string proTagName, string strLang)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("tbProduct_GetByTagName", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proTagName", proTagName));
                dbCmd.Parameters.Add(new SqlParameter("@proLang", strLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetByTop]
    public static List<tbProductDATA> tbProduct_GetByTop(string Top, string Where, string Order)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByTop", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Top", Top));
                dbCmd.Parameters.Add(new SqlParameter("@Where", Where));
                dbCmd.Parameters.Add(new SqlParameter("@Order", Order));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetByIndex]
    public static List<tbProductDATA> tbProduct_GetByIndex()
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByIndex", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetBycatId]
    public static List<tbProductDATA> tbProduct_GetBycatId(string scatId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetBycatId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_SelectcatId]
    public static List<tbProductDATA> tbProduct_SelectcatId(string scatId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_SelectcatId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetBycatIdAll]
    public static List<tbProductDATA> tbProduct_GetBycatIdAll(string scatId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetBycatIdAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetBySearch]
    public static DataTable tbProduct_GetByPaging(int currentPage, int pageSize, string Fields, string Filter, string Sort, ref int numofnews)
    {
        cmd = new SqlCommand("sp_tbProduct_Paging");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@CurentPage", currentPage);
        cmd.Parameters.Add("@PageSize", pageSize);
        cmd.Parameters.Add("@Fields", Fields);
        cmd.Parameters.Add("@Filter", Filter);
        cmd.Parameters.Add("@Sort", Sort);
        DataSet ds = common.Get_DataSet(cmd);
        numofnews = Convert.ToInt32(ds.Tables[1].Rows[0]["numofrows"].ToString().Trim());
        return ds.Tables[0];
    }
    #endregion
    #region[tbProduct_GetByHome]
    public static List<tbProductDATA> tbProduct_GetByHome(string scatId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByHome", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetByHome]
    public static List<tbProductDATA> tbProduct_GetByHome2(string scatId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByHome2", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetByHome_Top4]
    public static List<tbProductDATA> tbProduct_GetByHome_Top4(string scatId)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbProduct_GetByHome_Top4", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_ByArrCatID]
    public static List<tbProductDATA> tbProduct_ByArrCatID(string arrCatid)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select * from tbProduct where catId in (" + arrCatid + ") ", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbProduct_GetByID]

    public static List<tbProductDATA> Like_Name_tbPro(string proTagName)
    {
        List<tbProductDATA> list = new List<tbProductDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select * from tbProduct where dbo.FCCH_ReTextUnicode(proName)  like N'%" + proTagName + "%'", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                //dbCmd.Parameters.Add(new SqlParameter("@proTagName", proTagName));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbProductDATA objtbProductDATA = new tbProductDATA(
                        reader["proId"].ToString(),
                        reader["proCode"].ToString(),
                        reader["proName"].ToString(),
                        reader["proImage"].ToString(),
                        reader["proFiles"].ToString(),
                        reader["proContent"].ToString(),
                        reader["proDetail"].ToString(),
                        reader["proDate"].ToString(),
                        reader["proTitle"].ToString(),
                        reader["proDescription"].ToString(),
                        reader["proKeyword"].ToString(),
                        reader["proPriority"].ToString(),
                        reader["proIndex"].ToString(),
                        reader["catId"].ToString(),
                        reader["proType"].ToString(),
                        reader["proActive"].ToString(),
                        reader["proOriginalPrice"].ToString(),
                        reader["proPrice"].ToString(),
                        reader["proUnitprice"].ToString(),
                        reader["proPromotions"].ToString(),
                        reader["proWeight"].ToString(),
                        reader["proUnits"].ToString(),
                        reader["proOrd"].ToString(),
                        reader["proLang"].ToString(),
                        reader["proWarranty"].ToString(),
                        reader["proTagName"].ToString(),
                        reader["proVideo"].ToString(),
                        reader["proCount"].ToString(),
                        reader["proCare"].ToString(),
                        reader["proStatus"].ToString(),
                        reader["manufacturerId"].ToString());
                        list.Add(objtbProductDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}