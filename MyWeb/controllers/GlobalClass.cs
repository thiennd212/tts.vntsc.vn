﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class GlobalClass
{
    #region[Declare variables]
    private static string _conId;
    private static string _conCompany;
    private static string _conAddress;
    private static string _conTel;
    private static string _conFax;
    private static string _conWebsite;
    private static string _conMail_Method;
    private static string _conMail_Smtp;
    private static string _conMail_Port;
    private static string _conMail_Info;
    private static string _conMail_Noreply;
    private static string _conMail_Pass;
    private static string _conNews_Hot;
    private static string _conNews_Subject;
    private static string _conNews_Next;
    private static string _conEditorial;
    private static string _conContact;
    private static string _conContact1;
    private static string _conCopyright;
    private static string _catId;
    private static string _conTitle;
    private static string _conKeywords;
    private static string _conDescription;
    private static string _conLang;
    private static string _SEO;
    private static string _showSEO;
    private static string _viewNews1;
    private static string _viewNews2;
    private static string _viewNews3;
    private static string _viewNews4;
    private static string _viewNews5;
    private static string _viewNews6;
    private static string _viewNews7;
    private static string _viewNews8;
    private static string _viewNews9;
    private static string _viewNews10;
    private static string _viewProducts1;
    private static string _viewProducts2;
    private static string _viewProducts3;
    private static string _viewProducts4;
    private static string _viewProducts5;
    private static string _viewProducts6;
    private static string _viewProducts7;
    private static string _viewProducts8;
    private static string _viewProducts9;
    private static string _viewProducts10;
    private static string _viewProducts11;
    private static string _viewProducts12;
    private static string _viewProducts13;
    private static string _viewProducts14;
    private static string _viewPopup;
    private static string _viewBanner1;
    private static string _viewBanner2;
    private static string _viewBanner3;
    private static string _viewBanner4;
    private static string _viewBanner5;
    private static string _viewBanner6;
    private static string _viewBanner7;
    private static string _viewBanner8;
    private static string _viewSocial1;
    private static string _viewSocial2;
    private static string _viewSocial3;
    private static string _viewSocial4;
    private static string _viewSocial5;
    private static string _viewSocial6;
    private static string _styleYahoo;
    private static string _styleSkype;
    private static string _commentFacebook;
    private static string _widthfkComment;
    private static string _colorfkComment;
    private static string _numfkComment;
    private static string _widthfbLike;
    private static string _heightfbLike;
    private static string _commentFNews;
    private static string _commentFPage;
    private static string _baokim1;
    private static string _baokim2;
    private static string _baokim3;
    private static string _baokim4;
    private static string _nganluong1;
    private static string _nganluong2;
    private static string _nganluong3;
    private static string _nganluong4;
    private static string _pageError;
    private static string _Field1;
    private static string _Field2;
    private static string _Field3;
    private static string _Field4;
    private static string _Field5;
    private static string _FieldHeader;
    private static string _FieldBody;
    private static string _productInline;
    private static bool _ShowpaymentBaoKim;
    private static bool _ShowpaymentNganLuong;
    private static string _Google_AnalyticsId;
    private static string _Google_SearchId;
    private static bool _pageProducts;
    private static bool _showLibrary;
    private static bool _showShareBox;
    private static bool _usingWww;
    private static bool _showOrderButton;
    private static bool _showCoundZoom;
    private static string _CZzoomPosition;
    private static string _CZcaptionPosition;
    private static string _usingLayout;
    private static string _usingStyle;
    private static int _sessionCount;
    private static string _proTooltip;
    private static string _chkLogin;
    private static string _faviconIMG;
    private static string _currencySymbol;
    private static string _styleNews;
    private static string _colorDesign;
    private static string _wIframe;
    private static string _hIframe;
    private static string _wInnerIframe;
    private static string _fInnerIframe;
    private static string _conPageRedirect;
    private static string _typePageRedirect;
    private static string _webfooter;
    private static string _urlWebBase;    
    #endregion
    #region[Public Properties]
    public static string conId { get { return _conId; } set { _conId = value; } }
    public static string viewProducts14 { get { return _viewProducts14; } set { _viewProducts14 = value; } }
    public static string viewPopup { get { return _viewPopup; } set { _viewPopup = value; } }
    public static string conCompany { get { return _conCompany; } set { _conCompany = value; } }
    public static string conAddress { get { return _conAddress; } set { _conAddress = value; } }
    public static string conTel { get { return _conTel; } set { _conTel = value; } }
    public static string conFax { get { return _conFax; } set { _conFax = value; } }
    public static string conWebsite { get { return _conWebsite; } set { _conWebsite = value; } }
    public static string conMail_Method { get { return _conMail_Method; } set { _conMail_Method = value; } }
    public static string conMail_Smtp { get { return _conMail_Smtp; } set { _conMail_Smtp = value; } }
    public static string conMail_Port { get { return _conMail_Port; } set { _conMail_Port = value; } }
    public static string conMail_Info { get { return _conMail_Info; } set { _conMail_Info = value; } }
    public static string conMail_Noreply { get { return _conMail_Noreply; } set { _conMail_Noreply = value; } }
    public static string conMail_Pass { get { return _conMail_Pass; } set { _conMail_Pass = value; } }
    public static string conNews_Hot { get { return _conNews_Hot; } set { _conNews_Hot = value; } }
    public static string conNews_Subject { get { return _conNews_Subject; } set { _conNews_Subject = value; } }
    public static string conNews_Next { get { return _conNews_Next; } set { _conNews_Next = value; } }
    public static string conEditorial { get { return _conEditorial; } set { _conEditorial = value; } }
    public static string conContact { get { return _conContact; } set { _conContact = value; } }
    public static string conContact1 { get { return _conContact1; } set { _conContact1 = value; } }
    public static string conCopyright { get { return _conCopyright; } set { _conCopyright = value; } }
    public static string catId { get { return _catId; } set { _catId = value; } }
    public static string conTitle { get { return _conTitle; } set { _conTitle = value; } }
    public static string conKeywords { get { return _conKeywords; } set { _conKeywords = value; } }
    public static string conDescription { get { return _conDescription; } set { _conDescription = value; } }
    public static string conLang { get { return _conLang; } set { _conLang = value; } }
    public static string SEO { get { return _SEO; } set { _SEO = value; } }
    public static string showSEO { get { return _showSEO; } set { _showSEO = value; } }
    public static string viewNews1 { get { return _viewNews1; } set { _viewNews1 = value; } }
    public static string viewNews2 { get { return _viewNews2; } set { _viewNews2 = value; } }
    public static string viewNews3 { get { return _viewNews3; } set { _viewNews3 = value; } }
    public static string viewNews4 { get { return _viewNews4; } set { _viewNews4 = value; } }
    public static string viewNews5 { get { return _viewNews5; } set { _viewNews5 = value; } }
    public static string viewNews6 { get { return _viewNews6; } set { _viewNews6 = value; } }
    public static string viewNews7 { get { return _viewNews7; } set { _viewNews7 = value; } }
    public static string viewNews8 { get { return _viewNews8; } set { _viewNews8 = value; } }
    public static string viewNews9 { get { return _viewNews9; } set { _viewNews9 = value; } }
    public static string viewNews10 { get { return _viewNews10; } set { _viewNews10 = value; } }
    public static string viewProducts1 { get { return _viewProducts1; } set { _viewProducts1 = value; } }
    public static string viewProducts2 { get { return _viewProducts2; } set { _viewProducts2 = value; } }
    public static string viewProducts3 { get { return _viewProducts3; } set { _viewProducts3 = value; } }
    public static string viewProducts4 { get { return _viewProducts4; } set { _viewProducts4 = value; } }
    public static string viewProducts5 { get { return _viewProducts5; } set { _viewProducts5 = value; } }
    public static string viewProducts6 { get { return _viewProducts6; } set { _viewProducts6 = value; } }
    public static string viewProducts7 { get { return _viewProducts7; } set { _viewProducts7 = value; } }
    public static string viewProducts8 { get { return _viewProducts8; } set { _viewProducts8 = value; } }
    public static string viewProducts9 { get { return _viewProducts9; } set { _viewProducts9 = value; } }
    public static string viewProducts10 { get { return _viewProducts10; } set { _viewProducts10 = value; } }
    public static string viewProducts11 { get { return _viewProducts11; } set { _viewProducts11 = value; } }
    public static string viewProducts12 { get { return _viewProducts12; } set { _viewProducts12 = value; } }
    public static string viewProducts13 { get { return _viewProducts13; } set { _viewProducts13 = value; } }
    public static string viewBanner1 { get { return _viewBanner1; } set { _viewBanner1 = value; } }
    public static string viewBanner2 { get { return _viewBanner2; } set { _viewBanner2 = value; } }
    public static string viewBanner3 { get { return _viewBanner3; } set { _viewBanner3 = value; } }
    public static string viewBanner4 { get { return _viewBanner4; } set { _viewBanner4 = value; } }
    public static string viewBanner5 { get { return _viewBanner5; } set { _viewBanner5 = value; } }
    public static string viewBanner6 { get { return _viewBanner6; } set { _viewBanner6 = value; } }
    public static string viewBanner7 { get { return _viewBanner7; } set { _viewBanner7 = value; } }
    public static string viewBanner8 { get { return _viewBanner8; } set { _viewBanner8 = value; } }
    public static string viewSocial1 { get { return _viewSocial1; } set { _viewSocial1 = value; } }
    public static string viewSocial2 { get { return _viewSocial2; } set { _viewSocial2 = value; } }
    public static string viewSocial3 { get { return _viewSocial3; } set { _viewSocial3 = value; } }
    public static string viewSocial4 { get { return _viewSocial4; } set { _viewSocial4 = value; } }
    public static string viewSocial5 { get { return _viewSocial5; } set { _viewSocial5 = value; } }
    public static string viewSocial6 { get { return _viewSocial6; } set { _viewSocial6 = value; } }
    public static string styleYahoo { get { return _styleYahoo; } set { _styleYahoo = value; } }
    public static string styleSkype { get { return _styleSkype; } set { _styleSkype = value; } }
    public static string commentFacebook { get { return _commentFacebook; } set { _commentFacebook = value; } }
    public static string widthfkComment { get { return _widthfkComment; } set { _widthfkComment = value; } }
    public static string colorfkComment { get { return _colorfkComment; } set { _colorfkComment = value; } }
    public static string numfkComment { get { return _numfkComment; } set { _numfkComment = value; } }
    public static string widthfbLike { get { return _widthfbLike; } set { _widthfbLike = value; } }
    public static string heightfbLike { get { return _heightfbLike; } set { _heightfbLike = value; } }
    public static string commentFNews { get { return _commentFNews; } set { _commentFNews = value; } }
    public static string commentFPage { get { return _commentFPage; } set { _commentFPage = value; } }
    public static string baokim1 { get { return _baokim1; } set { _baokim1 = value; } }
    public static string baokim2 { get { return _baokim2; } set { _baokim2 = value; } }
    public static string baokim3 { get { return _baokim3; } set { _baokim3 = value; } }
    public static string baokim4 { get { return _baokim4; } set { _baokim4 = value; } }
    public static string nganluong1 { get { return _nganluong1; } set { _nganluong1 = value; } }
    public static string nganluong2 { get { return _nganluong2; } set { _nganluong2 = value; } }
    public static string nganluong3 { get { return _nganluong3; } set { _nganluong3 = value; } }
    public static string nganluong4 { get { return _nganluong4; } set { _nganluong4 = value; } }
    public static string pageError { get { return _pageError; } set { _pageError = value; } }
    public static string Field1 { get { return _Field1; } set { _Field1 = value; } }
    public static string Field2 { get { return _Field2; } set { _Field2 = value; } }
    public static string Field3 { get { return _Field3; } set { _Field3 = value; } }
    public static string Field4 { get { return _Field4; } set { _Field4 = value; } }
    public static string Field5 { get { return _Field5; } set { _Field5 = value; } }
    public static string FieldHeader { get { return _FieldHeader; } set { _FieldHeader = value; } }
    public static string FieldBody { get { return _FieldBody; } set { _FieldBody = value; } }
    public static bool ShowpaymentBaoKim { get { return _ShowpaymentBaoKim; } set { _ShowpaymentBaoKim = value; } }
    public static bool ShowpaymentNganLuong { get { return _ShowpaymentNganLuong; } set { _ShowpaymentNganLuong = value; } }
    public static string Google_Analytics_Id { get { return _Google_AnalyticsId; } set { _Google_AnalyticsId = value; } }
    public static string Google_Search_Id { get { return _Google_SearchId; } set { _Google_SearchId = value; } }
    public static string ProductInline { get { return _productInline; } set { _productInline = value; } }
    public static bool pageProducts { get { return _pageProducts; } set { _pageProducts = value; } }
    public static bool showLibrary { get { return _showLibrary; } set { _showLibrary = value; } }
    public static bool showShareBox { get { return _showShareBox; } set { _showShareBox = value; } }
    public static bool using_www { get { return _usingWww; } set { _usingWww = value; } }
    public static bool showOrderButton { get { return _showOrderButton; } set { _showOrderButton = value; } }
    public static bool showCoundZoom { get { return _showCoundZoom; } set { _showCoundZoom = value; } }
    public static string usingLayout { get { return _usingLayout; } set { _usingLayout = value; } }
    public static string usingStyle { get { return _usingStyle; } set { _usingStyle = value; } }
    public static string CZzoomPosition { get { return _CZzoomPosition; } set { _CZzoomPosition = value; } }
    public static string CZcaptionPosition { get { return _CZcaptionPosition; } set { _CZcaptionPosition = value; } }
    public static int SessionCount { get { return _sessionCount; } set { _sessionCount = value; } }
    public static string proTooltip { get { return _proTooltip; } set { _proTooltip = value; } }
    public static string chkLogin { get { return _chkLogin; } set { _chkLogin = value; } }
    public static string faviconIMG { get { return _faviconIMG; } set { _faviconIMG = value; } }
    public static string CurrencySymbol { get { return _currencySymbol; } set { _currencySymbol = value; } }
    public static string styleNews { get { return _styleNews; } set { _styleNews = value; } }
    public static string colorDesign { get { return _colorDesign; } set { _colorDesign = value; } }
    public static string wIframe { get { return _wIframe; } set { _wIframe = value; } }
    public static string hIframe { get { return _hIframe; } set { _hIframe = value; } }
    public static string wInnerIframe { get { return _wInnerIframe; } set { _wInnerIframe = value; } }
    public static string fInnerIframe { get { return _fInnerIframe; } set { _fInnerIframe = value; } }
    public static string webfooter { get { return _webfooter; } set { _webfooter = value; } }
    public static string urlWebBase { get { return _urlWebBase; } set { _urlWebBase = value; } }

    public static string ConPageRedirect
    {
        get { return _conPageRedirect; }
        set { _conPageRedirect = value; }
    }
    public static string TypePageRedirect
    {
        get { return _typePageRedirect; }
        set { _typePageRedirect = value; }
    }
    #endregion
    static GlobalClass()
    {

    }
}