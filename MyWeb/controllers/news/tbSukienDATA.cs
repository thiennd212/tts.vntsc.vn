﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbSukienDATA
{
    #region[Declare variables]
    private string _ID;
    private string _skName;
    private string _skLevel;
    private string _skOrd;
    #endregion
    #region[Function]
    public tbSukienDATA() { }
    public tbSukienDATA(string ID_, string skName_, string skLevel_, string skOrd_)
    {
        _ID = ID_;
        _skName = skName_;
        _skLevel = skLevel_;
        _skOrd = skOrd_;
    }
    #endregion
    #region[Assigned value]
    public string ID { get { return _ID; } set { _ID = value; } }
    public string skName { get { return _skName; } set { _skName = value; } }
    public string skLevel { get { return _skLevel; } set { _skLevel = value; } }
    public string skOrd { get { return _skOrd; } set { _skOrd = value; } }
    #endregion
}