﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Configuration;

namespace MyWeb
{
    public class Global : System.Web.HttpApplication
    {
        public static string LangDefault = "vi";
        static readonly string LangAdm = "LangAdm";
        static readonly string Lang = "Lang";
        public static DataTable langData;
        public static CookieClass Cookie = new CookieClass();
        public static dataAccessDataContext db = new dataAccessDataContext();
        protected void Application_Start(object sender, EventArgs e)
        {            
            try
            {
                using (dataAccessDataContext ctx = new dataAccessDataContext())
                {
                    ctx.ExecuteCommand(@"
                    alter table [tbConfig] add [conHeader] nvarchar(max)
                    alter table [tbConfig] add	[conBody] nvarchar(max)
                    alter table [tbConfig] add	[conPageRedirect] ntext
                    alter table [tbConfig] add	[typePageRedirect] int");
                    ctx.ExecuteCommand(@"ALTER PROCEDURE [dbo].[SP_tbConfig_Add] 
                    @conCompany nvarchar(200) = null, @conAddress nvarchar(350) = null, @conTel varchar(150) = null, @conFax varchar(50) = null, @conWebsite varchar(100) = null, @conMail_Method varchar(100) = null, @conMail_Smtp varchar(50) = null, @conMail_Port smallint = null, @conMail_Info varchar(150) = null, @conMail_Noreply varchar(50) = null, @conMail_Pass varchar(50) = null, @conNews_Hot smallint = null, @conNews_Subject smallint = null, @conNews_Next smallint = null, @conEditorial ntext = null, @conContact ntext = null, @conContact1 ntext = null, @conCopyright ntext = null, @catId int = null, @conTitle nvarchar(300) = null, @conKeywords nvarchar(300) = null, @conDescription nvarchar(300) = null, @conLang char(3) = null, @SEO bit = null, @showSEO bit = null, @viewNews nvarchar(500), @viewProducts nvarchar(500), @viewBanner nvarchar(500), @viewSocial nvarchar(500), @viewPayment nvarchar(500), @sortBy varchar(50) = null, @viewBy varchar(50) = null, @pageError ntext = null, @livechat ntext = null,
                    @conPageRedirect ntext=null,@typePageRedirect int =null
                    AS
                    Insert into [dbo].[tbConfig] ( [conCompany], [conAddress], [conTel], [conFax], [conWebsite], [conMail_Method], [conMail_Smtp], [conMail_Port], [conMail_Info], [conMail_Noreply], [conMail_Pass], [conNews_Hot], [conNews_Subject], [conNews_Next], [conEditorial], [conContact], [conContact1], [conCopyright], [catId], [conTitle], [conKeywords], [conDescription], [conLang], [SEO], [showSEO], [viewNews], [viewProducts], [viewBanner], [viewSocial], [viewPayment], [sortBy], [viewBy], [pageError], [livechat],[conPageRedirect],[typePageRedirect] )
                    Values ( @conCompany, @conAddress, @conTel, @conFax, @conWebsite, @conMail_Method, @conMail_Smtp, @conMail_Port, @conMail_Info, @conMail_Noreply, @conMail_Pass, @conNews_Hot, @conNews_Subject, @conNews_Next, @conEditorial, @conContact, @conContact1, @conCopyright, @catId, @conTitle, @conKeywords, @conDescription, @conLang, @SEO, @showSEO, @viewNews, @viewProducts, @viewBanner, @viewSocial, @viewPayment, @sortBy, @viewBy, @pageError, @livechat,@conPageRedirect,@typePageRedirect )");
                    ctx.ExecuteCommand(@"
                    ALTER PROCEDURE [dbo].[SP_tbConfig_Update]
                    @conid int,
                    @conCompany nvarchar(200) = null,
                    @conAddress nvarchar(350) = null,
                    @conTel varchar(150) = null,
                    @conFax varchar(50) = null,
                    @conWebsite varchar(100) = null,
                    @conMail_Method varchar(100) = null,
                    @conMail_Smtp varchar(250) = null,
                    @conMail_Port smallint = null,
                    @conMail_Info varchar(150) = null,
                    @conMail_Noreply varchar(50) = null,
                    @conMail_Pass varchar(50) = null,
                    @conNews_Hot smallint = null,
                    @conNews_Subject smallint = null,
                    @conNews_Next smallint = null,
                    @conEditorial ntext = null,
                    @conContact ntext = null,
                    @conContact1 ntext = null,
                    @conCopyright ntext = null,
                    @catId int = null,
                    @conTitle nvarchar(300) = null,
                    @conKeywords nvarchar(300) = null,
                    @conDescription nvarchar(300) = null,
                    @conLang char(3) = null,
                    @SEO bit = null,
                    @showSEO bit = null,
                    @viewNews nvarchar(500),
                    @viewProducts nvarchar(500),
                    @viewBanner nvarchar(500),
                    @viewSocial nvarchar(500),
                    @viewPayment nvarchar(500),
                    @sortBy varchar(50) = null,
                    @viewBy varchar(50) = null,
                    @pageError ntext = null,
                    @livechat ntext = null,@conPageRedirect ntext=null,@typePageRedirect int =null
                    AS
                    UPDATE [dbo].[tbConfig]
                    SET
                    [conCompany] = @conCompany,
                    [conAddress] = @conAddress,
                    [conTel] = @conTel,
                    [conFax] = @conFax,
                    [conWebsite] = @conWebsite,
                    [conMail_Method] = @conMail_Method,
                    [conMail_Smtp] = @conMail_Smtp,
                    [conMail_Port] = @conMail_Port,
                    [conMail_Info] = @conMail_Info,
                    [conMail_Noreply] = @conMail_Noreply,
                    [conMail_Pass] = @conMail_Pass,
                    [conNews_Hot] = @conNews_Hot,
                    [conNews_Subject] = @conNews_Subject,
                    [conNews_Next] = @conNews_Next,
                    [conEditorial] = @conEditorial,
                    [conContact] = @conContact,
                    [conContact1] = @conContact1,
                    [conCopyright] = @conCopyright,
                    [catId] = @catId,
                    [conTitle] = @conTitle,
                    [conKeywords] = @conKeywords,
                    [conDescription] = @conDescription,
                    [conLang] = @conLang,
                    [SEO] = @SEO,
                    [showSEO] = @showSEO,
                    [viewNews] = @viewNews,
                    [viewProducts] = @viewProducts,
                    [viewBanner] = @viewBanner,
                    [viewSocial] = @viewSocial,
                    [viewPayment] = @viewPayment,
                    [sortBy] = @sortBy,
                    [viewBy] = @viewBy,
                    [pageError] = @pageError,
                    [livechat] = @livechat,
                    [conPageRedirect]=@conPageRedirect,[typePageRedirect]=@typePageRedirect
                    WHERE [conid] = @conid");
                }
                common.RunScriptFile("Update082015.sql", false);
            }
            catch
            {
            }
            tbLanguage lang = db.tbLanguages.FirstOrDefault(s => s.lanDefault == true);
            if (lang != null)
                LangDefault = lang.lanId.Trim().ToLower();

            LoadConfig(LangDefault);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Cookie.SetCookie("Lang", LangDefault);
                Session["Lang"] = LangDefault;
            }
            if (Session["LangAdm"] == null)
            {
                Cookie.SetCookie("LangAdm", LangDefault);
                Session["LangAdm"] = LangDefault;
            }
            GetLangData(Session["Lang"].ToString());
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (GlobalClass.using_www)
            {
                if (Request.Url.Host != "localhost" && !Request.Url.Host.StartsWith("www") && !Request.Url.IsLoopback)
                {
                    UriBuilder builder = new UriBuilder(Request.Url);
                    builder.Host = "www." + Request.Url.Host;
                    builder.Path = builder.Path.ToLower().Replace("/default.aspx", "");
                    Response.StatusCode = 301;
                    Response.AddHeader("Location", builder.ToString());
                    Response.End();
                }
            }
            else
            {
                if (Request.Url.Host != "localhost" && Request.Url.Host.StartsWith("www") && !Request.Url.IsLoopback)
                {
                    UriBuilder builder = new UriBuilder(Request.Url);
                    builder.Host = Request.Url.Host.Replace("www.", "");
                    builder.Path = builder.Path.ToLower().Replace("/default.aspx", "");
                    Response.StatusCode = 301;
                    Response.AddHeader("Location", builder.ToString());
                    Response.End();
                }
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session["Lang"] = LangDefault;
            }
            if (Session["LangAdm"] == null)
            {
                Session["LangAdm"] = LangDefault;
            }

            MyWeb.Common.CookieClass cookie = new MyWeb.Common.CookieClass();
            string sessionCount = cookie.GetCookie("SessionCount");
            if (!String.IsNullOrEmpty(sessionCount))
                sessionCount = sessionCount == "" ? "0" : sessionCount;
            cookie.SetCookie("SessionCount", (int.Parse(sessionCount) + 1).ToString());
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        public static string GetLangKey(string key)
        {
            try
            {
                return langData.AsEnumerable().FirstOrDefault(row => string.Equals(row["Id"].ToString(), key, StringComparison.OrdinalIgnoreCase))["Name"].ToString();
            }
            catch { return ""; }
        }

        public static string CurrentLangAdm
        {
            get
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session[LangAdm] == null)
                {
                    return LangDefault;
                }
                return HttpContext.Current.Session[LangAdm] as string;
            }
            set
            {
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session[LangAdm] = value;
                }
            }
        }

        public static string GetLangAdm()
        {
            return CurrentLangAdm;
        }

        public static string CurrentLang
        {
            get
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session[Lang] == null)
                {
                    return LangDefault;
                }
                return HttpContext.Current.Session[Lang] as string;
            }
            set
            {
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session[Lang] = value;
                }
            }
        }

        public static string GetLang()
        {
            return CurrentLang;
        }

        public static void LoadConfig(string Lang)
        {
            string cacheKey = "config" + Lang;
            GlobalClass.urlWebBase = ConfigurationManager.AppSettings["url-web-base"];
            List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(Lang);
            var config = db.tbConfigs.FirstOrDefault(x => x.conLang == Lang);
            if (list.Count > 0)
            {
                string[] viewNews = list[0].viewNews.Split(Convert.ToChar(","));
                string[] viewProducts = list[0].viewProducts.Split(Convert.ToChar(","));
                string[] viewBanner = list[0].viewBanner.Split(Convert.ToChar(","));
                string[] viewSocial = list[0].viewSocial.Split(Convert.ToChar(","));
                string[] conMail_Smtp = list[0].conMail_Smtp.Split(Convert.ToChar(","));
                string[] viewPayment = list[0].viewPayment.Split(Convert.ToChar(","));
                GlobalClass.conId = list[0].conid;
                GlobalClass.catId = list[0].catId;
                GlobalClass.conAddress = list[0].conAddress;
                GlobalClass.conCompany = list[0].conCompany;
                GlobalClass.conContact = list[0].conContact;
                GlobalClass.conContact1 = list[0].conContact1;
                GlobalClass.conCopyright = list[0].conCopyright;
                GlobalClass.conEditorial = list[0].conEditorial;
                GlobalClass.conFax = list[0].conFax;
                GlobalClass.conLang = list[0].conLang;
                GlobalClass.conMail_Info = list[0].conMail_Info;
                GlobalClass.conMail_Noreply = list[0].conMail_Noreply;
                GlobalClass.conMail_Method = list[0].conMail_Method;
                GlobalClass.conMail_Pass = list[0].conMail_Pass;
                GlobalClass.conMail_Port = list[0].conMail_Port;
                GlobalClass.conMail_Smtp = list[0].conMail_Smtp;
                GlobalClass.conNews_Hot = list[0].conNews_Hot;
                GlobalClass.conNews_Next = list[0].conNews_Next;
                GlobalClass.conNews_Subject = list[0].conNews_Subject;
                GlobalClass.conTel = list[0].conTel;
                GlobalClass.conWebsite = list[0].conWebsite;
                GlobalClass.SEO = list[0].SEO;
                GlobalClass.showSEO = list[0].showSEO;
                GlobalClass.viewPopup = config.conPopup.ToString();

                try
                {
                    GlobalClass.viewNews1 = viewNews[0];
                    GlobalClass.viewNews2 = viewNews[1];
                    GlobalClass.viewNews3 = viewNews[2];
                    GlobalClass.viewNews4 = viewNews[3];
                    GlobalClass.viewNews5 = viewNews[4];
                    GlobalClass.viewNews6 = viewNews[5];
                    GlobalClass.viewNews7 = viewNews[6];
                    GlobalClass.viewNews8 = viewNews[7];
                    GlobalClass.viewNews9 = viewNews[8];
                    GlobalClass.viewNews10 = viewNews[9];
                }
                catch{}


                try
                {
                    GlobalClass.viewProducts1 = viewProducts[0];
                    GlobalClass.viewProducts2 = viewProducts[1];
                    GlobalClass.viewProducts3 = viewProducts[2];
                    GlobalClass.viewProducts4 = viewProducts[3];
                    GlobalClass.viewProducts5 = viewProducts[4];
                    GlobalClass.viewProducts6 = viewProducts[5];
                    GlobalClass.viewProducts7 = viewProducts[6];
                    GlobalClass.viewProducts8 = viewProducts[7];
                    GlobalClass.viewProducts9 = viewProducts[8];
                    GlobalClass.viewProducts10 = viewProducts[9];
                    GlobalClass.viewProducts11 = viewProducts[10];
                    GlobalClass.viewProducts12 = viewProducts[11];
                    GlobalClass.viewProducts13 = viewProducts[12];
                    GlobalClass.viewProducts14 = viewProducts[13];
                }
                catch{}
                


                GlobalClass.viewBanner1 = viewBanner[0];
                GlobalClass.viewBanner2 = viewBanner[1];
                GlobalClass.viewBanner3 = viewBanner[2];
                GlobalClass.viewBanner4 = viewBanner[3];
                GlobalClass.viewBanner5 = viewBanner[4];
                GlobalClass.viewBanner6 = viewBanner[5];
                GlobalClass.viewBanner7 = viewBanner[6];
                GlobalClass.viewBanner8 = viewBanner[7];


                GlobalClass.baokim1 = viewPayment[0];
                GlobalClass.baokim2 = viewPayment[1];
                GlobalClass.baokim3 = viewPayment[2];
                GlobalClass.baokim4 = viewPayment[3];
                GlobalClass.nganluong1 = viewPayment[4];
                GlobalClass.nganluong2 = viewPayment[5];
                GlobalClass.nganluong3 = viewPayment[6];
                GlobalClass.nganluong4 = viewPayment[7];


                GlobalClass.viewSocial1 = viewSocial.Length > 0 ? viewSocial[0] : "";
                GlobalClass.viewSocial2 = viewSocial.Length > 1 ? viewSocial[1] : "";
                GlobalClass.viewSocial3 = viewSocial.Length > 2 ? viewSocial[2] : "";
                GlobalClass.viewSocial4 = viewSocial.Length > 3 ? viewSocial[3] : "";
                GlobalClass.viewSocial5 = viewSocial.Length > 4 ? viewSocial[4] : "";
                GlobalClass.viewSocial6 = viewSocial.Length > 5 ? viewSocial[5] : "";


                GlobalClass.styleYahoo = viewSocial.Length > 6 ? viewSocial[6] : "2";
                GlobalClass.commentFacebook = viewSocial.Length > 7 ? viewSocial[7] : "0";
                GlobalClass.styleSkype = viewSocial.Length > 8 ? viewSocial[8] : "smallclassic";
                GlobalClass.widthfbLike = viewSocial.Length > 9 ? viewSocial[9] : "222";
                GlobalClass.heightfbLike = viewSocial.Length > 10 ? viewSocial[10] : "250";
                GlobalClass.widthfkComment = viewSocial.Length > 11 ? viewSocial[11] : "770";
                GlobalClass.numfkComment = viewSocial.Length > 12 ? viewSocial[12] : "5";
                GlobalClass.colorfkComment = viewSocial.Length > 13 ? viewSocial[13] : "light";
                GlobalClass.commentFNews = viewSocial.Length > 14 ? viewSocial[14] : "0";
                GlobalClass.commentFPage = viewSocial.Length > 15 ? viewSocial[15] : "0";
                GlobalClass.pageError = list[0].pageError;
                GlobalClass.Field1 = list[0].Field1;
                GlobalClass.Field2 = list[0].Field2;
                GlobalClass.Field3 = list[0].Field3;
                GlobalClass.Field4 = list[0].Field4;
                //GlobalClass.Field5 = list[0].Field5;
                GlobalClass.chkLogin = list[0].Field5;
                GlobalClass.faviconIMG = list[0].conEditorial;
                GlobalClass.styleNews = list[0].catId;
                GlobalClass.ShowpaymentBaoKim = Common.GlobalClass.GetAppSettingKey("show_payment_BaoKim") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("show_payment_BaoKim")) : false;
                GlobalClass.ShowpaymentNganLuong = Common.GlobalClass.GetAppSettingKey("show_payment_NganLuong") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("show_payment_NganLuong")) : false;
                GlobalClass.ProductInline = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("product_in_line")) ? Common.GlobalClass.GetAppSettingKey("product_in_line") : "3";
                GlobalClass.Google_Analytics_Id = Common.GlobalClass.GetAppSettingKey("google_analytics_id") != null ? Common.GlobalClass.GetAppSettingKey("google_analytics_id") : "";
                GlobalClass.Google_Search_Id = Common.GlobalClass.GetAppSettingKey("google_search_id") != null ? Common.GlobalClass.GetAppSettingKey("google_search_id") : "";
                GlobalClass.pageProducts = Common.GlobalClass.GetAppSettingKey("page_products") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("page_products")) : false;
                GlobalClass.showLibrary = Common.GlobalClass.GetAppSettingKey("show_library") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("show_library")) : false;
                GlobalClass.showShareBox = Common.GlobalClass.GetAppSettingKey("show_share_box") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("show_share_box")) : false;
                GlobalClass.using_www = Common.GlobalClass.GetAppSettingKey("using_www") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("using_www")) : false;
                GlobalClass.showOrderButton = Common.GlobalClass.GetAppSettingKey("show_order_button") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("show_order_button")) : false;
                GlobalClass.showCoundZoom = Common.GlobalClass.GetAppSettingKey("show_cound_zoom") != null ? bool.Parse(Common.GlobalClass.GetAppSettingKey("show_cound_zoom")) : false;
                GlobalClass.usingLayout = Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("using_layout")) ? Common.GlobalClass.GetAppSettingKey("using_layout") : "Default";
                GlobalClass.usingStyle = Common.GlobalClass.GetAppSettingKey("using_style") != null ? Common.GlobalClass.GetAppSettingKey("using_style") : "css";
                GlobalClass.SessionCount = Common.GlobalClass.GetAppSettingKey("session_count") != null ? int.Parse(Common.GlobalClass.GetAppSettingKey("session_count")) : 1;
                GlobalClass.CZzoomPosition = Common.GlobalClass.GetAppSettingKey("cz_zoom_position") != null ? Common.GlobalClass.GetAppSettingKey("cz_zoom_position") : "3";
                GlobalClass.CZcaptionPosition = Common.GlobalClass.GetAppSettingKey("cz_caption_position") != null ? Common.GlobalClass.GetAppSettingKey("cz_caption_position") : "top";
                GlobalClass.proTooltip = Common.GlobalClass.GetAppSettingKey("product_tooltip") != null ? Common.GlobalClass.GetAppSettingKey("product_tooltip") : "textonly";
                GlobalClass.CurrencySymbol = MyWeb.Common.StringClass.Check(MyWeb.Global.GetLangKey("currency_symbol")) ? MyWeb.Global.GetLangKey("currency_symbol") : "đ";
                GlobalClass.colorDesign = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("colorDesign")) ? Common.GlobalClass.GetAppSettingKey("colorDesign") : "#000000";
                GlobalClass.wIframe = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("wIframe")) ? Common.GlobalClass.GetAppSettingKey("wIframe") : "290px";
                GlobalClass.hIframe = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("hIframe")) ? Common.GlobalClass.GetAppSettingKey("hIframe") : "253px";
                GlobalClass.wInnerIframe = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("wInnerIframe")) ? Common.GlobalClass.GetAppSettingKey("wInnerIframe") : "270px";
                GlobalClass.fInnerIframe = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("fInnerIframe")) ? Common.GlobalClass.GetAppSettingKey("fInnerIframe") : "12px";
                GlobalClass.webfooter = MyWeb.Common.StringClass.Check(Common.GlobalClass.GetAppSettingKey("webfooter")) ? Common.GlobalClass.GetAppSettingKey("webfooter") : "true";
                if (GlobalClass.SEO.Equals("True"))
                {
                    GlobalClass.conDescription = list[0].conDescription + " | " + MyWeb.Common.StringClass.NameToTag(list[0].conDescription);
                    GlobalClass.conKeywords = list[0].conKeywords + " | " + MyWeb.Common.StringClass.NameToTag(list[0].conKeywords);
                    GlobalClass.conTitle = list[0].conTitle + " | " + MyWeb.Common.StringClass.NameToTag(list[0].conTitle);
                }
                else
                {
                    GlobalClass.conDescription = list[0].conDescription;
                    GlobalClass.conKeywords = list[0].conKeywords;
                    GlobalClass.conTitle = list[0].conTitle;
                }
                GetLangData(Lang);
            }
        }

        private static void GetLangData(string lang)
        {
            lang = lang == "" ? "vi" : lang;
            string langPath = "/uploads/language/" + lang + ".xml";
            DataSet ds = new DataSet();
            try
            {
                ds.ReadXml(System.Web.Hosting.HostingEnvironment.MapPath(langPath));
            }
            catch { }
            langData = ds.Tables["Items"];
        }
    }
}