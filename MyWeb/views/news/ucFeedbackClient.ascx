﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFeedbackClient.ascx.cs" Inherits="MyWeb.views.news.ucFeedbackClient" %>

<div  class="box-feed-sub">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <asp:Literal ID="ltrName" runat="server"></asp:Literal>
        </div>
    </div>
    <asp:Literal ID="ltrlist" runat="server"></asp:Literal>
    <div class="clearfix">
        <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
    </div>
</div>