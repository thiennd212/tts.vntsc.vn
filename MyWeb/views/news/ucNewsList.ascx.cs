﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucNewsList : System.Web.UI.UserControl
    {
        string idcha = "";
        string grnid = "";
        string lang = "vi";
        string p = "";
        string strWhere = "";
        int currentPage = 1;
        int recordCount = 0;
        int pageSize = 10;        
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["hp"] != null)
            {
                var pagNew = db.tbPages.Where(s => s.pagType == 200 && s.pagTagName == Request["hp"].ToString()).FirstOrDefault();
                if (Request["hp"].ToString().Contains("?"))
                {
                    pagNew = db.tbPages.Where(s => s.pagType == 200 && s.pagTagName == Request["hp"].ToString().Split('?')[0]).FirstOrDefault();
                }
                if (pagNew != null)
                {
                    grnid = pagNew.pagTagName;
                    idcha = pagNew.pagId.ToString();
                }
            }
            try
            {
                currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            }
            catch { }
            try { pageSize = int.Parse(GlobalClass.viewNews1); }
            catch { }
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                if (grnid != "")
                {
                    if (GlobalClass.styleNews == "2")
                    {
                        //BindDataAll();
                        BindDataGroupChild();
                    }
                    else
                    {
                        //BindData();
                        BindDataAll();
                    }
                    BindAdv(idcha);
                }
                else
                {
                    BindDataGroup();
                }
            }
        }

        private string LoadWhere()
        {
            strWhere = "newActive = '0' and newLang = '" + lang + "' and grnID = '" + idcha + "'";
            Page.Title = MyWeb.Global.GetLangKey("news") + " | " + GlobalClass.conTitle;
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(idcha);
            if (list.Count > 0)
            {
                Session["Lang"] = list[0].pagLang.Trim().ToLower();
                if (GlobalClass.showSEO.Equals("true"))
                {
                    Page.Title = list[0].pagName + " | " + GlobalClass.conTitle;
                }
                else
                {
                    Page.Title = list[0].pagName;
                }
                strWhere += " and grnID in (select pagId from tbPage where paglevel like (select paglevel from tbPage where pagId ='" + idcha + "' and pagType = '200') + '%')";
            }
            recordCount = int.Parse(tbNewsDB.tbNews_Count("Select Count(*) from tbNews Where " + strWhere));
            return strWhere;
        }

        private void BindData()
        {
            LoadWhere();
            List<tbNewsDATA> list = new List<tbNewsDATA>();
            list = tbNewsDB.tbNews_Paging((currentPage).ToString(), pageSize.ToString(), "*", strWhere, "newDate desc,newId desc");
            tbPage gn = db.tbPages.Where(s => s.pagTagName == grnid.Replace(".html", "") && s.pagType == 200).FirstOrDefault();
            ltrName.Text = gn.pagName;
            ltrGroupDes.Text = String.IsNullOrEmpty(gn.pagDetail) ? "" : "<div class='newGroupDes'>" + gn.pagDetail + "</div>";
            string _str = "";
            if (list.Count > 0)
            {
                _str += "<div class=\"box-body clearfix\">";
                for (int i = 0; i < list.Count; i++)
                {
                    tbNew dbNew = db.tbNews.FirstOrDefault(s => s.newid == Int32.Parse(list[i].newid));
                    var tbgrn = db.tbPages.Where(g => g.pagId == int.Parse(list[i].grnID)).First();
                    string alt = dbNew.newAlt != "" && dbNew.newAlt != null ? dbNew.newAlt : dbNew.newName;
                    string link = "/" + list[i].newTagName + ".html";
                    string image = list[i].newImage != null && list[i].newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, list[i].newName, list[i].newImage, alt) : "";
                    _str += String.Format("<div class=\"cat-news clearfix\">{2}<h3><a href=\"{0}\" title=\"{1}\">{1}</a> </h3><div class=\"clearfix\">{3}</div><div class=\"news-more\" ><a href=\"{0}\" rel=\"nofollow\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, list[i].newName, image, list[i].newContent.ToString());
                }
                _str += "</div>";
                ltrlist.Text = _str;
            }
            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
        }

        private void BindDataGroupChild()
        {
            int viewNew9 = (!string.IsNullOrEmpty(GlobalClass.viewNews9)) ? Convert.ToInt32(GlobalClass.viewNews9) : 4;

            tbPage gn = db.tbPages.Where(s => s.pagTagName == grnid.Replace(".html", "") && s.pagType == 200).FirstOrDefault();

            var listpage = db.tbPages.Where(u => u.pagActive == 1 && u.paglevel.Substring(0, gn.paglevel.Length) == gn.paglevel && u.pagId != gn.pagId).Select(u => u.pagId).ToArray();

            string _str = "";

            foreach (int pagNew in listpage)
            {
                IEnumerable<tbNew> news;
                var pagCurrent = db.tbPages.FirstOrDefault(x => x.pagId == pagNew);

                _str += "<div class=\"cate-header sub-top\"><div class=\"txt-name-sub\"><a href=\"" + pagCurrent.pagLink + "\" title=\"" + pagCurrent.pagName + "\">" + pagCurrent.pagName + "</a></div></div>";
                ltrGroupDes.Text = pagCurrent.pagDetail;
                var newall = db.tbNews.Where(u => u.newActive == 0 && u.newLang == lang && u.grnID == pagNew).OrderByDescending(u => u.newid).ToList();
                news = newall.Skip(0).Take(viewNew9).ToList();
                recordCount = newall.Count;
                if (news.Count() > 0)
                {
                    _str += "<div class=\"box-body homepage clearfix\">";
                    string col_tem1 = "", col_tem2 = "";
                    if (GlobalClass.viewNews10 == "3")
                    {
                        col_tem1 = " twocol1";
                        col_tem2 = " twocol2";
                    }
                    int i = 0;
                    if (GlobalClass.viewNews10 != "1")
                    {
                        i = 0;
                        foreach (tbNew n in news)
                        {
                            string alt = n.newAlt != "" && n.newAlt != null ? n.newAlt : n.newName;
                            string link = "/" + n.newTagName + ".html";
                            string image = n.newImage != null && n.newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, n.newName, n.newImage, alt) : "";
                            if (i == 0)
                                _str += String.Format("<div class=\"cat-news" + col_tem1 + " clearfix\">{2}<h3><a href=\"{0}\" title=\"{1}\">{1}</a> </h3><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.newName, image, n.newContent.ToString());
                            else
                            {
                                if (i == 1)
                                    _str += "<div class=\"cat-morenews" + col_tem2 + " clearfix\"><ul>";
                                _str += String.Format("<li><a href=\"" + link + "\" title=\"" + n.newName + "\">" + n.newName + "</a> </li>");
                                if (i == news.Count() - 1)
                                    _str += "</ul></div>";
                            }
                            i++;
                        }
                    }
                    else
                    {
                        foreach (tbNew n in news)
                        {
                            string alt = n.newAlt != "" && n.newAlt != null ? n.newAlt : n.newName;
                            string link = "/" + n.newTagName + ".html";
                            string image = n.newImage != null && n.newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, n.newName, n.newImage, alt) : "";
                            _str += String.Format("<div class=\"cat-news clearfix\">{2}<h4><a href=\"{0}\" title=\"{1}\">{1}</a> </h4><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.newName, image, n.newContent.ToString());
                            i++;
                        }
                    }
                    _str += "<div class=\"clear\"></div>";
                    _str += "</div>";
                }
                ltrGroupNews.Text = _str;
            }
        }

        void BindDataAll()
        {
            string curUrl = Request.RawUrl;
            string[] mang = curUrl.Split('=');
            string page = "1";
            if (mang.Length == 2)
            {
                if (mang[1] != null)
                {
                    page = mang[1];
                    currentPage = Convert.ToInt32(page) - 1;
                }
            }
            else
            {
                currentPage = 0;
            }
            tbPage gn = db.tbPages.Where(s => s.pagTagName == grnid.Replace(".html", "") && s.pagType == 200).FirstOrDefault();
            ltrName.Text = gn.pagName;
            ltrGroupDes.Text = String.IsNullOrEmpty(gn.pagDetail) ? "" : "<div class='newGroupDes'>" + gn.pagDetail + "</div>";
            var listpage = db.tbPages.Where(u => u.pagActive == 1 && u.paglevel.Substring(0, gn.paglevel.Length) == gn.paglevel).Select(u => u.pagId).ToArray();
            string _str = "";

            IEnumerable<tbNew> news;
            var newall = db.tbNews.Where(u => u.newActive == 0 && u.newLang == lang && listpage.Contains(u.grnID.Value)).OrderByDescending(u => u.newid).ToList();
            news = newall.Skip(currentPage * pageSize).Take(pageSize).ToList();
            recordCount = newall.Count;
            if (news.Count() > 0)
            {
                _str += "<div class=\"box-body homepage clearfix\">";
                string col_tem1 = "", col_tem2 = "";
                if (GlobalClass.viewNews10 == "3")
                {
                    col_tem1 = " twocol1";
                    col_tem2 = " twocol2";
                }
                int i = 0;
                if (GlobalClass.viewNews10 != "1")
                {
                    i = 0;
                    foreach (tbNew n in news)
                    {
                        string alt = n.newAlt != "" && n.newAlt != null ? n.newAlt : n.newName;
                        string link = "/" + n.newTagName + ".html";
                        string image = n.newImage != null && n.newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, n.newName, n.newImage, alt) : "";
                        if (i == 0)
                            _str += String.Format("<div class=\"cat-news" + col_tem1 + " clearfix\">{2}<h3><a href=\"{0}\" title=\"{1}\">{1}</a> </h3><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.newName, image, n.newContent.ToString());
                        else
                        {
                            if (i == 1)
                                _str += "<div class=\"cat-morenews" + col_tem2 + " clearfix\"><ul>";
                            _str += String.Format("<li><a href=\"" + link + "\" title=\"" + n.newName + "\">" + n.newName + "</a> </li>");
                            if (i == news.Count() - 1)
                                _str += "</ul></div>";
                        }
                        i++;
                    }
                }
                else
                {
                    foreach (tbNew n in news)
                    {
                        string alt = n.newAlt != "" && n.newAlt != null ? n.newAlt : n.newName;
                        string link = "/" + n.newTagName + ".html";
                        string image = n.newImage != null && n.newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, n.newName, n.newImage, alt) : "";
                        _str += String.Format("<div class=\"cat-news clearfix\">{2}<h4><a href=\"{0}\" title=\"{1}\">{1}</a> </h4><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.newName, image, n.newContent.ToString());
                        i++;
                    }
                }
                _str += "<div class=\"clear\"></div>";
                _str += "</div>";
            }
            ltrPaging.Text = common.PopulatePager(recordCount, (currentPage + 1), pageSize);
            ltrlist.Text = _str;
        }

        private void BindDataGroup()
        {
            List<tbNewsDATA> list = new List<tbNewsDATA>();
            if (Request["hp"] != null)
            {
                List<tbNewsDATA> lst = tbNewsDB.tbNews_GetByAll(lang);
                lst = lst.Where(u => u.newActive == "0" && u.grnID == idcha).OrderByDescending(u => u.newDate).ToList();
                recordCount = lst.Count();
                list = tbNewsDB.tbNews_Paging((currentPage).ToString(), pageSize.ToString(), "*", "newActive = '0' and newLang = '" + lang + "' and grnID ='" + idcha + "'", "newDate desc,newId desc");
            }
            else
            {
                List<tbNewsDATA> lst2 = tbNewsDB.tbNews_GetByAll(lang);
                lst2 = lst2.Where(u => u.newActive == "0").OrderByDescending(u => u.newDate).ToList();

                recordCount = lst2.Count;
                list = tbNewsDB.tbNews_Paging((currentPage).ToString(), pageSize.ToString(), "*", "newActive = '0' and newLang = '" + lang + "'", "newDate desc,newId desc");

            }
            string Chuoi = "";
            Chuoi += "<div class=\"box-body clearfix\">";
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    tbNew dbNew = db.tbNews.FirstOrDefault(s => s.newid == Int32.Parse(list[i].newid));
                    var tbgrn = db.tbPages.Where(g => g.pagId == int.Parse(list[i].grnID)).First();                    
                    string alt = dbNew.newAlt != "" && dbNew.newAlt != null ? dbNew.newAlt : dbNew.newName;
                    string link = "/" + list[i].newTagName + ".html";
                    string image = list[i].newImage != null && list[i].newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, list[i].newName, list[i].newImage, alt) : "";
                    Chuoi += String.Format("<div class=\"cat-news clearfix\">{2}<h3><a href=\"{0}\" title=\"{1}\">{1}</a> </h3><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\" rel=\"nofollow\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, list[i].newName, image, list[i].newContent.ToString());
                }
                Chuoi += "</div>";
            }
            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
            ltrlist.Text = Chuoi;
        }

        protected void BindAdv(string newId)
        {
            string strResult = "";
            var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(newId)).ToList();
            if (listAdv.Count() > 0)
            {
                var arrId = listAdv.Select(s => s.advertisID).ToArray();
                var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                dsAdv = dsAdv.OrderByDescending(x => x.advId).ToList();
                strResult += "<div class='advByGroup' id='advNew'>";
                //for (int i = 0; i < dsAdv.Count(); i++)
                //{                    
                    strResult += "<div class='advItem'>";
                    strResult += "<a class='advItemLink' href='" + dsAdv[0].advLink + "' title='" + dsAdv[0].advName + "' target='" + dsAdv[0].advTarget + "'><img class='advItemImg' src='" + dsAdv[0].advImage + "' alt='" + dsAdv[0].advName + "' /></a>";
                    strResult += "</div>";
                //}                
                strResult += "</div>";
            }
            ltrAdv.Text = strResult;
        }
    }
}