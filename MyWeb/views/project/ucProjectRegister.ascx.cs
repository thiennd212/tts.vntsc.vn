﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.pages
{
    public partial class ucProjectRegister : System.Web.UI.UserControl
    {
        string lang = "vi"; 
        dataAccessDataContext db= new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            BindProject();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
                   }

        protected void BindProject() {
            List<tbProject> list = db.tbProjects.Where(x => x.proLang == lang && x.proActive == 1).OrderByDescending(x => x.proDate).ToList();
            
            ddlProject.Items.Clear();
            ddlProject.Items.Add(new ListItem("- Dự án -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                ddlProject.Items.Add(new ListItem(list[i].proName, list[i].proId.ToString()));
            }
            list.Clear();
            list = null;
        }
    }
}