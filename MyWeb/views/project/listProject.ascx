﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listProject.ascx.cs" Inherits="MyWeb.views.project.listProject" %>
<div id="listProjectByCat">
    <asp:Literal runat="server" ID="ltrHeader"></asp:Literal>
    <div class="listProjectByCat_content">
        <asp:Literal runat="server" ID="ltrContent"></asp:Literal>    
    </div>    
    <asp:Literal runat="server" ID="ltrPaging"></asp:Literal>
</div>