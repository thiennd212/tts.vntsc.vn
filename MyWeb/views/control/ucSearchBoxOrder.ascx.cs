﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Globalization;
using System.Data;

namespace MyWeb.views.control
{
    public partial class ucSearchBoxOrder : System.Web.UI.UserControl
    {
        private string inputValue = MyWeb.Global.GetLangKey("search_text");
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {            
            this.Page.Form.DefaultButton = btnSearch.UniqueID;
            pnList.Visible = false;
            pnDetail.Visible = false;
            pnBox.Visible = true;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSoDienThoai.Text))
            {
                BindData(txtMaDon.Text, txtSoDienThoai.Text);
                pnList.Visible = true;
                pnDetail.Visible = false;
                pnBox.Visible = true;
                ScriptManager.GetCurrent(this.Page).SetFocus(this.txtFocus);              
            }
            else
            {
                BindDataDetail(txtMaDon.Text);
                pnList.Visible = false;
                pnDetail.Visible = true;
                pnBox.Visible = false;
                pnDetail.Focus();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(ConfigurationManager.AppSettings["url-web-base"]);
        }

        protected void btnCancelX_Click(object sender, EventArgs e)
        {
            txtSoDienThoai.Text = "";
            txtMaDon.Text = "";
            pnList.Visible = false;
            pnDetail.Visible = false;
            pnBox.Visible = true;
            LoadMetaConfig("Tra cứu đơn hàng", "", "", "");
        }

        public void BindDataDetail(string maDonHang)
        {
            LoadMetaConfig("Tra cứu đơn hàng", "", "", "");
            var orCheck = db.tbCustomers.FirstOrDefault(x => x.OrderCode == maDonHang);
            if (orCheck != null)
            {
                ltrNgay.Text = string.Format("{0:dd/MM/yyyy}", orCheck.CreateDateCus);
                if (orCheck.status == 10)
                    ltrTrangThai.Text = "Đã giao hàng thành công";
                else if (orCheck.status == 5)
                    ltrTrangThai.Text = "Đang xử lý";
                else
                    ltrTrangThai.Text = "Mới gửi";

                ltrMaDonHang.Text = maDonHang;
                ltrThongTinKhac.Text = orCheck.Content;
                ltrHinhThuc.Text = orCheck.Payment;
                ltrThanhTien.Text = NumberFormat(orCheck.totalmoney.ToString()) + " VNĐ";
                ltrTong.Text = NumberFormat(orCheck.totalmoney.ToString()) + " VNĐ";
                ltrTenNguoiNhan.Text = orCheck.name;
                ltrSoDienThoai.Text = orCheck.phone;
                ltrDiaChi.Text = orCheck.Address;
                ltrEmail.Text = orCheck.mail;

                List<tbShopingcartDATA> listcart = tbShopingcartDB.tbShopingcart_GetBycus_id(orCheck.id.ToString());
                rptProList.DataSource = listcart;
                rptProList.DataBind();
                for (int i = 0; i < listcart.Count; i++)
                {
                    Label lblorder = (Label)rptProList.Items[i].FindControl("lblorder");
                    int k = (i + 1);
                    lblorder.Text = k.ToString();
                }
                listcart.Clear();
                listcart = null;
            }
        }

        static string display_so(string so)
        {
            string pr = "";
            if (so.Length > 5)
            {
                string re = "";
                if (so.IndexOf(".") > -1)
                {
                    string[] s = so.Split(Convert.ToChar("."));
                    re = s[0];
                    if (s[1].Length > 2)
                    {
                        re += "." + s[1].Substring(0, 2);
                    }
                    else
                    {
                        re += "." + s[1];
                    }
                }
                else
                {
                    return so;
                }

                return re;
            }
            else
            {
                return so;
            }
            return so;
        }

        public static string NumberFormat(string so)
        {
            Double giatri = 0;
            if (so.ToString().Trim().Length > 0)
            {
                giatri = Double.Parse(so.ToString());
                so = giatri.ToString();
                string dp = display_so(so);
                string p1 = "", p2 = "";
                if (dp.IndexOf(".") > -1)
                {
                    string[] s = dp.Split(Convert.ToChar("."));
                    p1 = s[0];
                    p2 = s[1];
                }
                else
                {
                    p1 = dp;
                }
                if (p1.Length > 3)
                {
                    dp = "";
                    int count = 0;
                    for (int i = p1.Length - 1; i > -1; i--)
                    {
                        count++;
                        dp = p1[i].ToString() + dp;
                        if (i < p1.Length - 1 && count % 3 == 0 && i > 0)
                        {
                            dp = "." + dp;
                        }


                    }
                    if (p2.Length > 0)
                        dp = dp + "." + p2;
                }

                return dp;
            }
            else
            {
                return "";
            }
        }

        public void BindData(string maDonHang, string soDienThoai)
        {
            LoadMetaConfig("Tra cứu đơn hàng", "", "", "");
            var _listForder = db.tbOrders.Where(x => x.Lang == lang).OrderByDescending(s => s.NgayThang).ThenBy(s => s.Code).ToList();

            if (!string.IsNullOrEmpty(maDonHang))
            {
                _listForder = _listForder.Where(s => s.Code.ToLower().Contains(maDonHang.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(soDienThoai))
            {
                List<string> lstCode = db.tbCustomers.Where(x => x.phone.ToLower().Trim() == soDienThoai.ToLower().Trim()).Select(x => x.OrderCode).ToList();
                _listForder = _listForder.Where(s => lstCode.Contains(s.Code)).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                //ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page
        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string orderCode = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Edit":
                    {
                        pnList.Visible = false;
                        pnDetail.Visible = true;
                        pnBox.Visible = false;
                        BindDataDetail(orderCode);                        
                    }
                    break;
            }
        }
        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData(txtMaDon.Text, txtSoDienThoai.Text);
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData(txtMaDon.Text, txtSoDienThoai.Text);
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }
        public string ConvertDateToString(object ngaythang)
        {
            DateTime nt = Convert.ToDateTime(ngaythang);
            return nt.ToString("HH:mm MM/dd/yyyy",
                                CultureInfo.InvariantCulture);
        }
        protected string GetStatusTT(string status)
        {
            string result = "";
            if (status == "1")
            {
                result = "Chưa thanh toán";
            }
            else if (status == "2")
            {
                result = "Đã thanh toán";
            }

            return result;
        }

        protected string BindImages(string proId)
        {
            string str = "";
            try
            {
                var pro = db.tbProducts.FirstOrDefault(x => x.proId == int.Parse(proId));
                string strPart = pro.proImage;
                str = "<img style=\"width:50px;\" src=\"" + strPart.Split(',')[0].Replace("uploads", "uploads/_thumbs") + "\" />";
            }
            catch (Exception)
            { }
            return str;
        }

        protected string GetStatus(string status)
        {
            string result = "";
            if (status == "0")
            {
                result = "Chưa chuyển";
            }
            else if (status == "1")
            {
                result = "Đang chuyển";
            }
            else if (status == "2")
            {
                result = "Đã chuyển";
            }

            return result;
        }
        #endregion        

        void LoadMetaConfig(string strName, string strTitle, string strDescription, string strKeyword)
        {
            if (GlobalClass.showSEO.Equals("True"))
            {
                if (strDescription != "")
                {
                    strDescription = strDescription + " | " + GlobalClass.conDescription.ToString();
                }
                else
                {
                    strDescription = strTitle != "" ? strTitle : strName;
                    strDescription = strDescription + " | " + GlobalClass.conDescription.ToString();
                }
                if (strKeyword != "")
                {
                    strKeyword = strKeyword + " | " + GlobalClass.conKeywords.ToString();
                }
                else
                {
                    strKeyword = strTitle != "" ? strTitle : strName;
                    strKeyword = strKeyword + " | " + GlobalClass.conKeywords.ToString();
                }
                strTitle = strTitle != "" ? strTitle : strName;
            }
            Page.Title = strTitle != "" ? strTitle : strName;
        }
    }
}