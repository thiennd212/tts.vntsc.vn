﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucSlides : System.Web.UI.UserControl
    {
        public bool showSlide = true;
        string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string str = "";
            List<tbAdvertiseDATA> list = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("2");
            list = list.Where(s => s.advLang.Trim().ToLower() == lang).OrderByDescending(u => u.advOrd).ToList();
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    str += "<div class=\"item\"><a title=\"" + list[i].advName + "\" href=\"" + list[i].advLink + "\" target=\"" + list[i].advTarget + "\"><img src=\"" + list[i].advImage + "\" width=\"" + list[i].advWidth + "\" height=\"" + list[i].advHeight + "\" alt=\"" + list[i].advName + "\"  /></a><span class='advItemContent'>" + list[i].advContent + "</span></div>";
                }
            }
            else
            {
                showSlide = false;
            }
            list = null;
            ltrSlides.Text = str;
        }
    }
}