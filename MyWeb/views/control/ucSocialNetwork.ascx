﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSocialNetwork.ascx.cs" Inherits="MyWeb.views.control.ucSocialNetwork" %>

<div class="box-social-network">
    <div class="body-network">
        <div class="header"><%= MyWeb.Global.GetLangKey("connect_with_us")%></div>
        <ul class="body-social">
            <%if (GlobalClass.viewSocial1 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial1 %>' class="facebook" rel="nofollow"><span>Facebook</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial2 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial2 %>' class="tweet" rel="nofollow"><span>Tweet</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial3 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial3 %>' class="youtube" rel="nofollow"><span>Youtube</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial4 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial4 %>' class="zing" rel="nofollow"><span>Zing Me</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial5 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial5 %>' class="googleplus" rel="nofollow"><span>Google+</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial6 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial6 %>' class="rss" rel="nofollow"><span>RSS</span></a></li>
            <%} %>
        </ul>
    </div>
</div>
