﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucCustomerReply : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = true;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string _str = "";
            List<tbReply> list = db.tbReplies.Where(x => x.cusActive == 1).ToList();
            if (list.Count > 0)
            {
                _str += "<div class=\"custumer-say\">";
                for (int i = 0; i < list.Count; i++)
                {
                    string path = "";
                    path = list[i].cusImage;
                    _str += "<div class=\"cus\">";
                    if (path != "")
                    {
                        _str += "<div class=\"cus-1\">";
                        _str += "<a href=\"#\"><img src=\"" + path + "\" alt=\"\"/></a>";
                        _str += "</div>";
                    }
                    _str += "<div class=\"cus-2\">";
                    _str += "<div class=\"cus-write\">“" + list[i].NoiDung + "“</div>";
                    _str += "<div class=\"cus-name\">" + list[i].HoTen + " - " + list[i].ChucDanh + "</div>";
                    _str += "</div>";
                    _str += "</div>";
                }
                _str += "</div>";
            }
            ltradv.Text = _str;
            list.Clear();
            list = null;
        }
    }
}