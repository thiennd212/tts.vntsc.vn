﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSlidesFooter.ascx.cs" Inherits="MyWeb.views.control.ucSlidesFooter" %>

<%if(showSlide){%>
<div class="slides">
    <div id="owl-footer" class="owl-carousel">
        <asp:Literal ID="ltrSlides" runat="server"></asp:Literal>
    </div>
</div>
<!--end slide-->
<%} %>