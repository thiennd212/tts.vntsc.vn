﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucChangePassword.ascx.cs" Inherits="MyWeb.views.control.ucChangePassword" %>
<div class="box-change-password">
    <div class="cate-header">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("user_change_pasword") %></div>
    </div>
    <div class="box-body-sub">
        <div class="row">
            <div class="form-group">
                <span class="view-messenger">
                    <asp:Literal ID="lblthongbao" runat="server"></asp:Literal></span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_old_password")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpassc" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtpassc" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_new_password")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpass" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpass" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_re_enter_new_password")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtpass_confirm" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpass_confirm" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvdpasrewrite" runat="server" ErrorMessage="!" onservervalidate="cvdpasrewrite_ServerValidate" ControlToValidate="txtpass_confirm"></asp:CustomValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-7">
                    <asp:LinkButton ID="btnSend" runat="server" OnClick="btnSend_Click"><%= MyWeb.Global.GetLangKey("contact_send") %></asp:LinkButton>
                    <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click"><%= MyWeb.Global.GetLangKey("contact_cancel")%></asp:LinkButton>
                </div>
            </div>


        </div>
    </div>
</div>
