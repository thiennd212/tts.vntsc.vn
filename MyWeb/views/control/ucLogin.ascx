﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucLogin.ascx.cs" Inherits="MyWeb.views.control.ucLogin" %>

<div class="box-login">
    <div class="header"><%= MyWeb.Global.GetLangKey("user_login")%></div>
    <asp:Panel ID="pnlLogin" runat="server">
        <div class="body">
            <div class="list-err">
                <asp:Literal ID="ltrMess" runat="server"></asp:Literal>
            </div>
            <%if (Session["um"] != null)
              { %>
            <div class="lis-group">
                <div class="col-ts-left"><%= MyWeb.Global.GetLangKey("user_welcome")%>:</div>
                <div class="col-ts-right"><%=Session["um"] %></div>
            </div>
            <div class="lis-group">
                <a href="/thong-tin-ca-nhan.html"><%= MyWeb.Global.GetLangKey("user_profile")%></a>
            </div>
            <div class="lis-group">
                <a href="/doi-mat-khau.html"><%= MyWeb.Global.GetLangKey("user_change_pasword")%></a>
            </div>
            <div class="lis-group">
                <a href="/lich-su-mua-hang.html"><%= MyWeb.Global.GetLangKey("user_history")%></a>
            </div>
            <div class="lis-group">
                <asp:LinkButton ID="btnLogout" runat="server" ToolTip="" OnClick="btnLogout_Click"><%= MyWeb.Global.GetLangKey("user_logout")%></asp:LinkButton>
            </div>
            <%}
              else
              { %>

            <div class="lis-group">
                <div class="col-ts-left"><%= MyWeb.Global.GetLangKey("user_username")%>:</div>
                <div class="col-ts-right">
                    <asp:TextBox ID="txtUser" runat="server" Text=""></asp:TextBox></div>
            </div>

            <div class="lis-group">
                <div class="col-ts-left"><%= MyWeb.Global.GetLangKey("user_password")%>:</div>
                <div class="col-ts-right">
                    <asp:TextBox ID="txtPass" runat="server" TextMode="Password" Text=""></asp:TextBox></div>
            </div>
            <div class="lis-group">
                <div class="col-ts-left"></div>
                <div class="col-ts-right">
                    <asp:LinkButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" class="btnLogin"><%=MyWeb.Global.GetLangKey("user_login") %></asp:LinkButton>
                </div>
            </div>

            <%} %>
        </div>
    </asp:Panel>
</div>
