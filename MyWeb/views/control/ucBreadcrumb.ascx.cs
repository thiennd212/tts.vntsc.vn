﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucBreadcrumb : System.Web.UI.UserControl
    {
        string lang = "vi";
        string strRequest = "";
        string strCurr = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            strRequest = Request.Path;
            string tagMan = "";
            if (!strRequest.Contains("error404"))
            {
                if (!string.IsNullOrEmpty(Request["hp"]))
                {
                    if (Request["hp"].ToString().Contains("/"))
                    {
                        string[] arrRes = Request["hp"].ToString().Split('/');
                        strRequest = arrRes[0];
                        tagMan = arrRes[1];
                    }
                    else
                        strRequest = Request["hp"].ToString();
                }

                try
                {
                    var curType = db.tbPages.Where(s => s.pagTagName == strRequest).ToList();
                    var curType2 = db.tbPages.Where(s => s.pagLink == curType[0].pagTagName).FirstOrDefault();

                    if (curType.Count > 0 || curType2 != null)
                    {
                        strCurr = curType != null ? curType[0].pagType.ToString() : curType2.pagType.ToString();
                    }
                    else { strCurr = strRequest; }
                }
                catch
                {
                }

                lang = MyWeb.Global.GetLang();
                if (!IsPostBack)
                {
                    try
                    {
                        BindBreadcum();
                    }
                    catch { }
                }
            }
        }

        private void BindBreadcum() {

            string strReturn = "";
            strReturn += "";

            switch (strCurr)
            {
                case "5":
                    strReturn += GroupNews();
                    break;
                case "200":
                    strReturn += GroupNews();
                    break;
                case "800":
                    strReturn += News();
                    break;
                case "6":
                    strReturn += Category();
                    break;
                case "100":
                    strReturn += Category();
                    break;
                case "900":
                    strReturn += Product();
                    break;
                case "1000":
                    strReturn += CategoryProject();
                    break;
                case "1001":
                    strReturn += Project();
                    break;
                case "2":
                    strReturn += Pag();
                    break;
                case "9":
                    strReturn += GrLibrary();
                    break;
                case "300":
                    strReturn += GrLibrary();
                    break;
                default:
                    strReturn +=Other();
                     break;

            }
            ltrBreadcumb.Text = strReturn;

            if (Request["e"] != null)
            {
                if (Request["e"].ToString() == "listnew")
                {
                    ltrBreadcumb.Text = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("news") + " </a></li>";
                }
                if (Request["e"].ToString() == "listpro")
                {
                    ltrBreadcumb.Text = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("products") + " </a></li>";
                }
                if (Request["e"].ToString() == "searchOrder")
                {
                    ltrBreadcumb.Text = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\">Tra cứu đơn hàng</a></li>";
                }
            }

            
        }

        private string GroupNews()
        {
            string str = "";
            var glst = db.tbPages.Where(u => u.pagTagName == Request["hp"].ToString() && u.pagType == 200 && u.pagLang == lang).ToList();
            if (glst.Count > 0)
            {
                string link = "/" + glst[0].pagTagName + ".html";
                str += "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\">" + glst[0].pagName + "</a></li>";
            }
            return str;
        }

        private string News()
        {
            string str = "";
            List<tbNewsDATA> listnew = tbNewsDB.sp_tbNews_GetByTag(Request["hp"].ToString(), lang);
            if (listnew.Count > 0)
            {
                tbPage gn = db.tbPages.Where(s => s.pagId == int.Parse(listnew[0].grnID) && s.pagType == 200).FirstOrDefault();
                str += "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"" + gn.pagTagName + ".html\">" + gn.pagName + "</a><span>»</span></li><li><a href=\"#\"  class=\"active\">" + listnew[0].newName + "</a></li>";
            }
            return str;
        }

        private string Category()
        {
            string str = "";
            string str1 = "";
            List<tbPageDATA> glst = tbPageDB.tbPage_GetByTagName(strRequest).ToList();
            if (glst.Count > 0)
            {
                if (glst[0].paglevel.Length == 5)
                {
                    str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>" + str1 + "<li><a href=\"#\" class=\"active\">" + glst[0].pagName + "</a></li>");
                }
                else
                {
                    glst = glst.Where(s => s.pagType == "100").ToList();
                    if (glst.Count > 0)
                    {
                        int n = glst[0].paglevel.Length / 5;
                        for (int i = 0; i < n - 1; i++)
                        {
                            int l = (i + 1) * 5;
                            List<tbPage> glist = db.tbPages.Where(s => s.paglevel == glst[0].paglevel.Substring(0, l)).ToList();
                            if (glist.Count > 0)
                            {
                                str1 += string.Format("<li><a href=\"" + glist[0].pagTagName + ".html\">" + glist[0].pagName + "</a><span>»</span></li>");
                            }
                        }
                        str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>" + str1 + "<li><a href=\"#\" class=\"active\">" + glst[0].pagName + "</a></li>");
                    }
                }
            }
            return str;
        }

        private string CategoryProject()
        {
            string str = "";
            string str1 = "";
            List<tbPageDATA> glst = tbPageDB.tbPage_GetByTagName(strRequest).ToList();
            if (glst.Count > 0)
            {
                if (glst[0].paglevel.Length == 5)
                {
                    str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>" + str1 + "<li><a href=\"#\" class=\"active\">" + glst[0].pagName + "</a></li>");
                }
                else
                {
                    glst = glst.Where(s => s.pagType == "100").ToList();
                    if (glst.Count > 0)
                    {
                        int n = glst[0].paglevel.Length / 5;
                        for (int i = 0; i < n - 1; i++)
                        {
                            int l = (i + 1) * 5;
                            List<tbPage> glist = db.tbPages.Where(s => s.paglevel == glst[0].paglevel.Substring(0, l)).ToList();
                            if (glist.Count > 0)
                            {
                                str1 += string.Format("<li><a href=\"" + glist[0].pagTagName + ".html\">" + glist[0].pagName + "</a><span>»</span></li>");
                            }
                        }
                        str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>" + str1 + "<li><a href=\"#\" class=\"active\">" + glst[0].pagName + "</a></li>");
                    }
                }
            }
            return str;
        }

        private string Product()
        {
            string str2 = "";
            string str = "";
            try
            {
                var product = db.tbProducts.Where(s => s.proTagName.ToLower() == strRequest.ToLower()).FirstOrDefault();
                List<tbPageDATA> glst = tbPageDB.tbPage_GetByID(product.catId);
                var data = db.tbPages.Where(u => u.paglevel == glst[0].paglevel.Substring(0, glst[0].paglevel.Length - 5)).ToList();
                if (data.Count > 0)
                {
                    if (data[0].paglevel.Length > 5)
                    {
                        var dt = db.tbPages.Where(u => u.paglevel == glst[0].paglevel.Substring(0, 5) && u.paglevel != glst[0].paglevel).ToList();
                        if (dt.Count > 0)
                        {
                            str2 += string.Format("<li><a href=\"" + dt[0].pagTagName + ".html\">" + dt[0].pagName + "</a><span>»</span></li>");

                        }
                    }
                }
                if (data.Count > 0)
                {
                    str2 += string.Format("<li><a href=\"" + data[0].pagTagName + ".html\">" + data[0].pagName + "</a><span>»</span></li>");
                }
                str += "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>  " + str2 + "<li><a href=\"" + glst[0].pagTagName + ".html\">" + glst[0].pagName + "</a><span>»</span></li><li><a href=\"#\" class=\"active\">" + product.proName + "</a></li>";

            }
            catch { }
            return str;
        }

        private string Project()
        {
            string str2 = "";
            string str = "";
            try
            {
                var product = db.tbProjects.Where(s => s.proTagName.ToLower() == strRequest.ToLower()).FirstOrDefault();
                List<tbPageDATA> glst = tbPageDB.tbPage_GetByID(product.catId);
                var data = db.tbPages.Where(u => u.paglevel == glst[0].paglevel.Substring(0, glst[0].paglevel.Length - 5)).ToList();
                if (data.Count > 0)
                {
                    if (data[0].paglevel.Length > 5)
                    {
                        var dt = db.tbPages.Where(u => u.paglevel == glst[0].paglevel.Substring(0, 5) && u.paglevel != glst[0].paglevel).ToList();
                        if (dt.Count > 0)
                        {
                            str2 += string.Format("<li><a href=\"" + dt[0].pagTagName + ".html\">" + dt[0].pagName + "</a><span>»</span></li>");

                        }
                    }
                }
                if (data.Count > 0)
                {
                    str2 += string.Format("<li><a href=\"" + data[0].pagTagName + ".html\">" + data[0].pagName + "</a><span>»</span></li>");
                }
                str += "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>  " + str2 + "<li><a href=\"" + glst[0].pagTagName + ".html\">" + glst[0].pagName + "</a><span>»</span></li><li><a href=\"#\" class=\"active\">" + product.proName + "</a></li>";

            }
            catch { }
            return str;
        }

        private string Pag()
        {
            string str = "";
            var glst = db.tbPages.Where(u => u.pagTagName == Request["hp"].ToString() && u.pagType == 2 && u.pagLang == lang).ToList();
            if (glst.Count > 0)
            {
                string str2 = "";
                if (glst[0].paglevel.Length > 5)
                {
                    var gl = db.tbPages.Where(s => s.paglevel == glst[0].paglevel.Substring(0, glst[0].paglevel.Length - 5)).OrderBy(s => s.paglevel).ToList();                    
                    foreach (var item in gl)
                    {
                        str2 += string.Format("<li><a href=\"" + item.pagTagName + ".html\">" +item.pagName + "</a><span>»</span></li>");
                    }
                }
                str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>" + str2 + "<li><a href=\"#\" class=\"active\"> " + glst[0].pagName + " </a></li>");
            }
            return str;
        }

        private string GrLibrary()
        {
            string str = "";
            var glst = db.tbPages.Where(u => u.pagTagName == Request["hp"].ToString() && u.pagType == 300 && u.pagLang == lang).ToList();
            if (glst.Count > 0)
            {
                if (glst[0].paglevel.Length > 5)
                {
                    var gl = db.tbPages.Where(s => s.paglevel == glst[0].paglevel.Substring(0, glst[0].paglevel.Length - 5)).OrderBy(s => s.paglevel).ToList();
                    foreach (var item in gl)
                    {
                        str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"" + item.pagTagName + ".html\">" + item.pagName + "</a><span>»</span></li>");

                    }
                }
                else
                {
                    str += string.Format("<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li>");
                }
                str += string.Format("<li><a href=\"#\" class=\"active\"> " + glst[0].pagName + " </a></li>");
            }
            return str;

        }

        private string Other()
        {
            string str = "";
            string url = Request.ServerVariables["URL"].ToString().ToLower();
            if (url.Contains("lien-he")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("contact") + " </a></li>";
            else if (url.Contains("page-404")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("404_page") + " </a></li>";
            else if (url.Contains("thanh-toan")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("payment") + " </a></li>";
            else if (url.Contains("gio-hang")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("order") + " </a></li>";
            else if (url.Contains("tin-tuc")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("news") + " </a></li>";
            else if (url.Contains("forgot-password")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("forgot_title") + " </a></li>";
            else if (url.Contains("san-pham")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("products") + " </a></li>";
            else if (url.Contains("thu-vien")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("library") + " </a></li>";
            else if (url.Contains("san-pham-moi")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("new_products") + " </a></li>";
            else if (url.Contains("-tag")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("Tag") + " </a></li>";
            else if (url.Contains("tim-kiem")) str = "<li><a href=\"/\">" + MyWeb.Global.GetLangKey("home") + "</a><span>»</span></li><li><a href=\"#\" class=\"active\"> " + MyWeb.Global.GetLangKey("search_result") + " </a></li>";
            else
                str = "";
            return str;
        }
    }
}