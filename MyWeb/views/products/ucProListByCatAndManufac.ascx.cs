﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProListByCatAndManufac : System.Web.UI.UserControl
    {
        string lang = "vi";
        int currentPage = 1;
        int pageSize = 16;
        int recordCount = 0;
        private string strCatID = "";
        private string strManID = "";
        private string strNameCat = "";
        private string catTag = "";
        private string manTag = "";
        private string strNumberView = "";
        string strView_cate_products = "False";
        string viewBy = GlobalClass.viewProducts9;
        string sortBy = GlobalClass.viewProducts10;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
                pageSize = int.Parse(GlobalClass.viewProducts1);
                if (Request["hp"] != null) 
                {
                    catTag = Request["hp"].ToString();
                    if (Request["hp"].ToString().Contains("/"))
                    {
                        string[] arrRes = Request["hp"].ToString().Split('/');
                        catTag = arrRes[0];
                        manTag = arrRes[1];
                        strManID = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GM) && s.pagTagName == manTag).FirstOrDefault().pagId.ToString();
                    }
                    strCatID = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagTagName == catTag).FirstOrDefault().pagId.ToString();
                }
                string iCat = strCatID;
                strView_cate_products = Common.GlobalClass.GetAppSettingKey("using_www") != null ? "True" : "False";
            }
            catch { strView_cate_products = "False"; }

            strNumberView = GlobalClass.viewProducts1;
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                if (strView_cate_products == "True") { BindDataCate(); }
                else
                {
                    if (strCatID != "") { BindData(); }
                    else { BindDataAll(); }
                }

            }
        }

        private void BindDataCate()
        {
            strNameCat = MyWeb.Global.GetLangKey("products");
            pnlDrop.Visible = false;
            string _str = "", strContainer = "";
            string strLBId = "0", strLBName = "", strLBTagName = "#";
            //if (Request["hp"] != null && Request["hp"].ToString() != "")
                //strCatID = Request["hp"].ToString().Split('?')[0];
            List<tbPageDATA> objCategory = tbPageDB.tbPage_GetByAll(lang);
            if (strCatID != "")
            {
                tbPage objSelected = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(strCatID) && s.pagType == int.Parse(pageType.GP));

                if (objSelected != null)
                {
                    ltrListCatChild.Text = common.BindDataCatChild(objSelected.paglevel.ToString());
                    ltrGroupDes.Text = objSelected.pagDetail;
                    try
                    {
                        strContainer = objSelected.check5;
                    }
                    catch { }

                    ltrName.Text = objSelected.pagName;
                    strLBName = objSelected.pagName;
                    strLBId = objSelected.pagId.ToString();
                    strLBTagName = "/" + objSelected.pagTagName + ".html";
                    objCategory = objCategory.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.StartsWith(objSelected.paglevel) && s.paglevel.Length >= objSelected.paglevel.Length).ToList();
                }
            }
            else
            {
                ltrName.Text = strNameCat;
                strLBName = strNameCat;
                objCategory = objCategory.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.Length == 5).ToList();
            }

            if (objCategory.Count() > 0)
            {
                var lstCatId = objCategory.Select(s => s.pagId).ToArray();
                var _product = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang && lstCatId.Contains(s.catId)).ToList();
                if (!string.IsNullOrEmpty(strManID))
                    _product = _product.Where(x => x.manufacturerId == int.Parse(strManID)).ToList();
                if (sortBy == "name")
                    _product = _product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                else if (sortBy == "namedesc")
                    _product = _product.OrderByDescending(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                else if (sortBy == "price")
                    _product = _product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                else if (sortBy == "pricedesc")
                    _product = _product.OrderByDescending(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                else
                    _product = _product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();

                recordCount = _product.Count();

                int num = int.Parse(strNumberView);
                _product = _product.Skip(num * currentPage - num).Take(num).ToList();

                var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(strLBId)).ToList();
                if (listAdv.Count() > 0)
                {
                    var arrId = listAdv.Select(s => s.advertisID).ToArray();
                    var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                    dsAdv = dsAdv.OrderByDescending(x => x.advId).ToList();
                    _str += "<div class='advByGroup'>";
                    //for (int i = 0; i < dsAdv.Count(); i++)
                    //{
                    //Lấy ảnh mới nhất
                    _str += "<div class='advItem'>";
                    _str += "<a class='advItemLink' href='" + dsAdv[0].advLink + "' title='" + dsAdv[0].advName + "' target='" + dsAdv[0].advTarget + "'><img class='advItemImg' src='" + dsAdv[0].advImage + "' alt='" + dsAdv[0].advName + "' /></a>";
                    _str += "</div>";
                    //}
                    _str += "</div>";
                }
                if (_product.Count() > 0)
                {
                    _str += "<div class=\"cate-header sub-top\">";
                    _str += "<div class=\"txt-name-sub\"><h2><a href='" + strLBTagName + "' >" + strLBName + "</a></h2> <span style=\"display:none\">| " + String.Format(MyWeb.Global.GetLangKey("findproduct"), recordCount) + "</span></div>";
                    _str += "</div>";

                    _str += "<ul class=\"product-list\">";
                    _str += common.LoadProductList(_product, viewBy, strLBId);
                    _str += "</ul>";
                }

            }
            else
            {
                BindData();
            }
            if (strContainer != "")
            {
                ltrViewsContainer.Text = "<div class=\"sub-view-container\">" + strContainer + "</div>";
            }
            ltrCateView.Text = _str;
            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
        }

        private void BindData()
        {
            pnlDrop.Visible = true;
            strNameCat = MyWeb.Global.GetLangKey("products");
            string _str = "";
            string catLevel = "";
            string strContainer = "";
            viewBy = Request.RawUrl.Contains("view") ? common.GetParameterFromUrl(Request.RawUrl, "view") : viewBy;
            sortBy = Request.RawUrl.Contains("sort") ? common.GetParameterFromUrl(Request.RawUrl, "sort") : sortBy;
            try
            {
                tbPage category = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(strCatID));
                if (lang != category.pagLang.ToLower().Trim())
                {
                    Session["Lang"] = category.pagLang.Trim();
                    Response.Redirect(Request.Url.AbsolutePath);
                }
                if (category != null)
                {
                    ltrName.Text = "<a href=\"/" + MyWeb.Common.StringClass.NameToTag(category.pagTagName) + ".html\" >" + category.pagName + "</a>";
                    strNameCat = category.pagName;
                    catLevel = category.paglevel;
                }
                else
                {
                    ltrName.Text = strNameCat;
                }
                try
                {
                    strContainer = category.check5;
                }
                catch { }

            }
            catch { }
            IEnumerable<tbProduct> product = db.tbProducts.Where(s => s.proActive == 1 && s.proLang.Trim() == lang);
            if (catLevel.Length > 0)
            {
                int pageid = Convert.ToInt32(strCatID);
                string strcatlevel = db.tbPages.Where(u => u.pagId == pageid).FirstOrDefault().paglevel;
                List<tbProduct> listpro = new List<tbProduct>();
                var pagelistid = db.tbPages.Where(u => u.paglevel.Substring(0, strcatlevel.Length) == strcatlevel).ToList();
                for (int h = 0; h < pagelistid.Count; h++)
                {
                    int idpagecha = pagelistid[h].pagId;
                    var data = db.tbProducts.Where(u => u.catId == idpagecha.ToString() && u.proActive == 1).ToList();
                    for (int k = 0; k < data.Count; k++)
                    {
                        listpro.Add(data[k]);
                    }
                }
                product = listpro;
            }

            if (sortBy == "name")
                product = product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else if (sortBy == "namedesc")
                product = product.OrderByDescending(s => s.proName).ThenBy(s => s.proName).ThenByDescending(s => s.proName).ToList();
            else if (sortBy == "price")
                product = product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else if (sortBy == "pricedesc")
                product = product.OrderByDescending(s => double.Parse(s.proPrice)).ThenByDescending(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else
                product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
            recordCount = product.Count();
            product = product.Skip((pageSize * currentPage) - pageSize).Take(pageSize);
            var s11 = product.Count();

            var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(strCatID)).ToList();
            if (listAdv.Count() > 0)
            {
                var arrId = listAdv.Select(s => s.advertisID).ToArray();
                var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                _str += "<div class='advByGroup'>";
                for (int i = 0; i < dsAdv.Count(); i++)
                {
                    _str += "<div class='advItem'>";
                    _str += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                    _str += "</div>";
                }
                _str += "</div>";
            }

            if (strContainer != "")
            {
                ltrViewsContainer.Text = "<div class=\"sub-view-container\">" + strContainer + "</div>";
            }

            _str += "<ul class=\"product-list\">";
            _str += common.LoadProductList(product, viewBy);
            _str += "</ul>";
            ltrProList.Text = _str;
            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
        }

        private void BindDataAll()
        {
            string _str = "";
            string strContainer = "";
            viewBy = Request.RawUrl.Contains("view") ? common.GetParameterFromUrl(Request.RawUrl, "view") : viewBy;
            sortBy = Request.RawUrl.Contains("sort") ? common.GetParameterFromUrl(Request.RawUrl, "sort") : sortBy;
            if (strCatID != "")
            {
                tbPage category = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(strCatID));
                if (category != null)
                {
                    ltrName.Text = "<span><a href=\"/" + MyWeb.Common.StringClass.NameToTag(category.pagTagName) + ".html\" >" + category.pagName + "</a>";
                }
                try
                {
                    strContainer = category.check5;
                }
                catch { }
            }
            else
            {
                ltrName.Text = "Sản phẩm";
            }

            if (strContainer != "")
            {
                ltrViewsContainer.Text = "<div class=\"sub-view-container\">" + strContainer + "</div>";
            }

            IEnumerable<tbProduct> product = db.tbProducts.Where(s => s.proActive == 1 && s.proLang.Trim() == lang);

            if (sortBy == "name")
                product = product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else if (sortBy == "namedesc")
                product = product.OrderByDescending(s => s.proName).ThenBy(s => s.proName).ThenByDescending(s => s.proName).ToList();
            else if (sortBy == "price")
                product = product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else if (sortBy == "pricedesc")
                product = product.OrderByDescending(s => double.Parse(s.proPrice)).ThenByDescending(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
            else
                product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
            recordCount = product.Count();
            product = product.Skip((currentPage - 1) * pageSize).Take(pageSize);
            var s11 = product.Count();
            _str += "<ul class=\"product-list\">";
            _str += common.LoadProductList(product, viewBy); ;
            _str += "</ul>";
            ltrProList.Text = _str;

            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
        }

        protected void btnOderAZ_Click(object sender, EventArgs e)
        {
            string strCurLink = "";
            if (Request["hp"] != null)
                strCurLink = "/" + Request["hp"].ToString() + ".html";
            else strCurLink = "/san-pham.html";
            string currPage = strCurLink.Split('?')[0];
            string view = common.GetParameterFromUrl(strCurLink, "view");
            if (view != "")
            {
                currPage = common.AppendUrlParameter(currPage, "view", view);
            }
            string page = common.GetParameterFromUrl(strCurLink, "page");
            if (page != "")
            {
                currPage = common.AppendUrlParameter(currPage, "page", page);
            }
            Response.Redirect(common.AppendUrlParameter(currPage, "sort", "name"));
        }

        protected void btnOderZA_Click(object sender, EventArgs e)
        {
            string strCurLink = "";
            if (Request["hp"] != null)
                strCurLink = "/" + Request["hp"].ToString() + ".html";
            else strCurLink = "/san-pham.html";
            string currPage = strCurLink.Split('?')[0];
            string view = common.GetParameterFromUrl(strCurLink, "view");
            if (view != "")
            {
                currPage = common.AppendUrlParameter(currPage, "view", view);
            }
            string page = common.GetParameterFromUrl(strCurLink, "page");
            if (page != "")
            {
                currPage = common.AppendUrlParameter(currPage, "page", page);
            }
            Response.Redirect(common.AppendUrlParameter(currPage, "sort", "namedesc"));
        }

        protected void btnOderPriceAug_Click(object sender, EventArgs e)
        {
            string strCurLink = "";
            if (Request["hp"] != null)
                strCurLink = "/" + Request["hp"].ToString() + ".html";
            else strCurLink = "/san-pham.html";
            string currPage = strCurLink.Split('?')[0];
            string view = common.GetParameterFromUrl(strCurLink, "view");
            if (view != "")
            {
                currPage = common.AppendUrlParameter(currPage, "view", view);
            }
            string page = common.GetParameterFromUrl(strCurLink, "page");
            if (page != "")
            {
                currPage = common.AppendUrlParameter(currPage, "page", page);
            }
            Response.Redirect(common.AppendUrlParameter(currPage, "sort", "price"));
        }

        protected void btnOderPriceGar_Click(object sender, EventArgs e)
        {
            string strCurLink = "";
            if (Request["hp"] != null)
                strCurLink = "/" + Request["hp"].ToString() + ".html";
            else strCurLink = "/san-pham.html";
            string currPage = strCurLink.Split('?')[0];
            string view = common.GetParameterFromUrl(strCurLink, "view");
            if (view != "")
            {
                currPage = common.AppendUrlParameter(currPage, "view", view);
            }
            string page = common.GetParameterFromUrl(strCurLink, "page");
            if (page != "")
            {
                currPage = common.AppendUrlParameter(currPage, "page", page);
            }
            Response.Redirect(common.AppendUrlParameter(currPage, "sort", "pricedesc"));
        }
    }
}