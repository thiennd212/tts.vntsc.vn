﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProHomeV2 : System.Web.UI.UserControl
    {
        string lang = "vi";
        public string strNumberView = "", strClassName = "";
        int currentPage = 1;
        string viewtype = GlobalClass.viewProducts5;
        string viewBy = GlobalClass.viewProducts9;
        string sortBy = GlobalClass.viewProducts10;        
        int recordCount = 0;
        int pageSize = Convert.ToInt32(GlobalClass.viewProducts2);
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            try
            {
                currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"].ToString()) : 1;
            }
            catch { }
            if (!IsPostBack)
            {
                strClassName = Session["home_page"] != null ? "box-pro-home" : "box-pro-sub";
                strNumberView = GlobalClass.viewProducts2;
                BindData();

            }
        }
        private void BindData()
        {
            string _str = "";
            List<tbPage> lstPagCatHome = db.tbPages.Where(x => x.check1.ToString() == "1" && x.pagLang == lang && x.pagType == int.Parse(pageType.GP) && x.paglevel.Length == 5 && x.pagActive == 1).OrderBy(x => x.pagOrd).ToList();
            if (lstPagCatHome != null && lstPagCatHome.Count > 0)
            {
                foreach (var cat in lstPagCatHome)//Loop vòng cha
                {
                    string cId = cat.pagId.ToString();
                    List<string> catChilId = db.tbPages.Where(x => x.check1.ToString() == "1" && x.paglevel.StartsWith(cat.paglevel) && x.pagActive == 1 && x.pagLang == lang && x.pagType == int.Parse(pageType.GP)).OrderBy(x => x.pagOrd).Select(x => x.pagId.ToString()).ToList();
                    IEnumerable<tbProduct> product = db.tbProducts.Where(s => s.proActive == 1 && catChilId.Contains(s.catId) && s.proLang.Trim() == lang).OrderBy(s => s.proOrd).ThenByDescending(s => s.proDate).Skip(0).Take(pageSize);
                    
                    _str += "<div class=\"box-cat-home\">";
                        _str += "<div class=\"cate-header sub-top\">";
                            _str += "<div class=\"txt-name-sub\"><h2><a href=\"/" + MyWeb.Common.StringClass.NameToTag(cat.pagTagName) + ".html\" title=\"" + cat.pagName + "\">" + cat.pagName + "</a></h2>";
                            var catChild = db.tbPages.Where(s => s.check1.ToString() == "1" && s.pagType == int.Parse(pageType.GP) && s.pagActive == 1 && s.pagLang.Trim() == lang && s.paglevel.StartsWith(cat.paglevel) && s.paglevel.Length == (cat.paglevel.Length * 2)).OrderBy(x => x.pagOrd).ToList();
                            if (catChild != null && catChild.Count > 0)
                            {
                                _str += "<div class=\"wrap-child-cat\">";
                                int i = 0;
                                foreach (var item in catChild)
                                {
                                    if (i == 0)
                                        _str += "<span> <h3><a href=\"/" + MyWeb.Common.StringClass.NameToTag(item.pagTagName) + ".html\" title=\"" + item.pagName + "\">" + item.pagName + "</a></h3></span>";
                                    else
                                        _str += "<span> | <h3><a href=\"/" + MyWeb.Common.StringClass.NameToTag(item.pagTagName) + ".html\" title=\"" + item.pagName + "\">" + item.pagName + "</a></h3></span>";
                                    i++;
                                }
                                _str += "</div>";
                            }
                            _str += "</div>";
                        _str += "</div>";
                        if (!string.IsNullOrEmpty(cat.pagImage))
                        {
                            _str += "<div class=\"col-md-3 padding-left-none\">";
                            _str += "<img class=\"img-cat-home\" src=\"" + cat.pagImage + "\"/>";
                            _str += "<div class=\"img-cat-home-content\"/>" + cat.check5 + "</div>";
                            _str += "</div>";
                            _str += "<div class=\"col-md-9 padding-right-none\">";
                        }
                        else {
                            _str += "<div class=\"col-md-12 padding-none\">";
                        }
                            _str += "<ul class=\"product-list\">";
                            _str += common.LoadProductList(product, viewBy, cat.pagId.ToString());
                            _str += "</ul>";
                        _str += "</div>";
                    _str += "</div>";
                }
            }
            ltrGroup.Text = _str;
        }
    }
}