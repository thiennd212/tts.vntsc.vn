﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProHome.ascx.cs" Inherits="MyWeb.views.products.ucProHome" %>
<div class="box-pro-sub">
    <div class="cate-header sub-top">
        <div class="list-cat-child col-md-12">
            <asp:Literal ID="ltrListCatChild" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="<%=strClassName %>">
        <asp:Literal ID="ltrGroup" runat="server"></asp:Literal>
        <asp:Literal ID="ltrpro" runat="server"></asp:Literal>
    </div>
</div>
