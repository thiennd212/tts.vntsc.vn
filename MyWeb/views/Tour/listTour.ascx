﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listTour.ascx.cs" Inherits="MyWeb.views.Tour.listTour" %>
<div id="listTourByCat">
    <asp:Literal runat="server" ID="ltrHeader"></asp:Literal>
    <div class="listTourByCat_content">
        <asp:Literal runat="server" ID="ltrContent"></asp:Literal>    
    </div>    
    <asp:Literal runat="server" ID="ltrPaging"></asp:Literal>
</div>