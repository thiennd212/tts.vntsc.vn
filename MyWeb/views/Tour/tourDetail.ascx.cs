﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.Tour
{
    public partial class tourDetail : System.Web.UI.UserControl
    {
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        int currentPage;
        dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            lang = MyWeb.Global.GetLang();
            currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);
            if (Page.IsValid)
            {
                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailfrom = list[0].conMail_Noreply;
                string mailto = list[0].conMail_Info;
                string pas = list[0].conMail_Pass;
                string host = host = "smtp.gmail.com";
                if (list[0].conMail_Method.Length > 0)
                {
                    host = list[0].conMail_Method;
                }
                int post = 465;
                if (list[0].conMail_Port.Length > 0)
                {
                    post = int.Parse(list[0].conMail_Port);
                }
                string cc = "";
                string Noidung = "";
                Noidung += "<table>";
                Noidung += "<tr><td colspan =\"2\"><h3>THÔNG TIN TOUR</h3></td></tr>";
                Noidung += "<tr><td colspan =\"2\">Cảm ơn bạn đã đặt tour tại: <b><a href=\"http://" + Request.Url.Host + "\" target=\"_blank\">" + Request.Url.Host + "</a></b></tr>";
                Noidung += "<tr><td colspan =\"2\">Thông tin của bạn:</tr>";
                Noidung += hdTourName.Value != "" ? "<tr><td><b>Tên tour:</b></td><td>" + hdTourName.Value + "</td></tr>" : "";
                Noidung += txtname.Text != "" ? "<tr><td><b>Thông tin khách hàng:</b></td><td>" + txtname.Text + "</td></tr>" : "";
                Noidung += txtDienthoai.Text != "" ? "<tr><td><b>Điện thoại:</b></td><td>" + txtDienthoai.Text + "</td></tr>" : "";
                Noidung += txtmail.Text != "" ? "<tr><td><b>Email:</b></td><td>" + txtmail.Text + "</td></tr>" : "";
                Noidung += txtSoNguoi.Text != "" ? "<tr><td><b>Số người:</b></td><td>" + txtSoNguoi.Text + "</td></tr>" : "";
                Noidung += txtThoiGian.Text != "" ? "<tr><td><b>Ngày khởi hành:</b></td><td>" + txtThoiGian.Text + "</td></tr>" : "";
                Noidung += txtnoidung.Text != "" ? "<tr><td><b>Yêu cầu thêm:</b></td><td>" + txtnoidung.Text + "</td></tr>" : "";
                Noidung += "</table>";

                try
                {
                    common.SendMail(mailto, cc, "", "", "Thông tin liên hệ từ " + txtname.Text, Noidung);
                    common.SendMail(txtmail.Text.Trim(), "", "", "", "Thông tin liên hệ trên website " + Request.Url.Host, Noidung);
                    lblthongbao.Text = "Bạn đã gửi thông tin thành công !!";
                }
                catch
                {
                    lblthongbao.Text = "Lỗi không gửi được liên hệ !!";
                }
                #endregion
                string file_path = "";
                tbContact ObjtbContact = new tbContact();
                ObjtbContact.conName = common.killChars(txtname.Text);
                ObjtbContact.conCompany = "";
                ObjtbContact.conAddress = "";
                ObjtbContact.conTel = common.killChars(txtDienthoai.Text);
                ObjtbContact.conMail = common.killChars(txtmail.Text);
                ObjtbContact.conDetail = common.killChars(txtnoidung.Text);
                ObjtbContact.conActive = 0;
                ObjtbContact.conLang = lang;
                ObjtbContact.conPositions = "";
                ObjtbContact.confax = "";
                ObjtbContact.confile = file_path;
                ObjtbContact.conwebsite = "";
                ObjtbContact.numberPeople = int.Parse(txtSoNguoi.Text);
                ObjtbContact.bookDate = txtThoiGian.Text;
                ObjtbContact.conType = 9;
                ObjtbContact.conDate = DateTime.Now;
                ObjtbContact.conTour = hdTourName.Value;

                db.tbContacts.InsertOnSubmit(ObjtbContact);
                db.SubmitChanges();

                lblthongbao.Text = MyWeb.Global.GetLangKey("mess_contact");
                txtname.Text = "";
                txtDienthoai.Text = "";
                txtmail.Text = "";
                txtnoidung.Text = "";                
                //Response.Redirect(Request.RawUrl);
            }
        }

        protected void LoadData()
        {
            strUrlFace = Request.Url.ToString();
            if (Request["hp"] != null)
            {
                string tagName = Request["hp"].ToString();
                var curTour = db.Tours.FirstOrDefault(s => s.Tagname == tagName);

                if (curTour != null)
                {
                    string img1 = "", img2 = "", strInfoRight = "", strContent1 = "", strContent2 = "", strOther="";

                    // Info left
                    if (curTour.Image1 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image1 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image1 + "\" /></div>";
                    }
                    if (curTour.Image2 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image2 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image2 + "\" /></div>";
                    }
                    if (curTour.Image3 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image3 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image3 + "\" /></div>";
                    }
                    if (curTour.Image4 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image4 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image4 + "\" /></div>";
                    }
                    if (curTour.Image5 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image5 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image5 + "\" /></div>";
                    }
                    if (curTour.Image6 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image6 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image6 + "\" /></div>";
                    }
                    if (curTour.Image7 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image7 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image7 + "\" /></div>";
                    }
                    if (curTour.Image8 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image8 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image8 + "\" /></div>";
                    }
                    if (curTour.Image9 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image9 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image9 + "\" /></div>";
                    }
                    if (curTour.Image10 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curTour.Image10 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curTour.Image10 + "\" /></div>";
                    }

                    ltrInfoLeft.Text = "<div id='sync1' class='owl-carousel'>" + img1 + "</div><div id='sync2' class='owl-carousel'>" + img2 + "</div>";
                    hdTourName.Value = curTour.Name;
                    hdTourId.Value = curTour.Id.ToString();
                    // Info right
                    strInfoRight += "<div class='tourItems'>";
                    strInfoRight += "  <div class='tourItems_info1'>";
                    //strInfoRight += "      <a class='tourItems_img' href='/" + curTour.Tagname + ".html' title='" + curTour.Name +
                                 //"'><img src='" + curTour.Image1 + "' alt='" + curTour.Name + "' /></a>";
                    strInfoRight += "      <a class='tourItems_link' href='/" + curTour.Tagname + ".html' title='" + curTour.Name + "'>" + curTour.Name + "</a>";                    
                    strInfoRight += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" + String.Format("{0:0,0}", curTour.PriceOrigin) +
                                 " " + curTour.PriceUnit + "</span>";
                    strInfoRight += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}", curTour.Price) + " " +
                                 curTour.PriceUnit + "</span>";
                    strInfoRight += "      <span class='tourItems_topic'><strong>Chủ đề: </strong>" + GetName(curTour.IdTourTopic.Value) + "</span>";
                    strInfoRight += "      <span class='tourItems_cat'><strong>Nhóm tour: </strong>" + GetName(curTour.IdTourCategory.Value) + "</span>";
                    strInfoRight += "      <span class='tourItems_des'>" + MyWeb.Common.StringClass.GetContent(curTour.Description, 150) + "</span>";
                    strInfoRight += "  </div>";
                    strInfoRight += "  <div class='tourItems_info2'>";
                    strInfoRight += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(curTour.IdPlace.Value) + "</div>";
                    strInfoRight += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(curTour.Id) + "</div>";
                    strInfoRight += "  </div>";
                    strInfoRight += "  <div class='tourItems_info3'>";
                    strInfoRight += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + curTour.Start +
                                 "</span>";
                    strInfoRight += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + curTour.NumberOfDay + "</span>";
                    strInfoRight += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + curTour.Transportation +
                                 "</span>";
                    strInfoRight += "  </div>";
                    strInfoRight += "</div>";

                    ltrInfoRight.Text = strInfoRight;

                    // Tab
                    var listTab = db.TourTags.Where(s => s.IdTour == curTour.Id).OrderBy(s => s.Order).ToList();
                    if (listTab.Count > 0)
                    {
                        strContent1 += "<ul class=\"tabs-pro\">";
                        for (int i = 0; i < listTab.Count; i++)
                        {
                            if (i == 0)
                            {
                                strContent1 += "<li class=\"active\" rel=\"tab" + i + "\">" + listTab[i].Name + "</li>";
                            }
                            else
                            {
                                strContent1 += "<li rel=\"tab" + i + "\">" + listTab[i].Name + "</li>";
                            }
                            strContent2 += "<div id=\"tab" + i + "\" class=\"tab_content\">" + listTab[i].Text + "</div>";

                        }
                        strContent1 += "</ul>";
                    }

                    ltrContent.Text = strContent1 + strContent2;

                    // Load list comment
                    var listComment = db.tbComments.Where(s => s.comActive == 1 && s.comNewId == curTour.Id).OrderByDescending(s => s.comId).ToList();
                    int sizeComment = 10;
                    if (listComment.Count() > 0)
                    {
                        if (listComment.Count > sizeComment)
                            ltrPagingComment.Text = getCommentPaging(currentPage, sizeComment, listComment.Count);
                        listComment = listComment.Skip((currentPage - 1) * sizeComment).Take(sizeComment).ToList();
                        string strComment = "";
                        strComment += "<div id='listComment'>";
                        for (int c = 0; c < listComment.Count(); c++)
                        {
                            strComment += "<div class='commentNewItem'>";
                            strComment += "<span class='itemName'>" + listComment[c].comName + "</span>";
                            strComment += "<span class='itemEmail'>" + listComment[c].comEmail + "</span>";
                            strComment += "<span class='itemContent'>" + listComment[c].comComment + "</span>";
                            strComment += "<span class='itemCreatedDate'>" + listComment[c].comDate + "</span>";
                            strComment += "</div>";
                        }
                        strComment += "</div>";
                        ltrListComment.Text = strComment;
                    }

                    // Other
                    var listOther =
                        db.Tours.Where(
                            s =>
                                s.Lang == lang && s.Active == 1 && s.IdTourCategory == curTour.IdTourCategory &&
                                s.Id != curTour.Id).OrderByDescending(s => s.Id).Take(5).ToList();
                    strOther += "<span>Danh sách tour liên quan</span>";

                    foreach (var item in listOther)
                    {
                        strOther += "<div class='tourItems'>";
                        strOther += "  <div class='tourItems_info1'>";
                        strOther += "      <a class='tourItems_img' href='/" + item.Tagname + ".html' title='" + item.Name +
                                     "'><img src='" + item.Image1 + "' alt='" + item.Name + "' /></a>";
                        strOther += "      <a class='tourItems_link' href='/" + item.Tagname + ".html' title='" + item.Name + "'>" + item.Name + "</a>";
                        strOther += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" +
                                        String.Format("{0:0,0}",item.PriceOrigin) + " " + item.PriceUnit + "</span>";
                        strOther += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}",item.Price) +
                                        " " + item.PriceUnit + "</span>";
                        strOther += "  </div>";
                        strOther += "  <div class='tourItems_info2'>";
                        strOther += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(item.IdPlace.Value) + "</div>";
                        strOther += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(item.Id) + "</div>";
                        strOther += "  </div>";
                        strOther += "  <div class='tourItems_info3'>";
                        strOther += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + item.Start +
                                     "</span>";
                        strOther += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + item.NumberOfDay + "</span>";
                        strOther += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + item.Transportation +
                                     "</span>";
                        strOther += "  </div>";
                        strOther += "</div>";
                    }

                    ltrOther.Text = strOther;
                }
                else
                {
                    Response.Redirect("/error404.html");
                }
            }
            else
            {
                Response.Redirect("/error404.html");
            }
        }
        
        protected string getCommentPaging(int cur, int size, int total)
        {
            string strResult = "";
            var numberOfPage = total % size == 0 ? total / size : (total / size) + 1;
            strResult += "<ul class='paging'>";
            var curUrl = Request.RawUrl;
            curUrl = curUrl.IndexOf("?") > 0 ? curUrl.Substring(0, curUrl.IndexOf("?")) : curUrl;
            for (int i = 1; i <= numberOfPage; i++)
            {
                strResult += i == cur ?
                    "<li class='active'>" + i + "</li>" :
                    "<li class='item'><a href='" + curUrl + "?page=" + i + "' title='Trang " + i + "'>" + i + "</a></li>";
            }
            strResult += "</ul>";
            return strResult;
        }

        protected void lbtSendComment_Click(object sender, EventArgs e)
        {
            try
            {
                tbComment obj = new tbComment();
                obj.comName = txtCommentName.Text;
                obj.comEmail = txtCommentEmail.Text;
                obj.comComment = txtCommentContent.Text;
                obj.comDate = DateTime.Now;
                obj.comNewId = Convert.ToInt64(hdTourId.Value.ToString());
                obj.comActive = 0;
                db.tbComments.InsertOnSubmit(obj);
                db.SubmitChanges();
                lbAlter.Visible = true;
                lbAlter.Text = "Gửi bình luận thành công !";
                txtCommentName.Text = "";
                txtCommentEmail.Text = "";
                txtCommentContent.Text = "";
            }
            catch (Exception)
            {
                lbAlter.Visible = true;
                lbAlter.Text = "Có lỗi xảy ra, vui lòng thử lại sau !";
            }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strInfoRight = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strInfoRight += strInfoRight != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strInfoRight;
        }
    }
}